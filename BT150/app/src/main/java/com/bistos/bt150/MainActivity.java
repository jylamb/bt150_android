package com.bistos.bt150;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.speech.SpeechRecognizer;
import androidx.core.content.FileProvider;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bistos.bt150.com.DBHelper;
import com.bistos.bt150.com.PHCommon;
import com.bistos.bt150.com.PHLib;
import com.bistos.bt150.com.ProgramData;
import com.bistos.bt150.com.RoundedImageView;
import com.bistos.bt150.com.STSwipeTapDetector;
import com.bistos.bt150.com.VCmdControl;
import com.bistos.bt150.com.VoiceCommand;
import com.bistos.bt150.menu.Menu01Activity;
import com.bistos.bt150.menu.Menu02Activity;
import com.bistos.bt150.menu.Menu03Activity;
import com.telit.terminalio.TIOConnection;
import com.telit.terminalio.TIOConnectionCallback;
import com.telit.terminalio.TIOManager;
import com.telit.terminalio.TIOManagerCallback;
import com.telit.terminalio.TIOPeripheral;
import com.vikramezhil.droidspeech.DroidSpeech;
import com.vikramezhil.droidspeech.OnDSListener;
import com.vikramezhil.droidspeech.OnDSPermissionsListener;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.InvalidObjectException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import cn.jeesoft.widget.pickerview.CharacterPickerView;
import cn.jeesoft.widget.pickerview.OnOptionChangedListener;

import static android.speech.RecognizerIntent.ACTION_GET_LANGUAGE_DETAILS;
import static android.speech.RecognizerIntent.ACTION_WEB_SEARCH;
import static android.speech.RecognizerIntent.DETAILS_META_DATA;
import static com.bistos.bt150.com.PHCommon.ACT_MAIN;
import static com.bistos.bt150.com.PHCommon.ACT_MENU02;
import static com.bistos.bt150.com.PHCommon.FW_BLOCK_SIZE;
import static com.bistos.bt150.com.PHCommon.OP_TYPE_DOUBLE;
import static com.bistos.bt150.com.PHCommon.OP_TYPE_SINGLE;
import static com.bistos.bt150.com.PHCommon.VOICE_MODE_OPERATION;
import static com.bistos.bt150.com.PHCommon.VOICE_REC_LANG_ENGLISH;
import static com.bistos.bt150.com.PHCommon.VOICE_REC_LANG_FRENCH;
import static com.bistos.bt150.com.PHCommon.VOICE_REC_LANG_KOREAN;
import static com.bistos.bt150.com.PHCommon.hexStringToByteArray;
import static com.bistos.bt150.com.PHCommon.mConnection;
import static com.bistos.bt150.com.PHCommon.mPeripheral;
import static com.bistos.bt150.com.PHCommon.nOperationMode;
import static com.bistos.bt150.com.PHCommon.nOperationType;
import static com.bistos.bt150.com.PHCommon.nPressureExp;
import static com.bistos.bt150.com.PHCommon.nPrgOpType5;
import static com.bistos.bt150.com.PHCommon.nPrgOpType6;
import static com.bistos.bt150.com.PHCommon.nPrgOpType7;
import static com.bistos.bt150.com.PHCommon.nPrgOpType8;
import static com.bistos.bt150.com.PHCommon.nPrgReqCnt5;
import static com.bistos.bt150.com.PHCommon.nPrgReqCnt6;
import static com.bistos.bt150.com.PHCommon.nPrgReqCnt7;
import static com.bistos.bt150.com.PHCommon.nPrgReqCnt8;
import static com.bistos.bt150.com.PHCommon.nSendDataIdx;
import static com.bistos.bt150.com.PHCommon.nVoiceMode;
import static com.bistos.bt150.com.PHCommon.sCmdFWAddress;
import static com.bistos.bt150.com.PHCommon.sCmdFWBinary;
import static com.bistos.bt150.com.PHCommon.sCmdFWEnd;
import static com.bistos.bt150.com.PHCommon.sCmdFWErase;
import static com.bistos.bt150.com.PHCommon.sCmdFWEraseDelay;
import static com.bistos.bt150.com.PHCommon.sCmdFWFlash;
import static com.bistos.bt150.com.PHCommon.sCmdFWLast;
import static com.bistos.bt150.com.PHCommon.sCmdFWReadyRecv_CR;
import static com.bistos.bt150.com.PHCommon.sCmdFWReadyRecv_D;
import static com.bistos.bt150.com.PHCommon.sCmdFWReadyRecv_E;
import static com.bistos.bt150.com.PHCommon.sCmdFWReadyRecv_ERR;
import static com.bistos.bt150.com.PHCommon.sCmdFWReadyRecv_R;
import static com.bistos.bt150.com.PHCommon.sCmdFWReadyRecv_Y;
import static com.bistos.bt150.com.PHCommon.sCmdFWReadySend_Idle;
import static com.bistos.bt150.com.PHCommon.sCmdFWReadySend_Ready;
import static com.bistos.bt150.com.PHCommon.sCmdFWReadySend_U;
import static com.bistos.bt150.com.PHCommon.sCmdFWReadySend_V;
import static com.bistos.bt150.com.PHCommon.sCmdFWReadySend_X;
import static com.bistos.bt150.com.PHCommon.sCmdFWReadySend_Z;
import static com.bistos.bt150.com.PHCommon.sCmdFWRecv;
import static com.bistos.bt150.com.PHLib.LOG;
import static com.bistos.bt150.com.PHLib.LOGe;

/**
 * Created by lamb on 2017. 10. 26..
 */

public class MainActivity extends AppCompatActivity implements TIOManagerCallback, TIOConnectionCallback, OnDSListener, OnDSPermissionsListener
{

    private static final String TAG = "MainActivity";

    private static final int RESULT_LOAD_ALBUM = 100;
    private static final int RESULT_LOAD_CAMERA = 200;
    private static final int CROP_FROM_CAMERA = 300;
    //TIO Lib
    private static final int ENABLE_BT_REQUEST_ID = 1;
//    private static final int SCAN_INTERVAL 		  = 8000;
    private static final int SCAN_INTERVAL 		  = 5000;
    private static final int RSSI_INTERVAL 		  = 1670;

    private TIOManager                  mTio;
    private ListView 					mPeripheralsListView;
    private ArrayAdapter<TIOPeripheral> mPeripheralList;
    private TextView textView_PopBle_Scan;
    private LinearLayout linearLayout_PopBle_Scan;
    private Button button_PopBle_Scan = null;

    private String          mErrorMessage;

    private AlertDialog     mConnectingDialog;
    private AlertDialog     mDataLoadingDialog;
//    private AlertDialog     mFWUpgradeDialog;
    private Dialog     mFWUpgradeDialog;


    private ImageButton imageButton_ActMain_Settings;
    private ImageButton imageButton_ActMain_Help;
    private RoundedImageView roundedimageview_ActMyCarMain_Photo;
    private ImageButton imagebutton_ActMyCarMain_PhotoDel;
    private ImageButton imagebutton_ActMyCarMain_PhotoReg;
    private TextView textView_ActMain_UserName;
    private TextView textView_ActMain_BirthDay;
    private TextView textView_ActMain_BirthFrom;
    private ImageButton imageButton_ActMain_Male;
    private ImageButton imageButton_ActMain_Female;

    private LinearLayout linearLayout_ActMain_UserName;
    private LinearLayout linearLayout_ActMain_BirthDay;
    private LinearLayout linearLayout_ActMain_Operation;
    private LinearLayout linearLayout_ActMain_Program;
    private LinearLayout linearLayout_ActMain_Statistic;
    private TextView textView_ActMain_BLEStatus;
    private LinearLayout linearLayout_ActMain_BLEStatus;
    private Dialog dlgSetttingsEdit;

    private CharacterPickerView pickerViewLanguage;
    private CharacterPickerView pickerViewDevSearchTimeOut;

    private CharacterPickerView pickerViewDurationMin;
    private CharacterPickerView pickerViewDurationSec;

    private ArrayList<String> arrVCmdLanguage = new ArrayList<String>();
    private ArrayList<String> arrDevSearchTimeOut = new ArrayList<String>();

    private ArrayList <String> arrSeqMin = new ArrayList<String>();
    private ArrayList <String> arrSeqSec = new ArrayList<String>();

    private Bitmap bitmapMember = null;

//    private Uri imageUri = null;
    private Uri cropImageUri = null;
    private Uri photoImageUri = null;

    private Dialog dlgBLEScan;
    private boolean bFirst = true;
    private boolean bActivityShow = false;

    private Timer mTimerStatusCheck = null;
    private TimerTask taskStatusCheck = null;
    private int nCntElapsedTime = 0;
    private int nCntElapsedBattery = 0;
    private int nCntElapsedProgramProcess = 0;

    private Timer mTimerProgramData = null;
    private TimerTask taskProgramData = null;

    private Timer mTimerBleScan = null;
    private TimerTask taskBleScan = null;

    private Timer mTimerFWUp = null;
    private TimerTask taskFWUp = null;

    private DroidSpeech droidSpeech;
    private boolean bSpeechRecogizing = false;
    private boolean isRecognitionAvailable = false;
    private  BluetoothAdapter mBluetoothAdapter = null;

    private ArrayList<String> arrLine = new ArrayList<>();
    private int nFWDataSize = 0;
    private short nFWSendCnt = 0;
    private byte[] byteFWSend = null;

    private Timer mTimerFWDataBlock = null;
    private TimerTask taskFWDataBlock = null;
    private int nFWBlockSendIdx = 0;

    private boolean bFWUpMode = false;

    private int nProgressStatus = 0;
    private Handler handler = new Handler();

    public void showDialogProgress(Activity activity, String msg, String a_Title) {

        if (mFWUpgradeDialog == null) {
            mFWUpgradeDialog = new Dialog(activity);
            mFWUpgradeDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            mFWUpgradeDialog.setCancelable(false);
            mFWUpgradeDialog.setContentView(R.layout.pop_dialog_progress);
        }

        final ProgressBar text = (ProgressBar) mFWUpgradeDialog.findViewById(R.id.progress_horizontal);
        final TextView text2 = mFWUpgradeDialog.findViewById(R.id.value123);
        final TextView strMessage = mFWUpgradeDialog.findViewById(R.id.strMessage);
        final TextView strTitle = mFWUpgradeDialog.findViewById(R.id.strTitle);

        if (msg != null && msg.length() > 0) {
            strMessage.setText(msg);
        }

        if (a_Title != null && a_Title.length() > 0) {
            strTitle.setText(a_Title);
        }

        handler.post(new Runnable() {
            @Override
            public void run() {
                text.setProgress(nProgressStatus);
                text2.setText(String.valueOf(nProgressStatus));
//                if (nProgressStatus >= 100) {
//                    mFWUpgradeDialog.dismiss();
//                }
            }
        });

        if(mFWUpgradeDialog != null && !mFWUpgradeDialog.isShowing()) {
            mFWUpgradeDialog.show();
            Window window = mFWUpgradeDialog.getWindow();
            window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        }

    }

    private void checkConnectKnownPeripheral(TIOPeripheral peripheral) {

        SharedPreferences pref = getSharedPreferences(getString(R.string.project_name), Activity.MODE_PRIVATE);
        String aDevAddr = pref.getString("device_addr", "");

        if (aDevAddr != null && aDevAddr.equals(peripheral.getAddress())) {
            if (dlgBLEScan != null && dlgBLEScan.isShowing()) {
                dlgBLEScan.cancel();
            }
            onPeripheralCellPressed(peripheral);
        }
    }

    @Override
    public void onPeripheralFound(TIOPeripheral peripheral) {
        LOGe(TAG, "onPeripheralDiscovered " + peripheral.toString());

        // overrule default behaviour: peripheral shall be saved only after having been connected
        peripheral.setShallBeSaved(false);
        mTio.savePeripherals();

        updatePeripheralsListView();

        checkConnectKnownPeripheral(peripheral);
    }

    @Override
    public void onPeripheralUpdate(TIOPeripheral peripheral) {
        LOGe(TAG, "onPeripheralUpdate " + peripheral.toString());

        updatePeripheralsListView();

        checkConnectKnownPeripheral(peripheral);
    }

    private final BroadcastReceiver cmdDisconnect = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent a_intent) {
            doDisconnectPeripheral();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        //Check BLE
        if (!getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) {
            PHLib.showConfirmDialog(getResources().getString(R.string.ble_not_supported), this, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    PHLib.killApp(MainActivity.this);
                }
            });
            return;
        }

        initSeqTime();

        initDevSearchTimeOut();

        initVoiceCommandLanguage();

        checkVoicRecognitionSupported();

        if (isRecognitionAvailable) {
            initDroidSpeech();
        } else {
            PHCommon.bVCmdEnabled = false;
        }

        registerReceiver(cmdDisconnect, new IntentFilter(PHCommon.MSG_CMD_DISCONNECT));

        imageButton_ActMain_Settings = (ImageButton) findViewById(R.id.imageButton_ActMain_Settings);
        imageButton_ActMain_Help = (ImageButton) findViewById(R.id.imageButton_ActMain_Help);
        roundedimageview_ActMyCarMain_Photo = (RoundedImageView) findViewById(R.id.roundedimageview_ActMyCarMain_Photo);
        imagebutton_ActMyCarMain_PhotoDel = (ImageButton) findViewById(R.id.imagebutton_ActMyCarMain_PhotoDel);
        imagebutton_ActMyCarMain_PhotoReg = (ImageButton) findViewById(R.id.imagebutton_ActMyCarMain_PhotoReg);
        textView_ActMain_UserName = (TextView) findViewById(R.id.textView_ActMain_UserName);
        textView_ActMain_BirthDay = (TextView) findViewById(R.id.textView_ActMain_BirthDay);
        textView_ActMain_BirthFrom = (TextView) findViewById(R.id.textView_ActMain_BirthFrom);
        imageButton_ActMain_Male = (ImageButton) findViewById(R.id.imageButton_ActMain_Male);
        imageButton_ActMain_Female = (ImageButton) findViewById(R.id.imageButton_ActMain_Female);
        linearLayout_ActMain_UserName = (LinearLayout) findViewById(R.id.linearLayout_ActMain_UserName);
        linearLayout_ActMain_BirthDay = (LinearLayout) findViewById(R.id.linearLayout_ActMain_BirthDay);
        linearLayout_ActMain_Operation = (LinearLayout) findViewById(R.id.linearLayout_ActMain_Operation);
        linearLayout_ActMain_Program = (LinearLayout) findViewById(R.id.linearLayout_ActMain_Program);
        linearLayout_ActMain_Statistic = (LinearLayout) findViewById(R.id.linearLayout_ActMain_Statistic);
        textView_ActMain_BLEStatus = (TextView) findViewById(R.id.textView_ActMain_BLEStatus);
        linearLayout_ActMain_BLEStatus = (LinearLayout) findViewById(R.id.linearLayout_ActMain_BLEStatus);



        imageButton_ActMain_Settings.setOnClickListener(onClickMenu);
        imageButton_ActMain_Help.setOnClickListener(onClickMenu);
        imagebutton_ActMyCarMain_PhotoDel.setOnClickListener(onClickMenu);
        imagebutton_ActMyCarMain_PhotoReg.setOnClickListener(onClickMenu);
        linearLayout_ActMain_UserName.setOnClickListener(onClickMenu);
        linearLayout_ActMain_BirthDay.setOnClickListener(onClickMenu);
        imageButton_ActMain_Male.setOnClickListener(onClickMenu);
        imageButton_ActMain_Female.setOnClickListener(onClickMenu);
        linearLayout_ActMain_Operation.setOnClickListener(onClickMenu);
        linearLayout_ActMain_Program.setOnClickListener(onClickMenu);
        linearLayout_ActMain_Statistic.setOnClickListener(onClickMenu);
        linearLayout_ActMain_BLEStatus.setOnClickListener(onClickMenu);

        //Init TIO
        mTio = TIOManager.getInstance();
        if(mTio == null) {
            LOGe(TAG, "mTio is null!!!!!!!!!!!!!!!!!!");
        }
        mTio.enableTrace(true);

        // displays a dialog requesting user permission to enable Bluetooth.
        if (! mTio.isBluetoothEnabled()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, ENABLE_BT_REQUEST_ID);
        }

        if (Locale.getDefault().getLanguage().equals("fr")) {
            imageButton_ActMain_Help.setVisibility(View.GONE);
        } else {
            imageButton_ActMain_Help.setVisibility(View.VISIBLE);
        }

        loadDefaultProgramData_20210325();

        loadUserInfo();

        loadUserPhoto();

        PHCommon.initSendingThread();
    }

    private void initSeqTime() {

        arrSeqMin.clear();
        arrSeqSec.clear();

        for (int i = 0; i < 31; i++) {
            arrSeqMin.add(String.format("%02d", i));
        }

        for (int i = 0; i < 60; i++) {
            arrSeqSec.add(String.format("%02d", i));
        }

//        LOG(TAG, "aStart:" + aStart + " aEnd:" + aEnd);
    }

    private void initDevSearchTimeOut() {

        SharedPreferences pref = getSharedPreferences(getString(R.string.project_name), Activity.MODE_PRIVATE);
        PHCommon.curDevSearchTimeOut = pref.getInt("dev_search_limit", 3);

        arrDevSearchTimeOut.clear();
        arrDevSearchTimeOut.add("1");
        arrDevSearchTimeOut.add("3");
        arrDevSearchTimeOut.add("5");
    }

    @Override
    protected void onPause() {
        super.onPause();

        stopRssiListener();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {

        LOGe(TAG, "onDestroy - MainAct");

        PHCommon.stopSendingThread();
        stopStatusTimer();
        stopDroidSpeech();

        super.onDestroy();

        if ( mConnection != null ) {
            mConnection.setListener( null );
        }
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);

        if (hasFocus) {
            bActivityShow = true;
            if (bFirst) {
                bFirst = false;
                LOGe(TAG, "showBLEScan onWindowFocusChanged: bFirst:" + bFirst);
                showBLEScan();
            }
        }

    }

    @Override
    protected void onResume() {
        super.onResume();

        PHCommon.nActivityNo = ACT_MAIN;
        nVoiceMode = VOICE_MODE_OPERATION;

        if (mBluetoothAdapter == null) {
            // Device does not support Bluetooth
        } else {
            if (!mBluetoothAdapter.isEnabled()) {
                // Bluetooth is not enable :)
                PHLib.showMessageDialog(getString(R.string.BlutoothIsOff), MainActivity.this);
            }
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            PHCommon.showExitAlert(this);
            return true;
        }
        return false;
    }

    private void startDroidSpeechMainThread() {

        if (mTimerProgramData != null) {
            mTimerProgramData.cancel();
            mTimerProgramData.purge();
            mTimerProgramData = null;
        }

        taskProgramData = new TimerTask() {
            public void run() {
                if (mTimerProgramData != null) {
                    mTimerProgramData.cancel();
                    mTimerProgramData.purge();
                    mTimerProgramData = null;
                }

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        startDroidSpeech();

                        LOGe(TAG, "Close Dialog!!!!!!!!");

                        if((mDataLoadingDialog != null) && mDataLoadingDialog.isShowing()) {
                            mDataLoadingDialog.dismiss();
                        };

                        PHCommon.bProcessingProgramData = false;
                        nCntElapsedProgramProcess = 0;
                    }
                });
            }
        };
        mTimerProgramData = new Timer();
        mTimerProgramData.schedule(taskProgramData, 1000);
    }

    private void startCheckDeviceStatusTimer() {

//        runOnUiThread(new Runnable() {
//            @Override
//            public void run() {
//                showProgramDataLoading();
//            }
//        });

        PHCommon.sendData(PHCommon.sCmdElapsedTime);

        PHCommon.sendData(PHCommon.sCmdInqueryStatus);

        stopStatusTimer();

        taskStatusCheck = new TimerTask() {
            public void run() {
                if (mPeripheral.getConnectionState() == TIOConnection.STATE_CONNECTED) {

                    if (!PHCommon.bProgramDataLoaded) {
                        PHCommon.bProgramDataLoaded = true;

                        PHCommon.sendProgramDataAll(MainActivity.this);

                        if (!Locale.getDefault().getLanguage().equals("fr")) {
                            startDroidSpeechMainThread();
                        }

//                        runOnUiThread(new Runnable() {
//                            @Override
//                            public void run() {
//                                PHCommon.bRecvProgramCnt = false;
//                                PHCommon.bProcessingProgramData = true;
//                                nCntElapsedProgramProcess = 0;
//                                PHCommon.sendData("A;");
//                            }
//                        });

                        PHCommon.bRecvProgramCnt = true;

                    } else {

                        if (!PHCommon.bRecvProgramCnt) {
//                            doDisconnectPeripheral();
                            return;
                        }

                        if (!PHCommon.bProcessingProgramData && nCntElapsedProgramProcess > 60*5) {
                            //상태체크
                            PHCommon.bProcessingProgramData = false;
                            nCntElapsedProgramProcess = 0;

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    PHCommon.sendData(PHCommon.sCmdInqueryStatus);
                                }
                            });
                        }
                        nCntElapsedProgramProcess++;

                        if (nCntElapsedTime > 5 || PHCommon.nElapsedTime == 0) {
                            //시계
                            nCntElapsedTime = 0;

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    PHCommon.sendData(PHCommon.sCmdElapsedTime);
                                }
                            });
                        }
                        nCntElapsedTime++;

                        if (nCntElapsedBattery > 7 || PHCommon.nElapsedTime == 0) {
                            //배터리
                            nCntElapsedBattery = 0;

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    PHCommon.sendData(PHCommon.sCmdInqueryBattery);
                                }
                            });
                        }
                        nCntElapsedBattery++;
                    }

                } else {
                    nCntElapsedProgramProcess = 0;
                    nCntElapsedBattery = 0;
                    nCntElapsedTime = 0;
                }

            }
        };
        mTimerStatusCheck = new Timer();
        mTimerStatusCheck.schedule(taskStatusCheck, 1000, 1000);
    }

    private void stopStatusTimer() {
        if (mTimerStatusCheck != null) {
            mTimerStatusCheck.cancel();
            mTimerStatusCheck.purge();
            mTimerStatusCheck = null;
        }
    }


    private void inQueryProgramData() {

        showProgramDataLoading();

        for (int i = 4; i < PHCommon.arrProgramData.length; i++) {

            LOGe(TAG, "inQueryProgramData: nPrgReqCnt5:" + nPrgReqCnt5 + " nPrgReqCnt6:" + nPrgReqCnt6 + " nPrgReqCnt7:" + nPrgReqCnt7 + " nPrgReqCnt8:" + nPrgReqCnt8);

            if (i == 4 && nPrgReqCnt5 == 0
                    || i == 5 && nPrgReqCnt6 == 0
                    || i == 6 && nPrgReqCnt7 == 0
                    || i == 7 && nPrgReqCnt8 == 0
            ) {
                continue;
            }

            ArrayList<ProgramData> aData = PHCommon.arrProgramData[i];
            for (int j = 0; j < aData.size(); j++) {
//                ProgramData programData = aData.get(j);
                    if (i == 4 && j >= nPrgReqCnt5
                            || i == 5 && j >= nPrgReqCnt6
                            || i == 6 && j >= nPrgReqCnt7
                            || i == 7 && j >= nPrgReqCnt8
                    ) {
                        break;
                    }
                    PHCommon.sendData(String.format("O%dS%d;", i, j));
            }
        }


        if (mTimerProgramData != null) {
            mTimerProgramData.cancel();
            mTimerProgramData.purge();
            mTimerProgramData = null;
        }

        taskProgramData = new TimerTask() {
            public void run() {
                if (mTimerProgramData != null) {
                    mTimerProgramData.cancel();
                    mTimerProgramData.purge();
                    mTimerProgramData = null;
                }

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        LOGe(TAG, "Close Dialog!!!!!!!!");

                        if((mDataLoadingDialog != null) && mDataLoadingDialog.isShowing()) {
                            mDataLoadingDialog.dismiss();
                        };

                        PHCommon.bProcessingProgramData = false;
                        nCntElapsedProgramProcess = 0;
                    }
                });
            }
        };
        mTimerProgramData = new Timer();
        mTimerProgramData.schedule(taskProgramData, 5000, 1000);
    }

    private void initializePeripheralsListView() {

        // create data adapter for peripherals list view
        mPeripheralList = new ArrayAdapter<TIOPeripheral>(this, 0) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                return createPeripheralCell(position);
            }
            @Override
            public int getCount() {
                return mTio.getPeripherals().length;
            }
        };
        mPeripheralsListView.setAdapter(mPeripheralList);
    }

    private View createPeripheralCell(int position) {

        final TIOPeripheral peripheral = mTio.getPeripherals()[position];

        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View peripheralCell = inflater.inflate(R.layout.peripheral_cell, mPeripheralsListView, false);

        TextView mainTitle = (TextView) peripheralCell.findViewById(R.id.mainTitle);
//        mainTitle.setText(peripheral.getName() + "  " + peripheral.getAddress());
        String aBLEName = peripheral.getName();
        LOGe(TAG, "aBLEName:" + aBLEName);
        if (aBLEName != null && aBLEName.length() > 9) {
            String aPrefix = aBLEName.substring(0, 2);
            String aSerial = "";
            if (aPrefix.toLowerCase().equals("hb") ) {
                aSerial = aBLEName.substring(6);
            } else if (aPrefix.toLowerCase().equals("hi") ) {
                String aHex = aBLEName.substring(8);
                int aIntVal = Integer.parseInt( aHex, 16 );
                aSerial = String.format("%04d", aIntVal);
            }
            mainTitle.setText("Hi Bebe Breast Pump – " + aSerial);
        }

        TextView subTitle = (TextView) peripheralCell.findViewById(R.id.subTitle);
        if ( peripheral.getAdvertisement() != null) {
            subTitle.setText(peripheral.getAdvertisement().toString() + " RSSI " + peripheral.getAdvertisement().getRssi());
        } else {
            subTitle.setText("");
        }

        peripheralCell.setOnTouchListener(createPeripheralCellGestureDetector(peripheral, null));

        return peripheralCell;
    }

    private STSwipeTapDetector createPeripheralCellGestureDetector(final TIOPeripheral peripheral, final Button removeButton) {
        STSwipeTapDetector detector = new STSwipeTapDetector(this) {

            @Override
            public boolean onLeftSwipe() {
                return true; // handled
            }
            @Override
            public boolean onRightSwipe() {
                return true;
            }
            @Override
            public boolean onTap() {
                dlgBLEScan.cancel();
                onPeripheralCellPressed(peripheral);
                return true; // handled
            }
        };

        return detector;
    }

    private void onPeripheralCellPressed(TIOPeripheral peripheral) {

        LOGe(TAG, "onPeripheralCellPressed "+ peripheral.toString());

        connectPeripheral(peripheral);
        updateUIState();

//        if(mConnection != null) {
//            mRemoteUARTMtuSize = mConnection.getRemoteMtuSize();
//            mLocalUARTMtuSize  = mConnection.getLocalMtuSize();
//        } else {
//            mRemoteUARTMtuSize = TIO_DEFAULT_UART_DATA_SIZE;
//            mLocalUARTMtuSize  = TIO_DEFAULT_UART_DATA_SIZE;
//        }

        if ( mConnection != null ) {
            mConnection.setListener( this );
            startRssiListener();
        }

        doConnectPeripheral();
    }

    public void doConnectPeripheral() {
        LOGe(TAG, "doConnectPeripheral");

        try {
            mConnection = mPeripheral.connect(this);
            showConnectionMessage();

        } catch (Exception ex) {
            LOGe(TAG, ex.getMessage());
        }
        updateUIState();
    }

    public void doDisconnectPeripheral() {
        LOGe(TAG, "doDisconnectPeripheral");

        stopDroidSpeech();

        stopRssiListener();
        try {
            mConnection.disconnect();

        } catch ( Exception ex){
            LOGe(TAG, ex.getMessage());
        }
        updateUIState();
    }

    void showConnectionMessage() {
        final AlertDialog.Builder dialog;
        if (mConnectingDialog == null) {
            // Create dialog
            dialog = new AlertDialog.Builder(this)
                    .setMessage(R.string.connecting)
                    .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            if ( mConnection != null ) {
                                try {
                                    mConnection.cancel();
                                    mConnection = null;
                                }
                                catch ( Exception ex) {
                                    Log.e(TAG,ex.toString() );
                                }
                            }
                        }
                    });
            mConnectingDialog = dialog.create();

            // Disable click event outside a dialog
            Window window = mConnectingDialog.getWindow();
            window.setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL,
                    WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL);
            window.clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        }

        // Show dialog
        mConnectingDialog.show();
    }

    void showProgramDataLoading() {
        final AlertDialog.Builder dialog;
        if (mDataLoadingDialog == null) {
            // Create dialog
            dialog = new AlertDialog.Builder(this)
                    .setMessage(getString(R.string.ProgramDataLoading) + "...")
                    .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            PHLib.showMessageDialog(getString(R.string.ProgramDataLoadingFail), MainActivity.this);
                            if ( mConnection != null ) {
                                try {
                                    mConnection.cancel();
                                    mConnection = null;
                                }
                                catch ( Exception ex) {
                                    Log.e(TAG,ex.toString() );
                                }
                            }
                        }
                    });
            mDataLoadingDialog = dialog.create();

            // Disable click event outside a dialog
            Window window = mDataLoadingDialog.getWindow();
            window.setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL, WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL);
            window.clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        }
        // Show dialog
        mDataLoadingDialog.show();
    }

    boolean showFWUpgradeMessage() {
        showDialogProgress(this, getString(R.string.FWUpgrade), getString(R.string.FWUpWarning));
        return true;
    }

    private void startRssiListener() {
        if (mPeripheral.getConnectionState() == TIOConnection.STATE_CONNECTED) {
            Log.d(TAG, "startRssiListener");
            try {
                mConnection.readRemoteRssi(RSSI_INTERVAL);
            } catch (Exception ex) {}
        }
    }

    private void stopRssiListener() {
        if ( mConnection != null) {
            Log.d(TAG, "stopRssiListener");
            try {
                mConnection.readRemoteRssi(0);
            }
            catch ( Exception ex) {}
        }
    }

    private void updatePeripheralsListView() {
        // update adapter with currently known peripherals
        mPeripheralList.notifyDataSetChanged();

        if (mTio.getPeripherals().length > 0) {
            mPeripheralsListView.setVisibility(View.VISIBLE);
            linearLayout_PopBle_Scan.setVisibility(View.GONE);
        } else {
            mPeripheralsListView.setVisibility(View.GONE);
            linearLayout_PopBle_Scan.setVisibility(View.VISIBLE);
        }
    }

    private void loadDefaultProgramData_20210325() {
        //P01 - P05
        parseProgramData("I0S0M0P01C2T0135E0;");
        parseProgramData("I0S1M1P03C0T0005E0;");
        parseProgramData("I0S2M1P05C0T1060E0;");
        //P02 - P06
        parseProgramData("I1S0M0P01C2T0240E0;");
        parseProgramData("I1S1M0P01C1T0180E0;");
        parseProgramData("I1S2M0P02C0T0120E0;");
        parseProgramData("I1S3M0P01C2T0240E0;");
        parseProgramData("I1S4M0P01C1T0180E0;");
        parseProgramData("I1S5M0P02C0T0120E0;");
        parseProgramData("I1S6M0P01C2T0120E0;");
        //P03 - P05
        parseProgramData("I2S0M0P01C2T0135E0;");
        parseProgramData("I2S1M1P03C0T0005E0;");
        parseProgramData("I2S2M1P05C0T1060E0;");
        //P04 - P06
        parseProgramData("I3S0M0P01C2T0240E0;");
        parseProgramData("I3S1M0P01C1T0180E0;");
        parseProgramData("I3S2M0P02C0T0120E0;");
        parseProgramData("I3S3M0P01C2T0240E0;");
        parseProgramData("I3S4M0P01C1T0180E0;");
        parseProgramData("I3S5M0P02C0T0120E0;");
        parseProgramData("I3S6M0P01C2T0120E0;");

        //P05 - for France
        parseProgramData("I4S0M0P01C2T0135E0;");
        parseProgramData("I4S1M1P03C0T0005E0;");
        parseProgramData("I4S2M1P05C0T1060E0;");

        //P06 - for France
        parseProgramData("I5S0M0P01C2T0240E0;");
        parseProgramData("I5S1M0P01C1T0180E0;");
        parseProgramData("I5S2M0P02C0T0120E0;");
        parseProgramData("I5S3M0P01C2T0240E0;");
        parseProgramData("I5S4M0P01C1T0180E0;");
        parseProgramData("I5S5M0P02C0T0120E0;");
        parseProgramData("I5S6M0P01C2T0120E0;");

    }

    private void loadProgramData(int a_TabIdx) {

        SharedPreferences pref = getSharedPreferences(getString(R.string.project_name), Activity.MODE_PRIVATE);
        for (int idx = 0; idx < 8; idx++) {
            String aData = pref.getString(String.format("programdata_%d_%d", a_TabIdx, idx), "");
            LOGe(TAG, "loadProgramData :" + String.format("programdata_%d_%d", a_TabIdx, idx) + ": " + aData);
            if (aData.length() == 17) {
                parseProgramData(aData);
            }
        }
    }

    private void loadProgramCntOpType() {
        SharedPreferences pref = getSharedPreferences(getString(R.string.project_name), Activity.MODE_PRIVATE);
        String aData = pref.getString("program_cnt_optype", "");
        LOGe(TAG, "loadProgramCntOpType:" + aData);
        if (aData.length() == 9) {

            nPrgReqCnt5 = PHLib.convertToInt(aData.substring(1, 2));
            nPrgReqCnt6 = PHLib.convertToInt(aData.substring(2, 3));
            nPrgReqCnt7 = PHLib.convertToInt(aData.substring(3, 4));
            nPrgReqCnt8 = PHLib.convertToInt(aData.substring(4, 5));

            nPrgOpType5 = PHLib.convertToInt(aData.substring(5, 6));
            nPrgOpType6 = PHLib.convertToInt(aData.substring(6, 7));
            nPrgOpType7 = PHLib.convertToInt(aData.substring(7, 8));
            nPrgOpType8 = PHLib.convertToInt(aData.substring(8, 9));

            LOGe(TAG, "loadProgramCntOpType: nPrgOpType5:" + nPrgOpType5 + " nPrgOpType6:" + nPrgOpType6 + " nPrgOpType7:" + nPrgOpType7 + " nPrgOpType8:" + nPrgOpType8);
            LOGe(TAG, "loadProgramCntOpType: nPrgReqCnt5:" + nPrgReqCnt5 + " nPrgReqCnt6:" + nPrgReqCnt6 + " nPrgReqCnt7:" + nPrgReqCnt7 + " nPrgReqCnt8:" + nPrgReqCnt8);

        }
    }

    private void loadUserInfo() {

//        if (!Locale.getDefault().getLanguage().equals("fr")) {
            loadProgramData(4);
            loadProgramData(5);
//        }
        loadProgramData(6);
        loadProgramData(7);

        loadProgramCntOpType();

        SharedPreferences pref = getSharedPreferences(getString(R.string.project_name), Activity.MODE_PRIVATE);
        String aUserName = pref.getString("userName", getString(R.string.UserName));
        int aBirthYear = pref.getInt("birthYear", -1);
        int aBirthMon = pref.getInt("birthMon", -1);
        int aBirthDay = pref.getInt("birthDay", -1);
        int aGender = pref.getInt("gender", -1);

        if (aUserName != null && aUserName.length() > 0) {
            textView_ActMain_UserName.setText(aUserName);
        }

        if (aBirthYear > 0 && aBirthMon > 0 && aBirthDay > 0) {
            textView_ActMain_BirthDay.setText(String.format("%d / %d / %d", aBirthYear, aBirthMon+1, aBirthDay));
            try {
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                Date dateBirth = sdf.parse(String.format("%04d-%02d-%02d", aBirthYear, aBirthMon+1, aBirthDay));
                textView_ActMain_BirthFrom.setText(String.format("%d", PHLib.getDaysFromDateToDate(dateBirth, new Date())));
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        if (aGender >= 0) {
            setGender(aGender);
        }

        PHCommon.nProgramLastTime = pref.getInt("program_last_time", 30*60);
        LOGe(TAG, "PHCommon.nProgramLastTime:" + PHCommon.nProgramLastTime);
    }

    public Bitmap getResizedBitmap(Bitmap bm, double newHeight, double newWidth) {
        int width = bm.getWidth();
        int height = bm.getHeight();
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;

        // Create a matrix for the manipulation
        Matrix matrix = new Matrix();

        // Resize the bit map
        matrix.postScale(scaleWidth, scaleHeight);

        // Recreate the new Bitmap
        Bitmap resizedBitmap = Bitmap.createBitmap(bm, 0, 0, width, height, matrix, false);
        return resizedBitmap;
    }

    private void resizeNSaveBitmap() {
        LOGe(TAG, "resizeNSaveBitmap-1");

        File cropFile = getFileName("Photo", 0);
        if (cropFile.exists()) {
            LOGe(TAG, "resizeNSaveBitmap-2");
            Bitmap image = BitmapFactory.decodeFile(cropFile.toString());
            int width = image.getWidth();
            int height = image.getHeight();

            double aRatio = 640.0 / (double) width;
            LOGe(TAG, "aRatio:" + aRatio + " width:" + width + " height:" + height);

            Bitmap aSmallBitmap = getResizedBitmap(image, height*aRatio, width*aRatio);

            Bitmap aNewBitmap = null;
            ExifInterface ei = null;
            try {
                ei = new ExifInterface(cropFile.getPath());
                int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
                LOGe(TAG, "resizeNSaveBitmap-3 : orientation: " + orientation);

                switch (orientation) {
                    case ExifInterface.ORIENTATION_ROTATE_90:
                        aNewBitmap = rotateImage(aSmallBitmap, 90);
                        break;
                    case ExifInterface.ORIENTATION_ROTATE_180:
                        aNewBitmap = rotateImage(aSmallBitmap, 180);
                        break;
                    case ExifInterface.ORIENTATION_ROTATE_270:
                        aNewBitmap = rotateImage(aSmallBitmap, 270);
                        break;
                    default:
                        aNewBitmap = aSmallBitmap;
                        break;
                }

            } catch (IOException e) {
                e.printStackTrace();
            }


            String aFilePath = Environment.getExternalStorageDirectory().getAbsolutePath();
            File dir = new File(aFilePath,  "bt150");
            if(! dir.isDirectory()) {
                dir.mkdirs();
            }

            LOGe(TAG, "resizeNSaveBitmap-4");

            String fname = "photo0.jpg";
            File file = new File(dir, fname);
            LOGe(TAG, "resizeNSaveBitmap-5:" + file);
            if (file.exists())
                file.delete();
            try {
                FileOutputStream out = new FileOutputStream(file);
                aNewBitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
                out.flush();
                out.close();
                LOGe(TAG, "resizeNSaveBitmap-6");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        LOGe(TAG, "resizeNSaveBitmap-7");

    }

    private static Bitmap rotateImage(Bitmap img, int degree) {
        Matrix matrix = new Matrix();
        matrix.postRotate(degree);
        Bitmap rotatedImg = Bitmap.createBitmap(img, 0, 0, img.getWidth(), img.getHeight(), matrix, true);
        img.recycle();
        return rotatedImg;
    }

    private void loadUserPhoto() {
        LOGe(TAG, "loadUserPhoto-1");

        File cropFile = getFileName("Photo", 0);
        if (cropFile.exists()) {
            Bitmap image = BitmapFactory.decodeFile(cropFile.toString());
            bitmapMember = image;
            if(bitmapMember != null) {
                roundedimageview_ActMyCarMain_Photo.setImageBitmap(bitmapMember);
            }
        }
    }

    private void showUserNameAlert() {
        final AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle(getString(R.string.Name));
        alert.setMessage(getString(R.string.InsertName));
        final EditText editText_ShowSerial_SerialNo = new EditText(this);
        alert.setView(editText_ShowSerial_SerialNo);
        alert.setPositiveButton(getString(R.string.OK), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                String aUserName = editText_ShowSerial_SerialNo.getText().toString();

                textView_ActMain_UserName.setText(aUserName);

                SharedPreferences pref = getSharedPreferences(getString(R.string.project_name), Activity.MODE_PRIVATE);
                SharedPreferences.Editor ePref = pref.edit();
                ePref.putString("userName", aUserName);
                ePref.commit();
            }
        });
        alert.setNegativeButton(getString(R.string.CANCEL),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        // Canceled.
                    }
                });
        alert.show();
    }

    private void showBirthDayAlert() {
        int year, month, day;

        Locale locale = getResources().getConfiguration().locale;
        Locale.setDefault(locale);

        GregorianCalendar calendar = new GregorianCalendar();
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH);
        day= calendar.get(Calendar.DAY_OF_MONTH);
//        hour = calendar.get(Calendar.HOUR_OF_DAY);
//        minute = calendar.get(Calendar.MINUTE);
        new DatePickerDialog(MainActivity.this, dateSetListener, year, month, day).show();
    }

    private DatePickerDialog.OnDateSetListener dateSetListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            String aBirthDay = String.format("%d / %d / %d", year, monthOfYear+1, dayOfMonth);
            textView_ActMain_BirthDay.setText(aBirthDay);
            try {
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                Date dateBirth = sdf.parse(String.format("%04d-%02d-%02d", year, monthOfYear+1, dayOfMonth));
                textView_ActMain_BirthFrom.setText(String.format("%d", PHLib.getDaysFromDateToDate(dateBirth, new Date())));
            } catch (ParseException e) {
                e.printStackTrace();
            }

            SharedPreferences pref = getSharedPreferences(getString(R.string.project_name), Activity.MODE_PRIVATE);
            SharedPreferences.Editor ePref = pref.edit();
            ePref.putInt("birthYear", year);
            ePref.putInt("birthMon", monthOfYear);
            ePref.putInt("birthDay", dayOfMonth);
            ePref.commit();
        }
    };

    private void setGender(int a_Gender) {
        if (a_Gender == 0) {
            imageButton_ActMain_Male.setImageResource(R.drawable.main_btn_m_ov);
            imageButton_ActMain_Female.setImageResource(R.drawable.main_btn_f);

        } else if (a_Gender == 1) {
            imageButton_ActMain_Male.setImageResource(R.drawable.main_btn_m);
            imageButton_ActMain_Female.setImageResource(R.drawable.main_btn_f_ov);
        }

        SharedPreferences pref = getSharedPreferences(getString(R.string.project_name), Activity.MODE_PRIVATE);
        SharedPreferences.Editor ePref = pref.edit();
        ePref.putInt("gender", a_Gender);
        ePref.commit();
    }

    private View.OnClickListener onClickMenu = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            if(v.getId() == R.id.imagebutton_ActMyCarMain_PhotoDel) {
                bitmapMember = null;
                roundedimageview_ActMyCarMain_Photo.setImageBitmap(null);

                File cropFile = getFileName("Crop", 0);
                if (cropFile.exists()) {
                    cropFile.delete();
                }

                File photoFile = getFileName("Photo", 0);
                if (photoFile.exists()) {
                    photoFile.delete();
                }

            } else if(v.getId() == R.id.imagebutton_ActMyCarMain_PhotoReg) {
                //사진등록
                showDialogPhoto();

            } else if(v.getId() == R.id.linearLayout_ActMain_UserName) {
                showUserNameAlert();

            } else if(v.getId() == R.id.linearLayout_ActMain_BirthDay) {
                showBirthDayAlert();

            } else if(v.getId() == R.id.imageButton_ActMain_Male) {
                setGender(0);

            } else if(v.getId() == R.id.imageButton_ActMain_Female) {
                setGender(1);

            } else if(v.getId() == R.id.linearLayout_ActMain_Operation) {
                if (PHCommon.bRecvProgramCnt && !PHCommon.bProcessingProgramData) {
                    createOperationActivity();
                }

            } else if(v.getId() == R.id.linearLayout_ActMain_Program) {
//                if (PHCommon.bRecvProgramCnt && !PHCommon.bProcessingProgramData) {
                    createProgramActivity();
//                }

            } else if(v.getId() == R.id.linearLayout_ActMain_Statistic) {
                createStatisticActivity();

            } else if(v.getId() == R.id.linearLayout_ActMain_BLEStatus) {
                LOGe(TAG, "showBLEScan onClickMenu");

                if (mPeripheral != null && mPeripheral.getConnectionState() == TIOConnection.STATE_CONNECTED) {
                    PHLib.showYesNoDialog(getString(R.string.AlreadyConnected), MainActivity.this, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            doDisconnectPeripheral();
                        }
                    });
                } else {
                    showBLEScan();
                }

            } else if(v.getId() == R.id.imageButton_ActMain_Settings) {
                showSettingsEdit();

            } else if(v.getId() == R.id.imageButton_ActMain_Help) {
                Intent intent = new Intent(MainActivity.this, HelpActivity.class);
                startActivity(intent);
            }
        }
    };

    private void createOperationActivity() {
        LOGe(TAG, "createOperationActivity-1");
        bActivityShow = false;
        Intent intent = new Intent(this, Menu01Activity.class);
        startActivity(intent);
    }

    private void createProgramActivity() {
        bActivityShow = false;
        Intent intent = new Intent(this, Menu02Activity.class);
        startActivity(intent);
    }

    private void createStatisticActivity() {
        bActivityShow = false;
        Intent intent = new Intent(this, Menu03Activity.class);
        startActivity(intent);
    }

    private void saveBabyPhoto(Uri a_Uri) {
        try {
            final InputStream imageStream = getContentResolver().openInputStream(a_Uri);
            final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);

            String aFilePath = Environment.getExternalStorageDirectory().getAbsolutePath();
            File dir = new File(aFilePath,  "bt150");
            if(! dir.isDirectory()) {
                dir.mkdirs();
            }
            String fname = "photo0.jpg";
            File file = new File(dir, fname);
            Log.i(TAG, "" + file);
            if (file.exists())
                file.delete();
            try {
                FileOutputStream out = new FileOutputStream(file);
                selectedImage.compress(Bitmap.CompressFormat.JPEG, 90, out);
                out.flush();
                out.close();
            } catch (Exception e) {
                e.printStackTrace();
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        loadUserPhoto();
    }

    private void showDialogPhoto() {

        final Dialog dialog = new Dialog(this, R.style.myDialog);
        LayoutInflater factory = LayoutInflater.from(this);
        int layoutId = R.layout.co_photopopup;
        View textEntryView = factory.inflate(layoutId, null);
        dialog.setContentView(textEntryView);
        Button buttonMyInfo_Album = (Button) textEntryView.findViewById(R.id.button_MyInfo_Album);
        buttonMyInfo_Album.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createAlbumActivity();
                dialog.cancel();
            }
        });
        Button buttonMyInfo_Camera = (Button) textEntryView.findViewById(R.id.button_MyInfo_Camera);
        buttonMyInfo_Camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createCameraActivity();
                dialog.cancel();
            }
        });
        Button buttonMyInfo_Cancel = (Button) textEntryView.findViewById(R.id.button_MyInfo_Cancel);
        buttonMyInfo_Cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
            }
        });


//        Resources res = getVCmdResource(this, PHCommon.curSpeechLanguage);

        TextView textView_MyInfo_Title = (TextView) textEntryView.findViewById(R.id.textView_MyInfo_Title);

        textView_MyInfo_Title.setText(getString(R.string.select_photo));
        buttonMyInfo_Album.setText(getString(R.string.select_photo_album));
        buttonMyInfo_Camera.setText(getString(R.string.select_photo_camera));
        buttonMyInfo_Cancel.setText(getString(R.string.select_photo_cancel));

        dialog.show();
    }

    private void createCameraActivity() {
//        bHoldRefresh = true;
//        photoImageUri = Uri.fromFile(getFileName("Photo", 0));
        photoImageUri = getFileUrl("photo0.jpg");
        // 카메라를 호출합니다.
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        intent.putExtra(android.provider.MediaStore.EXTRA_OUTPUT, photoImageUri);
//        startActivityForResult(intent, RESULT_LOAD_CAMERA);
        startActivityForResult(intent, CROP_FROM_CAMERA);
    }

    private void createAlbumActivity() {
//        bHoldRefresh = true;
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        intent.setType(android.provider.MediaStore.Images.Media.CONTENT_TYPE);
        startActivityForResult(intent, RESULT_LOAD_ALBUM);
//        startActivityForResult(intent, CROP_FROM_CAMERA);
    }

    private Uri getFileUrl() {
        return getFileUrl("crop0.jpg");
    }

    private Uri getFileUrl(String a_Gbn) {
        File aFile;
        String aFilePath = Environment.getExternalStorageDirectory().getAbsolutePath();
        File dir = new File(aFilePath,  "bt150");
        if(! dir.isDirectory()) {
            dir.mkdirs();
        }
		LOGe(TAG, "getFileUrl : " + aFilePath);

        aFile = new File(dir, a_Gbn);
        Uri fileUri = FileProvider.getUriForFile(this, getPackageName() + ".provider", aFile);

        LOGe(TAG, "getFileUrl:" + fileUri.getPath());

        return fileUri;
    }

    private File getFileName(String imageGbn, int imageNumber) {
        String imageName = "";

        if (imageGbn.matches("Crop")) {
            imageName = getResources().getString(R.string.imageuri_crop, String.valueOf(imageNumber));
        } else if (imageGbn.matches("Photo")) {
            imageName = getResources().getString(R.string.imageuri_photo, String.valueOf(imageNumber));
        } else {
            imageName = getResources().getString(R.string.imageuri_photo, String.valueOf(imageNumber));
        }
        File file = new File(Environment.getExternalStorageDirectory(), imageName);

        try {
            file.createNewFile();
//            Log.w(TAG,"imageName:" + imageName );
        } catch (Exception e) {
//            Log.e(TAG, "fileCreation fail");
        }
        return file;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data){

        if(resultCode != RESULT_OK) {
            return ;
        }

        LOGe(TAG, "requestCode:" + requestCode);
        switch(requestCode) {
            case ENABLE_BT_REQUEST_ID:
                if (resultCode == Activity.RESULT_CANCELED) {
                    PHLib.showConfirmDialog(getResources().getString(R.string.error_bluetooth_not_supported), this, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            PHLib.killApp(MainActivity.this);
                        }
                    });
                    return;
                }
                break;

            case CROP_FROM_CAMERA: {
                resizeNSaveBitmap();
                loadUserPhoto();
//                if (photoImageUri != null) {
//                    File f = new File(photoImageUri.getPath());
//                    if (f.exists()) {
//                        f.delete();
//                    }
//                }
                break;
            }
            case RESULT_LOAD_ALBUM: {
                try {
                    final Uri imageUri = data.getData();
                    final InputStream imageStream = getContentResolver().openInputStream(imageUri);
                    final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);

                    Bitmap aNewBitmap = null;
                    ExifInterface ei = null;
                    try {
//                        ei = new ExifInterface(aFilePath);
                        ei = new ExifInterface(PHLib.getGalleryBitmapPath(imageUri, MainActivity.this));
                        int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
                        LOGe(TAG, "RESULT_LOAD_ALBUM : orientation: " + orientation);

                        switch (orientation) {
                            case ExifInterface.ORIENTATION_ROTATE_90:
                                aNewBitmap = rotateImage(selectedImage, 90);
                                break;
                            case ExifInterface.ORIENTATION_ROTATE_180:
                                aNewBitmap = rotateImage(selectedImage, 180);
                                break;
                            case ExifInterface.ORIENTATION_ROTATE_270:
                                aNewBitmap = rotateImage(selectedImage, 270);
                                break;
                            default:
                                aNewBitmap = selectedImage;
                                break;
                        }

                    } catch (IOException e) {
                        e.printStackTrace();
                    }


                    String aFilePath = Environment.getExternalStorageDirectory().getAbsolutePath();
                    File dir = new File(aFilePath,  "bt150");
                    if(! dir.isDirectory()) {
                        dir.mkdirs();
                    }
                    String fname = "photo0.jpg";
                    File file = new File(dir, fname);
                    Log.i(TAG, "" + file);

                    if (file.exists())
                        file.delete();
                    try {
                        FileOutputStream out = new FileOutputStream(file);
                        aNewBitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
                        out.flush();
                        out.close();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } catch (FileNotFoundException e) {
                    e.printStackTrace();
//                    Toast.makeText(PostImage.this, "Something went wrong", Toast.LENGTH_LONG).show();
                }
                loadUserPhoto();
                break;
            }
            case RESULT_LOAD_CAMERA: {
                try {

//                    cropImageUri = Uri.fromFile(getFileName("Crop", 0));
                    cropImageUri = getFileUrl();
                    LOGe(TAG, "RESULT_LOAD_CAMERA-1:" + cropImageUri.getPath());

//                    photoImageUri = Uri.fromFile(getFileName("Photo", 0));
                    LOGe(TAG, "RESULT_LOAD_CAMERA-2:" + photoImageUri.getPath());
                    Uri aModUrl = Uri.fromFile(getFileName("Photo", 0));

                    Rect rectImage = PHLib.getResizeDimen(aModUrl, 640, false, MainActivity.this);
//                    Rect rectImage = PHLib.getResizeDimen(photoImageUri, 640, false, MainActivity.this);
                    ExifInterface exif = null;     //Since API Level 5
                    try {
//                        exif = new ExifInterface(photoImageUri.getPath());
                        exif = new ExifInterface(aModUrl.getPath());
                    } catch (IOException e) {
//                        e.printStackTrace();
                    }

                    LOGe(TAG, "RESULT_LOAD_CAMERA-3");

                    String exifOrientation = exif.getAttribute(ExifInterface.TAG_ORIENTATION);
//                    Log.w(TAG, "exifOrientation:" + exifOrientation);

                    double fRatioX = 1;
                    double fRatioY = 1;
                    int nOutX = rectImage.width();
                    int nOutY = rectImage.height();

                    double fRatio = rectImage.width()/rectImage.height();
                    if (rectImage.width() > rectImage.height()) {
                        fRatio = rectImage.height()/rectImage.width();
                    }

                    if (exifOrientation.equals("6") || exifOrientation.equals("8")) {
                        fRatioX = fRatio;
                        nOutX = rectImage.height();
                        nOutY = rectImage.width();
                    } else {
                        fRatioY = fRatio;
                        nOutX = rectImage.width();
                        nOutY = rectImage.height();
                    }
                    Log.w(TAG, "fRatioX:" + fRatioX + " fRatioY:" + fRatioY);
                    Log.w(TAG, "nOutX:" + nOutX + " nOutY:" + nOutY);

                    LOGe(TAG, "RESULT_LOAD_CAMERA-4");


                    Intent intent = new Intent("com.android.camera.action.CROP");
                    intent.setDataAndType(photoImageUri, "image/*");
                    intent.putExtra("outputX", nOutX);
                    intent.putExtra("outputY", nOutY);
                    intent.putExtra("aspectX", fRatioX);
                    intent.putExtra("aspectY", fRatioY);
                    intent.putExtra("scale", true);
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, cropImageUri);

                    if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.LOLLIPOP) {
//                        intent.setClipData(ClipData.newRawUri("", cropImageUri));
                        intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION|Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    }

                    intent.putExtra("outputFormat", Bitmap.CompressFormat.JPEG.toString());
                    startActivityForResult(intent, CROP_FROM_CAMERA);

                    LOGe(TAG, "RESULT_LOAD_CAMERA-5");

                    break;
                } catch (Exception e) {
//                    e.printStackTrace();
                }
            }
        }
    }

    private void showBLEScan() {
//        LOGe(TAG, "showBLEScan-1");
        if (dlgBLEScan != null && dlgBLEScan.isShowing()) {
            dlgBLEScan.cancel();
            dlgBLEScan = null;
        }

        dlgBLEScan = new Dialog(this, R.style.fullDialog);
        LayoutInflater factory = LayoutInflater.from(this);
        final View vPopDlg = factory.inflate(R.layout.pop_ble_scan, null);
        dlgBLEScan.setContentView(vPopDlg);
        dlgBLEScan.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

        textView_PopBle_Scan = (TextView) vPopDlg.findViewById(R.id.textView_PopBle_Scan);
        linearLayout_PopBle_Scan = (LinearLayout)vPopDlg.findViewById(R.id.linearLayout_PopBle_Scan);
        mPeripheralsListView = (ListView) vPopDlg.findViewById(R.id.listView_PopBle_Scan);

        button_PopBle_Scan = (Button) vPopDlg.findViewById(R.id.button_PopBle_Scan);
        button_PopBle_Scan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startTimedScan();
            }
        });

        ImageButton imageButton_PopBle_Close = (ImageButton) vPopDlg.findViewById(R.id.imageButton_PopBle_Close);
        imageButton_PopBle_Close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dlgBLEScan.cancel();

            }
        });
        dlgBLEScan.show();

        PHCommon.curDevSearchTime = 0;

        initializePeripheralsListView();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                startTimedScan();
            }
        }, 500);
    }

    private void cancelScan() {
        if (mTimerBleScan != null) {
            mTimerBleScan.cancel();
            mTimerBleScan.purge();
            mTimerBleScan = null;
        }

        try {
            mTio.stopScan();
        } catch (InvalidObjectException e) {
            e.printStackTrace();
        }
    }

    private void startTimedScan() {

        mTio.removeAllPeripherals();
        updatePeripheralsListView();

        if (mTimerBleScan != null) {
            mTimerBleScan.cancel();
            mTimerBleScan.purge();
            mTimerBleScan = null;
        }

        taskBleScan = new TimerTask() {
            public void run() {

                try {
                    if (mConnection != null && mConnection.getConnectionState() == TIOConnection.STATE_CONNECTED) {
                        if (mTimerBleScan != null) {
                            mTimerBleScan.cancel();
                            mTimerBleScan.purge();
                            mTimerBleScan = null;
                        }

                        try {
                            mTio.stopScan();
                        } catch (InvalidObjectException e) {
                            e.printStackTrace();
                        }

                    } else {
                        mTio.startScan(MainActivity.this);
                    }
//                mTio.startScan(MainActivity.this);
                } catch (Exception e) {
                    LOGe(TAG, "taskBleScan exception:" + e.getMessage());
                }
            }
        };
        mTimerBleScan = new Timer();
        mTimerBleScan.schedule(taskBleScan, 500, SCAN_INTERVAL);
//        mTimerBleScan.schedule(taskBleScan, 500);

        textView_PopBle_Scan.setText(getString(R.string.DeviceSearching) + "....");
    }

    private void updateConnectionState(final String a_Status) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                textView_ActMain_BLEStatus.setText(a_Status);
            }
        });
    }

    private boolean connectPeripheral(TIOPeripheral peripheral) {

        // retrieve peripheral instance from TIOManager
        mPeripheral = TIOManager.getInstance().findPeripheral(peripheral.getAddress());

        if ( mPeripheral != null) {
            mConnection = mPeripheral.getConnection();

            // register callback
            try {
                SharedPreferences pref = getSharedPreferences(getString(R.string.project_name), Activity.MODE_PRIVATE);
                SharedPreferences.Editor ePref = pref.edit();
                ePref.putString("device_addr", peripheral.getAddress());
                ePref.putString("device_name", peripheral.getName());
                ePref.commit();

                // display peripheral properties
                updateConnectionState(mPeripheral.getName() + " Connected!");

                LOGe(TAG, "mPeripheral.getAddress():" + mPeripheral.getAddress());
                if ( mPeripheral.getAdvertisement() != null ) {
                    LOGe(TAG, "mPeripheral.getAdvertisement().toString():" + mPeripheral.getAdvertisement().toString());
                }
                return true;
            } catch (Exception ex ){
                Log.e(TAG,"! Connect to peripheral failed, "+ ex.toString());
            }
        }
        return false;
    }

    private void updateUIState() {
        Log.d(TAG, "updateUIState");

        boolean isConnected = false;
        boolean isConnecting = false;

        if ( mConnection != null ) {
            isConnected  = mConnection.getConnectionState() == TIOConnection.STATE_CONNECTED;
            isConnecting = mConnection.getConnectionState() == TIOConnection.STATE_CONNECTING;
        }

        if (isConnected) {
            updateConnectionState(mPeripheral.getName() + getString(R.string.isConnected) + "!");
        } else if (isConnecting) {
//            updateConnectionState(mPeripheral.getName() + " Connecting!");
        } else {
            updateConnectionState(getString(R.string.DeviceIsNotConnected));
        }

    }

    @Override
    public void onConnected(TIOConnection connection) {
        Log.d(TAG, "onConnected");

        cancelScan();

        if((mConnectingDialog != null) && mConnectingDialog.isShowing()) {
            mConnectingDialog.dismiss();
        };

//        if ( mPeripheral.getAdvertisement() != null) {
//            mSubTitleView.setText(mPeripheral.getAdvertisement().toString());
//        }

        PHCommon.nOperationModeCur = PHCommon.OP_MODE_NONE;
        PHCommon.nExpressionTimeS = 0;
        PHCommon.nElapsedTime = 0;
        PHCommon.bProgramDataLoaded = false;
        PHCommon.gRecvData = "";
        PHCommon.gRecvBattery = "";
        PHCommon.bDeviceWakeUp = true;

        updateUIState();

        if (bFWUpMode) {
            showFWUpgradeMessage();
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    doFWUpgrade();
                }
            }, 100);

        } else {
            startRssiListener();
            startCheckDeviceStatusTimer();
        }

        if (!mPeripheral.shallBeSaved()) {
            // save if connected for the first time
            mPeripheral.setShallBeSaved(true);
            TIOManager.getInstance().savePeripherals();
        }
    }

    @Override
    public void onConnectFailed(TIOConnection connection, String errorMessage) {
        LOGe(TAG, "onConnectFailed " + errorMessage);

        if((mConnectingDialog != null) && mConnectingDialog.isShowing()) { mConnectingDialog.dismiss(); };

        mErrorMessage = errorMessage;
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                updateUIState();

                stopDroidSpeech();

                PHLib.showMessageDialog(getString(R.string.failConnectDevice), MainActivity.this);

                if (mErrorMessage.length() > 0) {
                    LOGe(TAG, "Failed to connect : " + mErrorMessage);
//                    PHLib.showMessageDialog("Failed to connect : " + mErrorMessage, MainActivity.this);
                }
            }
        };
        runOnUiThread( runnable );
    }

    @Override
    public void onDisconnected(TIOConnection connection, String errorMessage) {
        LOGe(TAG, "onDisconnected" + errorMessage);

        if((mConnectingDialog != null) && mConnectingDialog.isShowing()) {
            mConnectingDialog.dismiss();
        };

        stopDroidSpeech();

        stopRssiListener();

        mErrorMessage = errorMessage;

        Runnable runnable = new Runnable() {
            @Override
            public void run() {

                if (PHCommon.nOperationModeCur == PHCommon.OP_MODE_EXP) {
                    saveExpressionTime();
                }

                PHCommon.nOperationModeCur = PHCommon.OP_MODE_NONE;
                PHCommon.bDeviceWakeUp = false;

                updateUIState();

                Toast.makeText(MainActivity.this, getString(R.string.DeviceDisconnected), Toast.LENGTH_SHORT).show();

                PHCommon.bProgramDataLoaded = false;
                if (bActivityShow) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            showBLEScan();
                        }
                    });
                } else {
                    goBackToMain();
                }

                if (mErrorMessage.length() > 0) {
                    LOGe(TAG, "Disconnected with error message: " + mErrorMessage);
//                    PHLib.showMessageDialog("Disconnected with error message: " + mErrorMessage, MainActivity.this);
                }

            }
        };
        runOnUiThread(runnable);
    }

    private void goBackToMain() {
        bFirst = true;

        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_SINGLE_TOP);
        Bundle bundle = new Bundle();
        intent.putExtras(bundle);
        startActivity(intent);
    }

    private void parseProgramData(String a_Data) {
        LOGe(TAG, "parseProgramData:" + a_Data);

        int aIdx = PHLib.convertToInt(a_Data.substring(1, 2));
        int aSeq = PHLib.convertToInt(a_Data.substring(3, 4));

        ArrayList<ProgramData> aData = PHCommon.arrProgramData[aIdx];
        if(aData == null) {
            return ;
        }

        ProgramData programData = aData.get(aSeq);

//        int aEnd = PHLib.convertToInt(a_Data.substring(17, 18));
//        if (aEnd == 1) {
//            return;
//        }

        programData.nMode = PHLib.convertToInt(a_Data.substring(5, 6));
        programData.nPressure = PHLib.convertToInt(a_Data.substring(7, 9));
        programData.nCycle = PHLib.convertToInt(a_Data.substring(10, 11));
        programData.nLen = PHLib.convertToInt(a_Data.substring(12, 16));
        programData.bDownloaded = true;

        if (programData.nLen == 0) {
            programData.initProgramData();
        }
    }

    @Override
    public void onDataReceived(TIOConnection connection, byte[] data) {

        String aRecv = "";

        try {
            aRecv = new String(data);
            LOGe(TAG, "onDataReceived:" + aRecv);
        } catch (Exception ex) {
            LOGe(TAG, ex.getMessage());
        }

        if (bFWUpMode) {
            LOGe(TAG, "onDataReceived:" + PHLib.buildStringFromBytes(data, data.length));
//            sCmdFWRecv = aRecv;
            sCmdFWRecv = data[0];
            LOGe(TAG, "sCmdFWRecv:" + sCmdFWRecv);
            return ;
        }

        if (!aRecv.equals(PHCommon.gRecvData)) {
//            LOGe(TAG, "onNew Data:" + aRecv);

            PHCommon.gRecvData = aRecv;

            if (aRecv.length() == 6 && aRecv.substring(0, 1).equals("T")) {
                PHCommon.nElapsedTime = PHLib.convertToInt(aRecv.substring(1, 5));

            } else if ( aRecv.length() == 17
                    && aRecv.substring(0, 1).equals("I")
                    && aRecv.substring(2, 3).equals("S")
                    && aRecv.substring(4, 5).equals("M")
                    && aRecv.substring(6, 7).equals("P")
                    && aRecv.substring(9, 10).equals("C")
                    && aRecv.substring(11, 12).equals("T")) {

                LOGe(TAG, "### onDataReceived Main parseProgramData:" + aRecv);

                parseProgramData(aRecv);

            } else if ( aRecv.length() == 3
                    && aRecv.substring(0, 1).equals("V")) {

                Intent intent = new Intent(PHCommon.MSG_DATA_RECV);
                intent.putExtra("recv_battery", aRecv.substring(1, 2));
                sendBroadcast(intent);
                return ;

            } else if ( aRecv.length() == 10
                    && aRecv.substring(0, 1).equals("A")) {

                //Asssswwww;
                nPrgReqCnt5 = PHLib.convertToInt(aRecv.substring(1, 2));
                nPrgReqCnt6 = PHLib.convertToInt(aRecv.substring(2, 3));
                nPrgReqCnt7 = PHLib.convertToInt(aRecv.substring(3, 4));
                nPrgReqCnt8 = PHLib.convertToInt(aRecv.substring(4, 5));

                if (nPrgReqCnt5 > 0) {
                    nPrgOpType5 = PHLib.convertToInt(aRecv.substring(5, 6));
                }
                if (nPrgReqCnt6 > 0) {
                    nPrgOpType6 = PHLib.convertToInt(aRecv.substring(6, 7));
                }
                if (nPrgReqCnt7 > 0) {
                    nPrgOpType7 = PHLib.convertToInt(aRecv.substring(7, 8));
                }
                if (nPrgReqCnt8 > 0) {
                    nPrgOpType8 = PHLib.convertToInt(aRecv.substring(8, 9));
                }


                LOGe(TAG, "onDataReceived: nPrgOpType5:" + nPrgOpType5 + " nPrgOpType6:" + nPrgOpType6 + " nPrgOpType7:" + nPrgOpType7 + " nPrgOpType8:" + nPrgOpType8);
                LOGe(TAG, "onDataReceived: nPrgReqCnt5:" + nPrgReqCnt5 + " nPrgReqCnt6:" + nPrgReqCnt6 + " nPrgReqCnt7:" + nPrgReqCnt7 + " nPrgReqCnt8:" + nPrgReqCnt8);

                PHCommon.bRecvProgramCnt = true;

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        PHCommon.goBackToMain(MainActivity.this);
                        inQueryProgramData();
                    }
                });

            } else if (aRecv.length() >= 14
                    && aRecv.substring(0, 1).equals("M")
                    && aRecv.substring(2, 3).equals("P")
                    && aRecv.substring(5, 6).equals("C")
                    && aRecv.substring(7, 8).equals("R")) {

                String aRun = aRecv.substring(8, 9);

                int aNo = PHLib.convertToInt(aRecv.substring(10, 11));
                int aSeq = PHLib.convertToInt(aRecv.substring(12, 13));

                if (aRun.equals("X") || aRun.equals("x")) {
                    PHCommon.bDeviceWakeUp = false;
                } else {
                    PHCommon.bDeviceWakeUp = true;
                }

                if (aRun.equals("2")) {
                    PHCommon.bProgramRunning = true;
                } else {
                    PHCommon.bProgramRunning = false;
                }

                LOGe(TAG, "createOperationActivity-1: a_Recv:" + aRecv + "running:" + PHCommon.bProgramRunning);
                LOGe(TAG, "nOperationType:" + nOperationType);

                if (PHCommon.bRecvProgramCnt && !PHCommon.bProgramRunning && isOperatingConditionChanged(aRecv)) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            PHCommon.createOperationActivity(MainActivity.this);
                        }
                    });

                } else {
                    if (PHCommon.bProgramRunning) {
                        if (PHCommon.nActivityNo != ACT_MENU02) {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    PHCommon.createProgramActivity(MainActivity.this);
                                }
                            });
                        }
                    }
                }

                int aOperationMode = PHLib.convertToInt(aRecv.substring(1, 2));
                if (aOperationMode == PHCommon.OP_MODE_EXP && PHCommon.nOperationModeCur != PHCommon.OP_MODE_EXP) {
                    PHCommon.nExpressionTimeS = PHCommon.nElapsedTime;

                } else if (aOperationMode != PHCommon.OP_MODE_EXP && PHCommon.nOperationModeCur == PHCommon.OP_MODE_EXP) {
                    saveExpressionTime();
                }

                PHCommon.nOperationModeCur = aOperationMode;

                nOperationMode = aOperationMode;

                if (aRecv.length() > 15 && aRecv.substring(13, 14).equals("W")) {
                    String aOpType = aRecv.substring(14, 15);
                    if (aOpType.equals("0")) {
                        nOperationType = OP_TYPE_SINGLE;
                    } else {
                        nOperationType = OP_TYPE_DOUBLE;
                    }
                }

            }

            Intent intent = new Intent(PHCommon.MSG_DATA_RECV);
            intent.putExtra("recv_data", aRecv);
            sendBroadcast(intent);
        }
    }

    private boolean isOperatingConditionChanged(String a_Recv) {

//        boolean aRes = false;

        int aOperationMode = PHLib.convertToInt(a_Recv.substring(1, 2));
//        if (aOperationMode != nOperationMode) {
//            return true;
//        }

        if (a_Recv.length() > 15 && a_Recv.substring(13, 14).equals("W")) {
            int aOpType = PHLib.convertToInt(a_Recv.substring(14, 15));
            if (aOpType != nOperationType) {
                return true;
            }
        }

        if (aOperationMode == PHCommon.OP_MODE_EXP) {
            int aPressureExp = PHLib.convertToInt(a_Recv.substring(3, 5));
            int aCycleExp = PHLib.convertToInt(a_Recv.substring(6, 7));

            if (aPressureExp != nPressureExp) {

            }

        } else if (aOperationMode == PHCommon.OP_MODE_MSG) {
            int aPressureMsg = PHLib.convertToInt(a_Recv.substring(3, 5));
            int aCycleMsg = PHLib.convertToInt(a_Recv.substring(6, 7));
        }

        return  false;
    }


    private void saveExpressionTime() {
        int aElapsed = PHCommon.nElapsedTime - PHCommon.nExpressionTimeS;
        LOGe(TAG, "saveExpressionTime: aElapsed:" + aElapsed);
        if (aElapsed > 0) {
            Date aDate = new Date(System.currentTimeMillis());
            long aLogTime = aDate.getTime() -  aElapsed * 1000;

            SimpleDateFormat aSdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

            LOGe(TAG, "saveExpressionTime: aElapsed:" + aElapsed + " Time:" + aSdf.format(aLogTime));

            DBHelper dbHelper = new DBHelper(this);
            dbHelper.setMilkData(aLogTime, 0, 0, aElapsed);

        }
    }

    @Override
    public void onDataTransmitted(TIOConnection connection,int status, int bytesTransferred) {

    }


    @Override
    public void onReadRemoteRssi(TIOConnection connection,int status, int rssi) {
//        LOG(TAG, "onRemoteRssi " + rssi);
//        mRssi = rssi;
//        Runnable runnable = new Runnable() {
//            @Override
//            public void run() {
//                mRssiView.setText(Integer.toString(mRssi));
//            }
//        };
//        runOnUiThread(runnable);
    }


    //@Override
    public void  onLocalUARTMtuSizeUpdated(TIOConnection connection, int mtuSize) {
//        LOGe(TAG,"Local MTU Size = " + mtuSize);
//        mLocalUARTMtuSize = mtuSize;
//        mMtuReceiveSizeTextView.setText(String.format("MTU %d", mLocalUARTMtuSize));
    }


    //@Override
    public void  onRemoteUARTMtuSizeUpdated(TIOConnection connection, int mtuSize) {
//        Log.d(TAG,"Remote MTU Size = " + mtuSize);
//        mRemoteUARTMtuSize = mtuSize;
//        mMtuSendSizeTextView.setText(String.format("MTU %d", mRemoteUARTMtuSize));
    }

    private CheckBox checkBox_PopSettingsEdit_ML;
    private CheckBox checkBox_PopSettingsEdit_OZ;

    private CheckBox checkBox_PopSettingsEdit_VCmdUseOnline;
    private CheckBox checkBox_PopSettingsEdit_VCmdUseOffline;
    private CheckBox checkBox_PopSettingsEdit_VCmdResultShow;
    private CheckBox checkBox_PopSettingsEdit_VCmdResultHide;
    private CheckBox checkBox_PopSettingsEdit_VCmdEnable;
    private CheckBox checkBox_PopSettingsEdit_VCmdDisable;

    private LinearLayout layout_PopSettingsEdit_VCmdGroup;


    private View.OnClickListener onClickSettings = new View.OnClickListener() {
        @Override
        public void onClick(View view) {

            if (view.getId() == R.id.checkBox_PopSettingsEdit_ML) {
                checkBox_PopSettingsEdit_ML.setChecked(true);
                checkBox_PopSettingsEdit_OZ.setChecked(false);
                PHCommon.setUnit("ml", MainActivity.this);
                return;
            } else if (view.getId() == R.id.checkBox_PopSettingsEdit_OZ) {
                checkBox_PopSettingsEdit_ML.setChecked(false);
                checkBox_PopSettingsEdit_OZ.setChecked(true);
                PHCommon.setUnit("oz", MainActivity.this);
                return;
            }

            if (view.getId() == R.id.checkBox_PopSettingsEdit_VCmdUseOnline) {
                checkBox_PopSettingsEdit_VCmdUseOnline.setChecked(true);
                checkBox_PopSettingsEdit_VCmdUseOffline.setChecked(false);
                PHCommon.bVCmdUseOnline = true;
                droidSpeech.setOfflineSpeechRecognition(!PHCommon.bVCmdUseOnline);

            } else if (view.getId() == R.id.checkBox_PopSettingsEdit_VCmdUseOffline) {
                checkBox_PopSettingsEdit_VCmdUseOnline.setChecked(false);
                checkBox_PopSettingsEdit_VCmdUseOffline.setChecked(true);
                PHCommon.bVCmdUseOnline = false;
                droidSpeech.setOfflineSpeechRecognition(!PHCommon.bVCmdUseOnline);

            } else if (view.getId() == R.id.checkBox_PopSettingsEdit_VCmdResultShow) {
                checkBox_PopSettingsEdit_VCmdResultShow.setChecked(true);
                checkBox_PopSettingsEdit_VCmdResultHide.setChecked(false);
                PHCommon.bVCmdShowResult = true;

            } else if (view.getId() == R.id.checkBox_PopSettingsEdit_VCmdResultHide) {
                checkBox_PopSettingsEdit_VCmdResultShow.setChecked(false);
                checkBox_PopSettingsEdit_VCmdResultHide.setChecked(true);
                PHCommon.bVCmdShowResult = false;

            } else if (view.getId() == R.id.checkBox_PopSettingsEdit_VCmdEnable) {
                checkBox_PopSettingsEdit_VCmdEnable.setChecked(true);
                checkBox_PopSettingsEdit_VCmdDisable.setChecked(false);
                PHCommon.bVCmdEnabled = true;

            } else if (view.getId() == R.id.checkBox_PopSettingsEdit_VCmdDisable) {
                checkBox_PopSettingsEdit_VCmdEnable.setChecked(false);
                checkBox_PopSettingsEdit_VCmdDisable.setChecked(true);
                PHCommon.bVCmdEnabled = false;
            }

            SharedPreferences pref = getSharedPreferences(getString(R.string.project_name), Activity.MODE_PRIVATE);
            SharedPreferences.Editor ePref = pref.edit();
            ePref.putBoolean("vcmd_useonline", PHCommon.bVCmdUseOnline);
            ePref.putBoolean("vcmd_showresult", PHCommon.bVCmdShowResult);
            ePref.putBoolean("vcmd_enabled", PHCommon.bVCmdEnabled);
            ePref.commit();

            if (!Locale.getDefault().getLanguage().equals("fr")) {
                if (PHCommon.bVCmdEnabled) {
                    startDroidSpeech();
                } else {
                    stopDroidSpeech();
                }
            }
        }
    };

    public void showSettingsEdit() {

        dlgSetttingsEdit = new Dialog(this, R.style.fullDialog);
        LayoutInflater factory = LayoutInflater.from(this);
        final View vPopDlg = factory.inflate(R.layout.pop_settings_edit, null);
        dlgSetttingsEdit.setContentView(vPopDlg);
        dlgSetttingsEdit.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

        checkBox_PopSettingsEdit_ML = (CheckBox) vPopDlg.findViewById(R.id.checkBox_PopSettingsEdit_ML);
        checkBox_PopSettingsEdit_OZ = (CheckBox) vPopDlg.findViewById(R.id.checkBox_PopSettingsEdit_OZ);

        checkBox_PopSettingsEdit_VCmdUseOnline = (CheckBox) vPopDlg.findViewById(R.id.checkBox_PopSettingsEdit_VCmdUseOnline);
        checkBox_PopSettingsEdit_VCmdUseOffline = (CheckBox) vPopDlg.findViewById(R.id.checkBox_PopSettingsEdit_VCmdUseOffline);
        checkBox_PopSettingsEdit_VCmdResultShow = (CheckBox) vPopDlg.findViewById(R.id.checkBox_PopSettingsEdit_VCmdResultShow);
        checkBox_PopSettingsEdit_VCmdResultHide = (CheckBox) vPopDlg.findViewById(R.id.checkBox_PopSettingsEdit_VCmdResultHide);
        checkBox_PopSettingsEdit_VCmdEnable = (CheckBox) vPopDlg.findViewById(R.id.checkBox_PopSettingsEdit_VCmdEnable);
        checkBox_PopSettingsEdit_VCmdDisable = (CheckBox) vPopDlg.findViewById(R.id.checkBox_PopSettingsEdit_VCmdDisable);

        layout_PopSettingsEdit_VCmdGroup = (LinearLayout) vPopDlg.findViewById(R.id.layout_PopSettingsEdit_VCmdGroup);

        checkBox_PopSettingsEdit_ML.setOnClickListener(onClickSettings);
        checkBox_PopSettingsEdit_OZ.setOnClickListener(onClickSettings);
        checkBox_PopSettingsEdit_VCmdUseOnline.setOnClickListener(onClickSettings);
        checkBox_PopSettingsEdit_VCmdUseOffline.setOnClickListener(onClickSettings);
        checkBox_PopSettingsEdit_VCmdResultShow.setOnClickListener(onClickSettings);
        checkBox_PopSettingsEdit_VCmdResultHide.setOnClickListener(onClickSettings);
        checkBox_PopSettingsEdit_VCmdEnable.setOnClickListener(onClickSettings);
        checkBox_PopSettingsEdit_VCmdDisable.setOnClickListener(onClickSettings);

        if (!isRecognitionAvailable) {
            vPopDlg.findViewById(R.id.layout_PopSettingsEdit_VCmd_Title).setVisibility(View.GONE);
            vPopDlg.findViewById(R.id.layout_PopSettingsEdit_VCmd_Title_Line).setVisibility(View.GONE);
            vPopDlg.findViewById(R.id.layout_PopSettingsEdit_VCmd_Language).setVisibility(View.GONE);
            vPopDlg.findViewById(R.id.layout_PopSettingsEdit_VCmd_Language_Line).setVisibility(View.GONE);
            vPopDlg.findViewById(R.id.layout_PopSettingsEdit_VCmd_UseOnline).setVisibility(View.GONE);
            vPopDlg.findViewById(R.id.layout_PopSettingsEdit_VCmd_UseOnline_Line).setVisibility(View.GONE);
            vPopDlg.findViewById(R.id.layout_PopSettingsEdit_VCmd_Result).setVisibility(View.GONE);
            vPopDlg.findViewById(R.id.layout_PopSettingsEdit_VCmd_Result_Line).setVisibility(View.GONE);
            vPopDlg.findViewById(R.id.layout_PopSettingsEdit_VCmd_Enable).setVisibility(View.GONE);
            vPopDlg.findViewById(R.id.layout_PopSettingsEdit_VCmd_Enable_Line).setVisibility(View.GONE);
        }

//        checkBox_PopSettingsEdit_ML.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                checkBox_PopSettingsEdit_ML.setChecked(true);
//                checkBox_PopSettingsEdit_OZ.setChecked(false);
//                PHCommon.setUnit("ml", MainActivity.this);
//            }
//        });
//        checkBox_PopSettingsEdit_OZ.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                checkBox_PopSettingsEdit_ML.setChecked(false);
//                checkBox_PopSettingsEdit_OZ.setChecked(true);
//                PHCommon.setUnit("oz", MainActivity.this);
//            }
//        });

        pickerViewLanguage = (CharacterPickerView) vPopDlg.findViewById(R.id.pickerViewLanguage);
        pickerViewLanguage.setPicker(arrVCmdLanguage);
        pickerViewLanguage.setPickerTextColor(0xffe98a65, 0xffc5c5c5, getResources().getColor(R.color.colorPrimary));
        pickerViewLanguage.setItemsVisible(5);

        pickerViewDevSearchTimeOut = (CharacterPickerView) vPopDlg.findViewById(R.id.pickerViewDevSearchTimeOut);
        pickerViewDevSearchTimeOut.setPicker(arrDevSearchTimeOut);
        pickerViewDevSearchTimeOut.setPickerTextColor(0xffe98a65, 0xffc5c5c5, getResources().getColor(R.color.colorPrimary));
        pickerViewDevSearchTimeOut.setItemsVisible(5);


        final TextView textView_PopSettingEdit_DevAddr = (TextView) vPopDlg.findViewById(R.id.textView_PopSettingEdit_DevAddr);
        LinearLayout linearLayout_PopSettingEdit_DevDel = (LinearLayout) vPopDlg.findViewById(R.id.linearLayout_PopSettingEdit_DevDel);
        linearLayout_PopSettingEdit_DevDel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                PHLib.showYesNoDialog(getString(R.string.wantToDelete), MainActivity.this, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        textView_PopSettingEdit_DevAddr.setText("");

                        SharedPreferences pref = getSharedPreferences(getString(R.string.project_name), Activity.MODE_PRIVATE);
                        SharedPreferences.Editor ePref = pref.edit();
                        ePref.putString("device_addr", "");
                        ePref.putString("device_name", "");
                        ePref.commit();
                    }
                });
            }
        });


        Button button_PopStatisticAdd_FWUpgrade = (Button) vPopDlg.findViewById(R.id.button_PopStatisticAdd_FWUpgrade);
        button_PopStatisticAdd_FWUpgrade.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PHLib.showYesNoDialog(getString(R.string.FWUpConfirm), getString(R.string.FWUpgrade),  MainActivity.this, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dlgSetttingsEdit.cancel();
                        bFWUpMode = true;

                        stopStatusTimer();
                        stopDroidSpeech();

                        if (mPeripheral != null && mPeripheral.getConnectionState() == TIOConnection.STATE_CONNECTED) {
                            doDisconnectPeripheral();
                        } else {
//                            showBLEScan();

                            //FOR TEST
                            showFWUpgradeMessage();
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    doFWUpgrade();
                                }
                            }, 100);
                        }
                    }
                });
            }
        });


        ImageButton imageButton_PopStatisticAdd_Close = (ImageButton) vPopDlg.findViewById(R.id.imageButton_PopStatisticAdd_Close);
        imageButton_PopStatisticAdd_Close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dlgSetttingsEdit.cancel();
            }
        });
        Button button_PopStatisticAdd_Cancel = (Button) vPopDlg.findViewById(R.id.button_PopStatisticAdd_Cancel);
        button_PopStatisticAdd_Cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dlgSetttingsEdit.cancel();
            }
        });
        Button button_PopStatisticAdd_Save = (Button) vPopDlg.findViewById(R.id.button_PopStatisticAdd_Save);
        button_PopStatisticAdd_Save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                int []arrLanguages =  pickerViewLanguage.getCurrentItems();
                switch (arrLanguages[0]) {
                    case 0:
                        changeVoiceCommadLanguage(VOICE_REC_LANG_KOREAN);
                        break;
                    case 1:
                        changeVoiceCommadLanguage(VOICE_REC_LANG_ENGLISH);
                        break;
                    case 2:
                        changeVoiceCommadLanguage(VOICE_REC_LANG_FRENCH);
                        break;
                    default:
                        changeVoiceCommadLanguage(VOICE_REC_LANG_KOREAN);
                        break;
                }

                SharedPreferences pref = getSharedPreferences(getString(R.string.project_name), Activity.MODE_PRIVATE);
                SharedPreferences.Editor ePref = pref.edit();

                int []arrDevSearchTimeOut =  pickerViewDevSearchTimeOut.getCurrentItems();
                switch (arrDevSearchTimeOut[0]) {
                    case 0:
                        PHCommon.curDevSearchTimeOut = 1;
                        break;
                    case 1:
                        PHCommon.curDevSearchTimeOut = 3;
                        break;
                    case 2:
                        PHCommon.curDevSearchTimeOut = 5;
                        break;
                    default:
                        PHCommon.curDevSearchTimeOut = 1;
                        break;
                }
                ePref.putInt("dev_search_limit", PHCommon.curDevSearchTimeOut);

                int []arrMin =  pickerViewDurationMin.getCurrentItems();
                int []arrSec =  pickerViewDurationSec.getCurrentItems();

                PHCommon.nProgramLastTime = arrMin[0]*60 + arrSec[0];
                if (PHCommon.nProgramLastTime == 0) {
                    PHCommon.nProgramLastTime = 1;
                }
                ePref.putInt("program_last_time", PHCommon.nProgramLastTime);
                ePref.commit();

                dlgSetttingsEdit.cancel();

            }
        });

        pickerViewDurationMin = (CharacterPickerView) vPopDlg.findViewById(R.id.pickerViewDurationMin);
        pickerViewDurationMin.setPicker(arrSeqMin);
        pickerViewDurationMin.setPickerTextColor(0xffe98a65, 0xffc5c5c5, getResources().getColor(R.color.colorPrimary));
        pickerViewDurationMin.setOnOptionChangedListener(new OnOptionChangedListener() {
            @Override
            public void onOptionChanged(int option1, int option2, int option3) {
                if (option1 == 30) {
                    pickerViewDurationSec.setSelectOptions(0);
                }
            }
        });

        pickerViewDurationSec = (CharacterPickerView) vPopDlg.findViewById(R.id.pickerViewDurationSec);
        pickerViewDurationSec.setPicker(arrSeqSec);
        pickerViewDurationSec.setPickerTextColor(0xffe98a65, 0xffc5c5c5, getResources().getColor(R.color.colorPrimary));
        pickerViewDurationSec.setOnOptionChangedListener(new OnOptionChangedListener() {
            @Override
            public void onOptionChanged(int option1, int option2, int option3) {
                int []arrMin =  pickerViewDurationMin.getCurrentItems();
                if (arrMin[0] == 30) {
                    pickerViewDurationSec.setSelectOptions(0);
                }
            }
        });


        int aMinSel = (int) PHCommon.nProgramLastTime / 60;
        int aSecSel = (int) (PHCommon.nProgramLastTime % 60);

        LOGe(TAG, "aMinSel:" + aMinSel + "aSecSel:" + aSecSel);

        if(aMinSel == 0 && aSecSel == 0) {
            aSecSel = 1;
        }
        pickerViewDurationMin.setSelectOptions(aMinSel);
        pickerViewDurationSec.setSelectOptions(aSecSel);


        TextView textView_PopSettingEdit_AppVersion = (TextView) vPopDlg.findViewById(R.id.textView_PopSettingEdit_AppVersion);

        try {
            PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            textView_PopSettingEdit_AppVersion.setText(pInfo.versionName);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        String aUnit = PHCommon.getUnit(this);
        if (aUnit.equals("ml")) {
            checkBox_PopSettingsEdit_ML.setChecked(true);
            checkBox_PopSettingsEdit_OZ.setChecked(false);
        } else {
            checkBox_PopSettingsEdit_ML.setChecked(false);
            checkBox_PopSettingsEdit_OZ.setChecked(true);
        }

        SharedPreferences pref = getSharedPreferences(getString(R.string.project_name), Activity.MODE_PRIVATE);
        String aDevAddr = pref.getString("device_addr", "");
        String aDevName = pref.getString("device_name", "");

        if (aDevAddr.length() > 0) {
            textView_PopSettingEdit_DevAddr.setText(aDevName + "\n" + aDevAddr);
        } else {
            textView_PopSettingEdit_DevAddr.setText(getString(R.string.NoDevice));
        }

        if (PHCommon.curSpeechLanguage.equals(VOICE_REC_LANG_KOREAN)) {
            pickerViewLanguage.setSelectOptions(0);
        } else if (PHCommon.curSpeechLanguage.equals(VOICE_REC_LANG_ENGLISH)) {
            pickerViewLanguage.setSelectOptions(1);
        } else if (PHCommon.curSpeechLanguage.equals(VOICE_REC_LANG_FRENCH)) {
            pickerViewLanguage.setSelectOptions(2);
        }

        switch (PHCommon.curDevSearchTimeOut) {
            case 1:
                pickerViewDevSearchTimeOut.setSelectOptions(0);
                break;
            case 3:
                pickerViewDevSearchTimeOut.setSelectOptions(1);
                break;
            case 5:
                pickerViewDevSearchTimeOut.setSelectOptions(2);
                break;
            default:
                PHCommon.curDevSearchTimeOut = 1;
                pickerViewDevSearchTimeOut.setSelectOptions(0);
                break;
        }

        if (PHCommon.bVCmdUseOnline) {
            checkBox_PopSettingsEdit_VCmdUseOnline.setChecked(true);
            checkBox_PopSettingsEdit_VCmdUseOffline.setChecked(false);
        } else {
            checkBox_PopSettingsEdit_VCmdUseOnline.setChecked(false);
            checkBox_PopSettingsEdit_VCmdUseOffline.setChecked(true);
        }

        if (PHCommon.bVCmdShowResult) {
            checkBox_PopSettingsEdit_VCmdResultShow.setChecked(true);
            checkBox_PopSettingsEdit_VCmdResultHide.setChecked(false);
        } else {
            checkBox_PopSettingsEdit_VCmdResultShow.setChecked(false);
            checkBox_PopSettingsEdit_VCmdResultHide.setChecked(true);
        }

        if (PHCommon.bVCmdEnabled) {
            checkBox_PopSettingsEdit_VCmdEnable.setChecked(true);
            checkBox_PopSettingsEdit_VCmdDisable.setChecked(false);
        } else {
            checkBox_PopSettingsEdit_VCmdEnable.setChecked(false);
            checkBox_PopSettingsEdit_VCmdDisable.setChecked(true);
        }

        if (Locale.getDefault().getLanguage().equals("fr")) {
            layout_PopSettingsEdit_VCmdGroup.setVisibility(View.GONE);
        } else {
            layout_PopSettingsEdit_VCmdGroup.setVisibility(View.VISIBLE);
        }

        dlgSetttingsEdit.show();
    }

    private void LoadingFWData() {
        showFWUpMessage("Loading FW Data.");

        nFWDataSize = 0;

        try {
            //Calc FW Data Block Count & FW Data Size
//            InputStream is = getAssets().open("BT-150-R02.hex");
            InputStream is = getAssets().open("FW-150-Hospital-R2.00_220302.hex");
            BufferedReader reader = new BufferedReader(new InputStreamReader(is));
            while(reader.ready()) {
                String aLine = reader.readLine();
                if (aLine == null) {
                    break;
                }
                byte[] aBytes = hexStringToByteArray(aLine.substring(1, aLine.length()-2));
                nFWDataSize += aBytes[0];
                arrLine.add(aLine);
                LOGe(TAG, "calc:" + aLine.substring(1, aLine.length()-2));
//                LOGe(TAG, " " + String.format("len:%d %02x %02x", aBytes[0], aBytes[aBytes.length-2], aBytes[aBytes.length-1] ));
            }
            is.close();
            nFWSendCnt = (short) (nFWDataSize/FW_BLOCK_SIZE + 1);

            //Copy FW Data to Sending Buffer
            byteFWSend = new byte[nFWDataSize];
            int aCopyIdx = 0;
            int aCnt = 0;
//            InputStream is1 = getAssets().open("BT-150-R02.hex");
            InputStream is1 = getAssets().open("FW-150-Hospital-R2.00_220302.hex");
            BufferedReader reader1 = new BufferedReader(new InputStreamReader(is1));
            while(reader1.ready()) {
                String aLine = reader1.readLine();
                if (aLine == null) {
                    break;
                }
                byte[] aBytes = hexStringToByteArray(aLine.substring(9, aLine.length()-2));
                LOGe(TAG, "parse:" + aLine.substring(9, aLine.length()-2));
                System.arraycopy(aBytes, 0, byteFWSend, aCopyIdx, aBytes.length);
                aCopyIdx += aBytes.length;
                aCnt++;
                LOG(TAG, String.format("%05d", aCnt) + ":" + String.format("%06d", aCopyIdx) + ":" + PHLib.buildStringFromBytes(aBytes, aBytes.length));
            }
            is1.close();

            LOGe(TAG, "nFWSendCnt:" + nFWSendCnt + " nFWDataSize:" + nFWDataSize);

        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    private void sendShortBLE(short a_Data) {
        byte[] byteArray = new byte[2];
        byteArray[0] = (byte) ((a_Data >> 8 ) & 0xFF);
        byteArray[1] = (byte) (a_Data & 0xFF);

        LOGe(TAG, "sendShortBLE:" + a_Data + " : " + String.format("%02x %02x", byteArray[0], byteArray[1]));

        if (mConnection == null) {
            LOGe(TAG, "mConnection is null");
            return ;
        }

        try {
            mConnection.transmit(byteArray);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void sendByteBLE(byte a_One) {

        byte[] byteArray = new byte[1];
        byteArray[0] = a_One;

        LOG(TAG, "sendByteBLE:" + String.format("%02x", byteArray[0]));

        if (mConnection == null) {
            LOGe(TAG, "mConnection is null");
            return ;
        }

        try {
            mConnection.transmit(byteArray);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void send2BytesBLE(byte a_One, byte a_Two) {

        byte[] byteArray = new byte[2];
        byteArray[0] = a_One;
        byteArray[1] = a_Two;

        LOG(TAG, "send2BytesBLE:" + String.format("%02x %02x", byteArray[0], byteArray[1]));

        if (mConnection == null) {
            LOGe(TAG, "mConnection is null");
            return ;
        }

        try {
            mConnection.transmit(byteArray);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void sendFWDataBlock(final byte[] a_Data, final int a_Len) {

        LOGe(TAG, "sendFWDataBlock: nSendDataIdx: " + nSendDataIdx);

        sCmdFWRecv = sCmdFWReadySend_Idle;
        nFWBlockSendIdx = 0;

//        while(true) {
//            if (nFWBlockSendIdx+1 < a_Len) {
//                send2BytesBLE(a_Data[nFWBlockSendIdx], a_Data[nFWBlockSendIdx+1]);
//                nFWBlockSendIdx += 2;
//            } else {
//                sCmdFWRecv = sCmdFWReadyRecv_CR;
//                break;
//            }
//        }

        if (mTimerFWDataBlock != null) {
            mTimerFWDataBlock.cancel();
            mTimerFWDataBlock.purge();
            mTimerFWDataBlock = null;
        }

        taskFWDataBlock = new TimerTask() {
            public void run() {

                if (nFWBlockSendIdx+1 < a_Len) {
                    send2BytesBLE(a_Data[nFWBlockSendIdx], a_Data[nFWBlockSendIdx+1]);
                    nFWBlockSendIdx += 2;
                } else {
                    mTimerFWDataBlock.cancel();
                    mTimerFWDataBlock.purge();
                    mTimerFWDataBlock = null;
//                    sCmdFWRecv = sCmdFWReadyRecv_CR;
                }
            }
        };
        mTimerFWDataBlock = new Timer();
        mTimerFWDataBlock.schedule(taskFWDataBlock, 10, 10);
    }

    String aFWDebug = "";

    private void showFWUpMessage(final String a_Msg) {
        LOGe(TAG, a_Msg);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                int aMin = (int) ((nFWUpMsec / 1000) / 60);
                int aSec = (int) ((nFWUpMsec / 1000) % 60);

                showDialogProgress(MainActivity.this,
//                        "Elapsed:" + String.format("%02d:%02d", aMin, aSec) + "\n\n" + a_Msg,
                         getString(R.string.Elapsed) + ":" + String.format("%02d:%02d", aMin, aSec),
                        getString(R.string.FWUpWarning)
                );
            }
        });
    }

//    private void showFWUpMessageSub(final String a_Msg) {
////        LOGe(TAG, "showFWUpMessageSub:" + a_Msg);
//        runOnUiThread(new Runnable() {
//            @Override
//            public void run() {
//                mFWUpgradeDialog.setMessage(a_Msg);
//            }
//        });
//    }

    private long nFWUpMsec = 0;
    private static final int FW_INTV_MSEC = 100;

    private void timeoutFWUpgrade() {
        if (mTimerFWUp != null) {
            mTimerFWUp.cancel();
            mTimerFWUp.purge();
            mTimerFWUp = null;
        }

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                PHLib.showMessageDialog(getString(R.string.FWTimeOut), MainActivity.this);
            }
        });
    }

    private void doFWUpgrade() {
        sCmdFWRecv = sCmdFWReadySend_Ready;
        nFWUpMsec = 0;
        nProgressStatus = 0;
        nSendDataIdx = 0;

        LoadingFWData();

        if (mTimerFWUp != null) {
            mTimerFWUp.cancel();
            mTimerFWUp.purge();
            mTimerFWUp = null;
        }

        taskFWUp = new TimerTask() {
            public void run() {
//                showFWUpMessageSub("Recv:" + sCmdFWRecv + "\nSend:" + sCmdFWLast);

                nFWUpMsec += FW_INTV_MSEC;

                int aMin = (int) ((nFWUpMsec / 1000) / 60);
                int aSec = (int) ((nFWUpMsec / 1000) % 60);

                if ((aMin >= 1 && aSec > 0)
                        && (sCmdFWRecv == sCmdFWReadySend_Ready
                            || sCmdFWRecv == sCmdFWReadyRecv_R
                            || sCmdFWRecv == sCmdFWReadyRecv_E
                            || sCmdFWRecv == sCmdFWReadyRecv_D
                            || sCmdFWRecv == sCmdFWReadyRecv_Y)) {
                    timeoutFWUpgrade();
                    return ;
                }

                if (aMin >= 3 && aSec > 0) {
                    timeoutFWUpgrade();
                    return ;
                }

                if (sCmdFWRecv == sCmdFWReadySend_Ready) {
                    showFWUpMessage("FW Up Check Authentifiction: Z");
                    sendByteBLE(sCmdFWReadySend_Z);
                    sCmdFWLast = sCmdFWReadySend_Z;
//                    nProgressStatus = 1;
                    showDialogProgress(MainActivity.this, "", "");

                } else if (sCmdFWRecv == sCmdFWReadyRecv_R) {
                    showFWUpMessage("FW Up Check Authentifiction: X");
                    sendByteBLE(sCmdFWReadySend_X);
                    sCmdFWLast = sCmdFWReadySend_X;
//                    nProgressStatus = 2;
                    showDialogProgress(MainActivity.this, "", "");

                } else if (sCmdFWRecv == sCmdFWReadyRecv_E) {
                    showFWUpMessage("FW Up Check Authentifiction: V");
                    sendByteBLE(sCmdFWReadySend_V);
                    sCmdFWLast = sCmdFWReadySend_V;
//                    nProgressStatus = 3;
                    showDialogProgress(MainActivity.this, "", "");

                } else if (sCmdFWRecv == sCmdFWReadyRecv_D) {
                    showFWUpMessage("FW Up Check Authentifiction: U");
                    sendByteBLE(sCmdFWReadySend_U);
                    sCmdFWLast = sCmdFWReadySend_U;
//                    nProgressStatus = 4;
                    showDialogProgress(MainActivity.this, "", "");

                } else if (sCmdFWRecv == sCmdFWReadyRecv_Y) {
                    if (sCmdFWLast == sCmdFWReadySend_U) {
                        PHLib.Sleep(1000);
                        sCmdFWLast = sCmdFWEraseDelay;

                    } else if (sCmdFWLast == sCmdFWEraseDelay) {
                        sendByteBLE(sCmdFWErase);
                        sendByteBLE((byte) FW_BLOCK_SIZE);
                        sendByteBLE((byte)nFWSendCnt);
                        sCmdFWLast = sCmdFWErase;
                        sCmdFWRecv = sCmdFWReadySend_Idle;
                        showFWUpMessage("Erasing FW flash : Block Cnt:" + nFWSendCnt);
                    }
//                    nProgressStatus = 5;
                    showDialogProgress(MainActivity.this, "", "");

                } else if (sCmdFWRecv == sCmdFWReadySend_Idle) {

                } else if (sCmdFWRecv == sCmdFWReadyRecv_ERR) {
                    showFWUpMessage("FW Up Unknown Error.");

                } else if (sCmdFWRecv == sCmdFWReadyRecv_CR) {

                    if (sCmdFWLast == sCmdFWErase || sCmdFWLast == sCmdFWFlash) {

                        if (sCmdFWLast == sCmdFWErase) {
                            showFWUpMessage("Erasing finished!!");

//                            nProgressStatus = 6;
                            showDialogProgress(MainActivity.this, "", "");
                        }
                        sendByteBLE(sCmdFWAddress);

                        short aSendAddr = (short) ((FW_BLOCK_SIZE * nSendDataIdx) / 2);
                        sendShortBLE(aSendAddr);

                        sCmdFWLast = sCmdFWAddress;
                        sCmdFWRecv = sCmdFWReadySend_Idle;
//                        LOGe(TAG, "Send write address.");
                        showFWUpMessage("Writing Data Address:" + aSendAddr);

                    } else if (sCmdFWLast == sCmdFWAddress) {
                        sendByteBLE(sCmdFWBinary);

                        int aSendIdx = FW_BLOCK_SIZE * nSendDataIdx;
                        int aLen = FW_BLOCK_SIZE;
                        if (aSendIdx + aLen > nFWDataSize) {
                            aLen = nFWDataSize - aSendIdx;
                        }
                        sendShortBLE((short) aLen);

                        sendByteBLE(sCmdFWFlash);

                        sCmdFWLast = sCmdFWBinary;
//                        sCmdFWRecv = sCmdFWReadySend_Idle;
                        showFWUpMessage("Writing Data block No: " + nSendDataIdx);

                    } else if (sCmdFWLast == sCmdFWBinary) {
//                        sendByteBLE(sCmdFWFlash);

                        int aSendIdx = FW_BLOCK_SIZE * nSendDataIdx;
                        int aLen = FW_BLOCK_SIZE;
                        if (aSendIdx + aLen > nFWDataSize) {
                            aLen = nFWDataSize - aSendIdx;
                        }

                        byte[] aSendBlock = new byte[aLen];
                        System.arraycopy(byteFWSend, aSendIdx, aSendBlock, 0, aLen);
                        sendFWDataBlock(aSendBlock, aLen);

                        sCmdFWLast = sCmdFWFlash;
                        sCmdFWRecv = sCmdFWReadySend_Idle;
//                        LOGe(TAG, "Write FW Data:" + nSendDataIdx);
                        nSendDataIdx++;

                        int aPer = (nSendDataIdx * 100) / nFWSendCnt;
                        if (aPer > nProgressStatus ) {
                            nProgressStatus = aPer;
                        }
                        showDialogProgress(MainActivity.this, "", "");
                        LOGe(TAG, "#################################### aPer:" + aPer + " nProgressStatus:" + nProgressStatus);


                        if (nSendDataIdx >= nFWSendCnt) {
                            nProgressStatus = 100;
                            showDialogProgress(MainActivity.this, "", "");

                            sCmdFWRecv = sCmdFWReadyRecv_CR;
                            sCmdFWLast = sCmdFWEnd;
                        }

                    } else if (sCmdFWLast == sCmdFWEnd) {

                        if ( nSendDataIdx >= (nFWSendCnt+10) ) {
                            LOGe(TAG, "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! nSendDataIdx:" + nSendDataIdx + " nFWSendCnt:" + nFWSendCnt);

                            bFWUpMode = false;

                            mTimerFWUp.cancel();
                            mTimerFWUp.purge();
                            mTimerFWUp = null;

                            if((mFWUpgradeDialog != null) && mFWUpgradeDialog.isShowing()) {
                                mFWUpgradeDialog.dismiss();
                                PHLib.showMessageDialog(getString(R.string.FWRestart), MainActivity.this);
                            };
                        }
                        nSendDataIdx++;
                    }

                }

            }
        };
        mTimerFWUp = new Timer();
        mTimerFWUp.schedule(taskFWUp, FW_INTV_MSEC, FW_INTV_MSEC);
    }


//    private void refreshPopUnitSetting() {
//        String aUnit = PHCommon.getUnit(this);
//        if (aUnit.equals("ml")) {
//            checkBox_PopSettingsEdit_ML.setChecked(true);
//            checkBox_PopSettingsEdit_OZ.setChecked(false);
//        } else {
//            checkBox_PopSettingsEdit_ML.setChecked(false);
//            checkBox_PopSettingsEdit_OZ.setChecked(true);
//        }
//    }

//////////////////////////////////////////////////////////////////
//
//   Droid Speech
//
//////////////////////////////////////////////////////////////////
    private void initVoiceCommandLanguage() {
        arrVCmdLanguage.clear();
        arrVCmdLanguage.add(getString(R.string.LanguageKorean));
        arrVCmdLanguage.add(getString(R.string.LanguageEnglish));
//        arrVCmdLanguage.add(getString(R.string.LanguageFrench));
    }

    private void changeVoiceCommadLanguage(String a_Language) {
        if (PHCommon.curSpeechLanguage.equals(a_Language)) {
            return;
        }

        PHCommon.curSpeechLanguage = a_Language;
        if (droidSpeech != null) {
            LOGe(TAG, "changeVoiceCommadLanguage : " + PHCommon.curSpeechLanguage);
            droidSpeech.setPreferredLanguage(PHCommon.curSpeechLanguage);
        }
        VCmdControl.initVoiceCommand(this);
        VCmdControl.initAddingPhrase(PHCommon.curSpeechLanguage);

        SharedPreferences pref = getSharedPreferences(getString(R.string.project_name), Activity.MODE_PRIVATE);
        SharedPreferences.Editor ePref = pref.edit();
        ePref.putString("vcmd_language", PHCommon.curSpeechLanguage);
        ePref.commit();
    }

    private void checkVoicRecognitionSupported() {
        isRecognitionAvailable = SpeechRecognizer.isRecognitionAvailable(this);
        if (isRecognitionAvailable) {
//            try {
//                getVoiceDetailsIntent_TEST(this);
//                Intent languageDetailsIntent = RecognizerIntent.getVoiceDetailsIntent(this);
//                if (languageDetailsIntent == null) {
//                    LOGe(TAG, "Intent languageDetailsIntent = RecognizerIntent.getVoiceDetailsIntent(this);: is null!!");
//                    isRecognitionAvailable = false;
//                    Toast.makeText(this, "getVoiceDetailsIntent is null!", Toast.LENGTH_SHORT).show();
//                }
//            } catch (Exception e) {
//                isRecognitionAvailable = false;
//                Toast.makeText(this, "getVoiceDetailsIntent exception:" + e.getMessage(), Toast.LENGTH_SHORT).show();
//            }
        } else {
            Toast.makeText(this, "isRecognitionAvailable returns false!", Toast.LENGTH_SHORT).show();
        }
        LOGe(TAG, "isRecognitionAvailable:" + isRecognitionAvailable);
    }

    public static final Intent getVoiceDetailsIntent_TEST(Context context) {
        LOGe(TAG, "getVoiceDetailsIntent_TEST-1");
        Intent voiceSearchIntent = new Intent(ACTION_WEB_SEARCH);
        LOGe(TAG, "getVoiceDetailsIntent_TEST-2");
        ResolveInfo ri = context.getPackageManager().resolveActivity(voiceSearchIntent, PackageManager.GET_META_DATA);
        LOGe(TAG, "getVoiceDetailsIntent_TEST-3");
        if (ri == null || ri.activityInfo == null || ri.activityInfo.metaData == null) {
            LOGe(TAG, "getVoiceDetailsIntent_TEST-4");
            return null;
        }
        LOGe(TAG, "getVoiceDetailsIntent_TEST-5");
        String className = ri.activityInfo.metaData.getString(DETAILS_META_DATA);
        LOGe(TAG, "getVoiceDetailsIntent_TEST-6:" + className);
        if (className == null) {
            LOGe(TAG, "getVoiceDetailsIntent_TEST-7");
            return null;
        }
        LOGe(TAG, "getVoiceDetailsIntent_TEST-8");
        Intent detailsIntent = new Intent(ACTION_GET_LANGUAGE_DETAILS);
        LOGe(TAG, "getVoiceDetailsIntent_TEST-9");
        detailsIntent.setComponent(new ComponentName(ri.activityInfo.packageName, className));
        LOGe(TAG, "getVoiceDetailsIntent_TEST-10");
        return detailsIntent;
    }


    private void initDroidSpeech() {
        LOGe(TAG, "initDroidSpeech start!");
        SharedPreferences pref = getSharedPreferences(getString(R.string.project_name), Activity.MODE_PRIVATE);
        PHCommon.curSpeechLanguage = pref.getString("vcmd_language", VOICE_REC_LANG_KOREAN);
//        PHCommon.bVCmdUseOnline = pref.getBoolean("vcmd_useonline", true);
        PHCommon.bVCmdUseOnline = true;
        PHCommon.bVCmdShowResult = pref.getBoolean("vcmd_showresult", true);
        PHCommon.bVCmdEnabled = pref.getBoolean("vcmd_enabled", true);

//        VCmdControl.initVoiceCommand(this);

        droidSpeech = new DroidSpeech(this, getFragmentManager());
        if (droidSpeech != null) {
            droidSpeech.setOnDroidSpeechListener(this);
            droidSpeech.setShowRecognitionProgressView(false);
            droidSpeech.setOneStepResultVerify(true);
            droidSpeech.setRecognitionProgressMsgColor(Color.WHITE);
            droidSpeech.setOneStepVerifyConfirmTextColor(Color.WHITE);
            droidSpeech.setOneStepVerifyRetryTextColor(Color.WHITE);
            droidSpeech.setOfflineSpeechRecognition(!PHCommon.bVCmdUseOnline);
        }
    }

    private void startDroidSpeech() {
        if (!PHCommon.bVCmdEnabled) {
            return ;
        }

        LOGe(TAG, "######################## startDroidSpeech : " + bSpeechRecogizing + " PHCommon.bVCmdEnabled:" + PHCommon.bVCmdEnabled);
        if (!bSpeechRecogizing) {
            bSpeechRecogizing = true;

            droidSpeech.setPreferredLanguage(PHCommon.curSpeechLanguage);
            VCmdControl.initVoiceCommand(this);
            VCmdControl.initAddingPhrase(PHCommon.curSpeechLanguage);

            droidSpeech.startDroidSpeechRecognition();
            Toast.makeText(this, getString(R.string.StartVCmd) + "!", Toast.LENGTH_SHORT).show();
        }
    }

    private void stopDroidSpeech() {
        LOGe(TAG, "######################## stopDroidSpeech ########################");
        if (droidSpeech != null) {
            droidSpeech.closeDroidSpeechOperations();
        }
        bSpeechRecogizing = false;
        Toast.makeText(this, getString(R.string.StopVCmd), Toast.LENGTH_SHORT).show();
    }


    @Override
    public void onDroidSpeechSupportedLanguages(String currentSpeechLanguage, List<String> supportedSpeechLanguages) {
        Log.i(TAG, "Current speech language = " + currentSpeechLanguage);
        Log.i(TAG, "Supported speech languages = " + supportedSpeechLanguages.toString());

//        if(supportedSpeechLanguages.contains(PHCommon.curSpeechLanguage)) {
//            LOGe(TAG, "setPreferredLanguage : " + PHCommon.curSpeechLanguage);
//            droidSpeech.setPreferredLanguage(PHCommon.curSpeechLanguage);
//            VCmdControl.initVoiceCommand(this);
//            VCmdControl.initAddingPhrase(PHCommon.curSpeechLanguage);
//        }
    }

    private float fRmsChangedValue = 0;

    @Override
    public void onDroidSpeechRmsChanged(float rmsChangedValue) {
        if (fRmsChangedValue != rmsChangedValue) {
//            LOGe(TAG, "Rms change value = " + rmsChangedValue);
            fRmsChangedValue = rmsChangedValue;
        }
    }

    @Override
    public void onDroidSpeechLiveResult(String liveSpeechResult) {
        LOGe(TAG, "Live speech result = " + liveSpeechResult);
    }

    @Override
    public void onDroidSpeechFinalResult(String finalSpeechResult) {
        // Setting the final speech result
        VoiceCommand aCmd = VCmdControl.findVoiceCommand(finalSpeechResult, this);
        boolean aRet = VCmdControl.handleVoiceCommand(aCmd, this);
        String aShowText = finalSpeechResult;
        if (aRet) {
            aShowText = aCmd.arrCmdPhrase.get(0);
        }
        LOGe(TAG, "onDroidSpeechFinalResult: finalSpeechResult:" + finalSpeechResult + " aShowText:" + aShowText);

        if (PHCommon.bVCmdShowResult) {
            if (aRet) {
                Toast.makeText(this, aShowText, Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this, "Recognizing Fail:" + finalSpeechResult, Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onDroidSpeechClosedByUser() {
//        stop.setVisibility(View.GONE);
//        start.setVisibility(View.VISIBLE);
    }

    @Override
    public void onDroidSpeechError(String errorMsg) {
        // Speech error
        LOGe(TAG, "onDroidSpeechError:" + errorMsg);

        stopDroidSpeech();

        PHCommon.goBackToMain(this);

        PHLib.showYesNoDialog(errorMsg + "\n Do you want to restart voice command?", "Voice Command Error",  this, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                droidSpeech.setOfflineSpeechRecognition(true);
                startDroidSpeech();
            }
        });
    }

    // MARK: DroidSpeechPermissionsListener Method
    @Override
    public void onDroidSpeechAudioPermissionStatus(boolean audioPermissionGiven, String errorMsgIfAny) {
        if(audioPermissionGiven) {
            bSpeechRecogizing = false;
            startDroidSpeech();
        } else {
            // Permissions error
            String aMsg = "Audio recording permission error!";
            if(errorMsgIfAny != null) {
                aMsg = errorMsgIfAny;
//                Toast.makeText(this, errorMsgIfAny, Toast.LENGTH_LONG).show();
            }
            LOGe(TAG, "onDroidSpeechAudioPermissionStatus stopDroidSpeech");
            PHLib.showMessageDialog(aMsg, this);
            stopDroidSpeech();
        }
    }

}
