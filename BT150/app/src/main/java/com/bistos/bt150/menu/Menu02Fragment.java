package com.bistos.bt150.menu;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bistos.bt150.MainActivity;
import com.bistos.bt150.R;
import com.bistos.bt150.com.PHCommon;
import com.bistos.bt150.com.PHLib;
import com.bistos.bt150.com.ProgramData;

import java.util.ArrayList;

import cn.jeesoft.widget.pickerview.CharacterPickerView;
import cn.jeesoft.widget.pickerview.OnOptionChangedListener;

import static com.bistos.bt150.com.PHCommon.ACT_MENU02;
import static com.bistos.bt150.com.PHCommon.OP_MODE_NONE;
import static com.bistos.bt150.com.PHCommon.OP_TYPE_DOUBLE;
import static com.bistos.bt150.com.PHCommon.OP_TYPE_NONE;
import static com.bistos.bt150.com.PHCommon.OP_TYPE_SINGLE;
import static com.bistos.bt150.com.PHCommon.VOICE_MODE_PROGRAM;
import static com.bistos.bt150.com.PHCommon.VOICE_MODE_PROGRAM_EDIT;
import static com.bistos.bt150.com.PHCommon.nCurSelPrgTabIdx;
import static com.bistos.bt150.com.PHCommon.nOperationType;
import static com.bistos.bt150.com.PHCommon.nPrgOpType5;
import static com.bistos.bt150.com.PHCommon.nPrgOpType6;
import static com.bistos.bt150.com.PHCommon.nPrgOpType7;
import static com.bistos.bt150.com.PHCommon.nPrgOpType8;
import static com.bistos.bt150.com.PHCommon.nVoiceMode;
import static com.bistos.bt150.com.PHLib.LOGe;
import static com.bistos.bt150.com.VoiceCommand.CMD_CYCLE_DOWN;
import static com.bistos.bt150.com.VoiceCommand.CMD_CYCLE_UP;
import static com.bistos.bt150.com.VoiceCommand.CMD_MODE_CHANGE;
import static com.bistos.bt150.com.VoiceCommand.CMD_PRESSURE_DOWN;
import static com.bistos.bt150.com.VoiceCommand.CMD_PRESSURE_UP;

/**
 * Created by lamb on 2017. 10. 12..
 */

public class Menu02Fragment extends Fragment {

    private static final String TAG = "Menu02Fragment";

    public static final int MENU02_TAB_CHANGED = 1000;

    //    private TextView textView01;
    private View vFragment = null;
    private ListView listView_frgProgram_List;
    public ProgramListAdapter adapterProgramList;

    public ArrayList<ProgramData> arrData = null;
//    private LineChart mChart;

    private ArrayList<String> arrSeqMin = new ArrayList<String>();
    private ArrayList<String> arrSeqSec = new ArrayList<String>();

    private Dialog dlgProgramEdit = null;
    private View vPopDlg = null;

    public int nTabIdx = 0;
    public int nCurSelIdx = -1;
    private int nCurEditIdx = -1;
    public  int nPrgReqCnt = 0;

    //    private int nProgramPopType = PHCommon.OP_TYPE_SINGLE;
    private int nProgramPopMode = OP_MODE_NONE;
    private int nProgramPopPressureExp = 0;
    private int nProgramPopPressureMsg = 0;
    private int nProgramPopCycleExp = 0;
    private int nProgramPopCycleMsg = 0;

    private SeekBar seekBar_ActHome_Pressure = null;
    private SeekBar seekBar_ActHome_Cycle = null;
    private TextView textView_PopProgramEdit_Pressure = null;
    private TextView textView_PopProgramEdit_Cycle = null;

    private ImageView imageView_PopProgramEdit_Expression = null;
    private ImageView imageView_PopProgramEdit_Massage = null;
    private ImageView imageView_PopProgramEdit_Single = null;
    private ImageView imageView_PopProgramEdit_Double = null;
    private LinearLayout linearLayout_PopProgramEdit_Expression = null;
    private LinearLayout linearLayout_PopProgramEdit_Massage = null;
    private LinearLayout linearLayout_PopProgramEdit_Single = null;
    private LinearLayout linearLayout_PopProgramEdit_Double = null;
    private LinearLayout linearLayout_PopProgramEdit_Prev = null;
    private LinearLayout linearLayout_PopProgramEdit_Next = null;


    private CharacterPickerView pickerViewDurationMin;
    private CharacterPickerView pickerViewDurationSec;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        LOGe("onDataReceived onCreateView Menu02!!!!!!!!!!!!!!!!!!!!!!!!!!");

        vFragment = inflater.inflate(R.layout.fragment_menu02, container, false);

        adapterProgramList = new ProgramListAdapter(getActivity());

        listView_frgProgram_List = (ListView) vFragment.findViewById(R.id.listView_frgProgram_List);
        listView_frgProgram_List.setAdapter(adapterProgramList);
        listView_frgProgram_List.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                LOGe(TAG, "listView_frgProgram_List click : " + nTabIdx);
                int nProgramFixIdx = 4;
//                if (Locale.getDefault().getLanguage().equals("fr")) {
//                    nProgramFixIdx = 6;
//                }

                if (nTabIdx >= nProgramFixIdx) {
                    if(position > 0) {
                        ProgramData programData = arrData.get(position-1);
                        if(programData.nMode == OP_MODE_NONE) {
                            PHLib.showMessageDialog(getString(R.string.PreviousSeqFirst), getActivity());
                            return ;
                        }
                    }

                    showProgramEdit(position);
                }
            }
        });

//        listView_frgProgram_List.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
//            @Override
//            public boolean onItemLongClick(AdapterView<?> arg0, View arg1, int position, long id) {
//                showProgramEdit(position);
//                return true;
//            }
//        });

//        initChart();

        initProgramFragment(getArguments().getInt("position"));

        getActivity().registerReceiver(dataReceiver, new IntentFilter(PHCommon.MSG_DATA_RECV));
        getActivity().registerReceiver(cmdReceiver, new IntentFilter(PHCommon.MSG_PRGLIST_RELOAD));
        getActivity().registerReceiver(vcmdReceiver, new IntentFilter(PHCommon.MSG_VCMD_RECV));

        if (PHCommon.bProgramRunning) {
            parseRecvData(PHCommon.gRecvData);
        }

        return vFragment;
    }

    @Override
    public void onDestroyView() {
        getActivity().unregisterReceiver(dataReceiver);
        getActivity().unregisterReceiver(cmdReceiver);
        getActivity().unregisterReceiver(vcmdReceiver);

        super.onDestroyView();
    }

    public void refreshList() {
        LOGe(TAG, "refreshList:" + nTabIdx);
        arrData = PHCommon.arrProgramData[nTabIdx];
        adapterProgramList.notifyDataSetChanged();
        calcProgItemCount();
    }

    private void calcProgItemCount() {
        int aCnt = 0;
        for (int j = 0; j < arrData.size(); j++) {
            if (arrData.get(j).nMode == OP_MODE_NONE) {
                break;
            }
            aCnt++;
        }
        nPrgReqCnt = aCnt;
    }

    private void parseRecvData(String a_Recv) {

        if (a_Recv.length() >= 14
                && a_Recv.substring(0, 1).equals("M")
                && a_Recv.substring(2, 3).equals("P")
                && a_Recv.substring(5, 6).equals("C")
                && a_Recv.substring(7, 8).equals("R")
                && a_Recv.substring(9, 10).equals("I")
                && a_Recv.substring(11, 12).equals("S"))
        {
//            int aRun = PHLib.convertToInt(a_Recv.substring(8, 9));
            String aRun = a_Recv.substring(8, 9);
            int aNo = PHLib.convertToInt(a_Recv.substring(10, 11));
            int aSeq = PHLib.convertToInt(a_Recv.substring(12, 13));

            LOGe(TAG, "Fragment parseRecvData: aRun:" + aRun + " aNo:" + aNo + " aSeq:" + aSeq);
            LOGe(TAG, "nTabIdx:" + nTabIdx);

            int aSelIdx = PHCommon.nCurSelPrgTabIdx;

            LOGe(TAG, "onDataReceived nActivityNo:" + PHCommon.nActivityNo);
            if (PHCommon.nActivityNo == ACT_MENU02) {
//            if (aRun.equals("2")) {

//                if (aSelIdx != aNo) {
//                    ((Menu02Activity)getActivity()).setPageSelect(aNo);
//                }

                if (aNo == nTabIdx) {
                    nCurSelIdx = aSeq;
                    timerDelayRunForScroll(10);
                }

                if (aRun.equals("2")) {
                    PHCommon.bProgramRunning = true;

                } else {
                    if (PHCommon.bProgramRunning) {
                        PHCommon.nElapsedTime = 0;
                    }
                    PHCommon.bProgramRunning = false;
                    nCurSelIdx = -1;
                }

                adapterProgramList.notifyDataSetChanged();
                calcProgItemCount();

//            } else {
//                nCurSelIdx = -1;
//                adapterProgramList.notifyDataSetChanged();
//            }

            }

        }
    }

    public void timerDelayRunForScroll(long time) {
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                try {
                    listView_frgProgram_List.smoothScrollToPosition(nCurSelIdx);
                } catch (Exception e) {}
            }
        }, time);
    }

    private final BroadcastReceiver dataReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent a_intent) {
            Bundle bundle = a_intent.getExtras();
            String aRecv = bundle.getString("recv_data");
//            LOGe(TAG, "dataReceiver:" + aRecv);
            parseRecvData(PHCommon.gRecvData);
        }
    };

    private final BroadcastReceiver cmdReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent a_intent) {
            refreshList();
        }
    };

    private final BroadcastReceiver vcmdReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent a_intent) {

            Bundle bundle = a_intent.getExtras();
            int aCmdCode = bundle.getInt("CmdCode");
            int aCmdType = bundle.getInt("CmdType");
            int aStepVal = bundle.getInt("StepVal");
            LOGe(TAG, "onReceive: aCmdCode:" + aCmdCode + " aCmdType:" + aCmdType + " aStepVal:" + aStepVal);
            switch (aCmdCode) {
                case CMD_MODE_CHANGE:
                    if (isProgramEditing()) {
                        if (nProgramPopMode == PHCommon.OP_MODE_EXP) {
                            nProgramPopMode = PHCommon.OP_MODE_MSG;
                        } else {
                            nProgramPopMode = PHCommon.OP_MODE_EXP;
                        }
                        refreshModeTap();
                    }
                    break;
//                case CMD_MODE_EXPRESS:
//                case CMD_EXPRESS:
//                    if (isProgramEditing()) {
//                        nProgramPopMode = PHCommon.OP_MODE_EXP;
//                        refreshModeTap();
//                    }
//                    break;
//                case CMD_MODE_MASSAGE:
//                case CMD_MASSAGE:
//                    nProgramPopMode = PHCommon.OP_MODE_MSG;
//                    refreshModeTap();
//                    break;
                case CMD_PRESSURE_UP:
                    if (isProgramEditing()) {
                        doPressureUp();
                    }
                    break;
                case CMD_PRESSURE_DOWN:
                    if (isProgramEditing()) {
                        doPressureDown();
                    }
                    break;
                case CMD_CYCLE_UP:
                    if (isProgramEditing()) {
                        doCycleUp();
                    }
                    break;
                case CMD_CYCLE_DOWN:
                    if (isProgramEditing()) {
                        doCycleDown();
                    }
                    break;
//                case CMD_PROGRAM_START:
//                case CMD_START:
//                    if (!isProgramEditing()) {
//                        getActivity().runOnUiThread(new Runnable() {
//                            @Override
//                            public void run() {
//                                showProgramEdit(0);
//                            }
//                        });
//                    }
//                    break;
//                case CMD_PROGRAM_END:
//                case CMD_END:
//                    if (isProgramEditing()) {
//                        dlgProgramEdit.cancel();
//                        nVoiceMode = VOICE_MODE_PROGRAM;
//                    }
//                    break;
//                case CMD_PRESSURE_SAVE:
//                    break;
//                case CMD_CYCLE_SAVE:
//                    break;
//                case CMD_SECOND_UP:
//                    if (isProgramEditing()) {
//                        changeTime(1);
//                    }
//                    break;
//                case CMD_SECOND_DOWN:
//                    if (isProgramEditing()) {
//                        changeTime(0);
//                    }
//                    break;
//                case CMD_MINUTE_UP:
//                    if (isProgramEditing()) {
//                        changeTime(4);
//                    }
//                    break;
//                case CMD_MINUTE_DOWN:
//                    if (isProgramEditing()) {
//                        changeTime(3);
//                    }
//                    break;
//                case CMD_NEXT_SEQ:
//                case CMD_NEXT:
//                    if (isProgramEditing()) {
//                        goPrevSeqNext(nCurEditIdx+1);
//                    }
//                    break;
                default:
                    break;
            }

        }
    };

    private void changeTime(int a_Dir){
        ProgramData programData = arrData.get(nCurEditIdx);

        if (programData.nLen == 0) {
            int []arrMin =  pickerViewDurationMin.getCurrentItems();
            int []arrSec =  pickerViewDurationSec.getCurrentItems();

            programData.nLen = arrMin[0]*60 + arrSec[0];
        }

        int aMin = (int) programData.nLen / 60;
        int aSec = (int) (programData.nLen % 60);
        
        if (a_Dir == 0) {
            aSec--;
        } else if (a_Dir == 1) {
            aSec++;
        } else if (a_Dir == 2) {
            aMin--;
        } else if (a_Dir == 3) {
            aMin++;
        }

        if (aSec < 0) {
            aSec = 0;
        } else if (aSec > 60) {
            aSec = 60;
        }

        if (aMin < 0) {
            aMin = 0;
        } else if (aMin > 100) {
            aMin = 100;
        }

        pickerViewDurationMin.setSelectOptions(aMin);
        pickerViewDurationSec.setSelectOptions(aSec);
    }

    private boolean isProgramEditing() {
        if (dlgProgramEdit != null && dlgProgramEdit.isShowing()) {
            return true;
        }
        return false;
    }

    public void initProgramFragment(int a_Idx) {
        LOGe(TAG, "initProgramFragment:" + a_Idx);

        nTabIdx = a_Idx;

        initSeqTime();

        arrData = PHCommon.arrProgramData[nTabIdx];

//        arrData.clear();
//        int aCode = a_Idx*1000 + 1;
//        arrData.add(new ProgramData(aCode++, PHCommon.OP_MODE_MSG, 7, 0, 1));
//        arrData.add(new ProgramData(aCode++, PHCommon.OP_MODE_EXP, 6, 1, 2));
//        arrData.add(new ProgramData(aCode++, PHCommon.OP_MODE_MSG, 2, 2, 1));
//        arrData.add(new ProgramData(aCode++, PHCommon.OP_MODE_EXP, 4, 4, 2));
//        arrData.add(new ProgramData(aCode++, PHCommon.OP_MODE_MSG, 7, 2, 1));
//        arrData.add(new ProgramData(aCode++, PHCommon.OP_MODE_EXP, 2, 6, 2));
//        arrData.add(new ProgramData(aCode++, PHCommon.OP_MODE_MSG, 4, 2, 2));
//        arrData.add(new ProgramData(aCode++, PHCommon.OP_MODE_EXP, 5, 4, 2));

        if (arrData !=null && arrData.size() ==0) {
            for (int i = 0; i < 8; i++) {
                int aCode = (a_Idx + 1) * 1000 + i;
                arrData.add(new ProgramData(aCode, OP_MODE_NONE, 0, 0, 0, PHCommon.OP_TYPE_SINGLE));
            }
        }

        arrSeqMin.clear();
        arrSeqSec.clear();

        for (int i = 0; i < 31; i++) {
            arrSeqMin.add(String.format("%02d", i));
        }

        for (int i = 0; i < 60; i++) {
            arrSeqSec.add(String.format("%02d", i));
        }

        adapterProgramList.notifyDataSetChanged();
        calcProgItemCount();

//        initProgramChart();
    }

    private void initSeqTime() {

        arrSeqMin.clear();
        arrSeqSec.clear();

        for (int i = 0; i < 31; i++) {
            arrSeqMin.add(String.format("%02d", i));
        }

        for (int i = 0; i < 60; i++) {
            arrSeqSec.add(String.format("%02d", i));
        }

//        LOG(TAG, "aStart:" + aStart + " aEnd:" + aEnd);
    }

    private class ProgramListAdapter extends BaseAdapter {
        private LayoutInflater mInflater;

        public ProgramListAdapter(Context context) {
            mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            if (arrData != null) {
                return arrData.size();
            }
            return 0;
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            LinearLayout linearLayout_RowProgram_Base;
            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.row_program_list, parent, false);
                linearLayout_RowProgram_Base = (LinearLayout) convertView.findViewById(R.id.linearLayout_RowProgram_Base);
            } else {
                linearLayout_RowProgram_Base = (LinearLayout) convertView.findViewById(R.id.linearLayout_RowProgram_Base);
                if (linearLayout_RowProgram_Base == null) {
                    convertView = mInflater.inflate(R.layout.row_program_list, parent, false);
                    linearLayout_RowProgram_Base = (LinearLayout) convertView.findViewById(R.id.linearLayout_RowProgram_Base);
                }
            }

            if(position == nCurSelIdx) {
                linearLayout_RowProgram_Base.setBackgroundColor(getResources().getColor(R.color.colorSelectList));
            } else {
                linearLayout_RowProgram_Base.setBackgroundColor(getResources().getColor(R.color.colorWhite));
            }

            ImageView imageView_RowProgram_Seq = (ImageView) convertView.findViewById(R.id.imageView_RowProgram_Seq);

            ImageView imageView_RowProgram_OpType = (ImageView) convertView.findViewById(R.id.imageView_RowProgram_OpType);
            ImageView imageView_RowProgram_Mode = (ImageView) convertView.findViewById(R.id.imageView_RowProgram_Mode);
            TextView textView_RowProgram_Pressure = (TextView) convertView.findViewById(R.id.textView_RowProgram_Pressure);
            TextView textView_RowProgram_Cycle = (TextView) convertView.findViewById(R.id.textView_RowProgram_Cycle);
            TextView textView_RowProgram_Time = (TextView) convertView.findViewById(R.id.textView_RowProgram_Time);
            TextView textView_RowProgram_GuideText = (TextView) convertView.findViewById(R.id.textView_RowProgram_GuideText);

            LinearLayout linearLayout_RowProgram_Del = (LinearLayout) convertView.findViewById(R.id.linearLayout_RowProgram_Del);
            linearLayout_RowProgram_Del.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (nTabIdx < 4) {
                        return;
                    }

                    PHLib.showYesNoDialog(getString(R.string.deleteSequence), getActivity(), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int which) {
                            for(int i = position; i < arrData.size()-1; i++) {
                                arrData.get(i).nCode = arrData.get(i+1).nCode;
                                arrData.get(i).nMode = arrData.get(i+1).nMode;
                                arrData.get(i).nPressure = arrData.get(i+1).nPressure;
                                arrData.get(i).nCycle = arrData.get(i+1).nCycle;
                                arrData.get(i).nLen = arrData.get(i+1).nLen;
                                arrData.get(i).bDownloaded = arrData.get(i+1).bDownloaded;
                            }
                            arrData.get(arrData.size()-1).initProgramData();

                            adapterProgramList.notifyDataSetChanged();
                            calcProgItemCount();

                            ((Menu02Activity)getActivity()).refreshSaveDeleteAllButton();
                        }
                    });


                }
            });

            ImageView imageView_RowProgram_Del = (ImageView) convertView.findViewById(R.id.imageView_RowProgram_Del);

            int nProgramFixIdx = 4;
//            if (Locale.getDefault().getLanguage().equals("fr")) {
//                nProgramFixIdx = 6;
//            }

            if (nTabIdx < nProgramFixIdx) {
                imageView_RowProgram_Del.setVisibility(View.INVISIBLE);
            }

            ProgramData programData = arrData.get(position);


            int resID = getResources().getIdentifier(String.format("program_seq_%d", position + 1) , "drawable", getActivity().getPackageName());
            imageView_RowProgram_Seq.setImageResource(resID);

            if (programData.nMode == OP_MODE_NONE) {
                if (nTabIdx >= nProgramFixIdx) {
                    textView_RowProgram_GuideText.setVisibility(View.VISIBLE);
                    textView_RowProgram_GuideText.setText(getString(R.string.addContents));
                } else {
                    textView_RowProgram_GuideText.setVisibility(View.GONE);
                    textView_RowProgram_GuideText.setText("");
                }

                imageView_RowProgram_OpType.setImageResource(0);
                imageView_RowProgram_Mode.setImageResource(0);

                textView_RowProgram_Pressure.setText("-");
                textView_RowProgram_Cycle.setText("-");
                textView_RowProgram_Time.setText("-");
//                linearLayout_RowProgram_Del.setVisibility(View.INVISIBLE);

                imageView_RowProgram_Mode.setVisibility(View.INVISIBLE);
                textView_RowProgram_Pressure.setVisibility(View.INVISIBLE);
                textView_RowProgram_Cycle.setVisibility(View.INVISIBLE);
                textView_RowProgram_Time.setVisibility(View.INVISIBLE);

                imageView_RowProgram_Del.setImageResource(R.drawable.program_btn_next);

            } else {
                textView_RowProgram_GuideText.setVisibility(View.GONE);
                textView_RowProgram_GuideText.setText("");

                if(nTabIdx == 0) {
                    imageView_RowProgram_OpType.setImageResource(R.drawable.program_ic_double);
                } else if(nTabIdx == 1) {
                    imageView_RowProgram_OpType.setImageResource(R.drawable.program_ic_double);
                } else if(nTabIdx == 2) {
                    imageView_RowProgram_OpType.setImageResource(R.drawable.program_ic_single);
                } else if(nTabIdx == 3) {
                    imageView_RowProgram_OpType.setImageResource(R.drawable.program_ic_single);
                } else if(nTabIdx == 4) {
//                    if (Locale.getDefault().getLanguage().equals("fr")) {
//                        imageView_RowProgram_OpType.setImageResource(R.drawable.program_ic_single);
//                    } else {
                        if (nPrgOpType5 == OP_TYPE_SINGLE) {
                            imageView_RowProgram_OpType.setImageResource(R.drawable.program_ic_single);
                        } else {
                            imageView_RowProgram_OpType.setImageResource(R.drawable.program_ic_double);
                        }
//                    }
                } else if(nTabIdx == 5) {
//                    if (Locale.getDefault().getLanguage().equals("fr")) {
//                        imageView_RowProgram_OpType.setImageResource(R.drawable.program_ic_double);
//                    } else {
                        if (nPrgOpType6 == OP_TYPE_SINGLE) {
                            imageView_RowProgram_OpType.setImageResource(R.drawable.program_ic_single);
                        } else {
                            imageView_RowProgram_OpType.setImageResource(R.drawable.program_ic_double);
                        }
//                    }
                } else if(nTabIdx == 6) {
                    if (nPrgOpType7 == OP_TYPE_SINGLE) {
                        imageView_RowProgram_OpType.setImageResource(R.drawable.program_ic_single);
                    } else {
                        imageView_RowProgram_OpType.setImageResource(R.drawable.program_ic_double);
                    }
                } else if(nTabIdx == 7) {
                    if (nPrgOpType8 == OP_TYPE_SINGLE) {
                        imageView_RowProgram_OpType.setImageResource(R.drawable.program_ic_single);
                    } else {
                        imageView_RowProgram_OpType.setImageResource(R.drawable.program_ic_double);
                    }
                }

                imageView_RowProgram_Mode.setVisibility(View.VISIBLE);

                textView_RowProgram_Pressure.setVisibility(View.VISIBLE);
                textView_RowProgram_Cycle.setVisibility(View.VISIBLE);
                textView_RowProgram_Time.setVisibility(View.VISIBLE);
                linearLayout_RowProgram_Del.setVisibility(View.VISIBLE);

                imageView_RowProgram_Del.setImageResource(R.drawable.program_btn_del);

                if (programData.nMode == PHCommon.OP_MODE_EXP) {
                    imageView_RowProgram_Mode.setImageResource(R.drawable.img_ex_off);
                } else if (programData.nMode == PHCommon.OP_MODE_MSG) {
                    imageView_RowProgram_Mode.setImageResource(R.drawable.img_ma_off);
                }

                textView_RowProgram_Pressure.setText(String.format("%d", programData.nPressure + 1));
                textView_RowProgram_Cycle.setText(String.format("%d", programData.nCycle + 1));

                int aMin = (int) (programData.nLen / 60);
                int aSec = (int) (programData.nLen % 60);
                textView_RowProgram_Time.setText(String.format("%02d:%02d", aMin, aSec));

            }


            return convertView;
        }
    }

    private void doPressureDown() {
        int aChartValue = 0;

        if (nProgramPopMode == PHCommon.OP_MODE_EXP) {
            nProgramPopPressureExp--;
            if(nProgramPopPressureExp < PHCommon.EXP_PRESSURE_MIN) {
                nProgramPopPressureExp = PHCommon.EXP_PRESSURE_MIN;
            }
            aChartValue = nProgramPopPressureExp;

        } else if (nProgramPopMode == PHCommon.OP_MODE_MSG) {
            nProgramPopPressureMsg--;
            if(nProgramPopPressureMsg < PHCommon.MSG_PRESSURE_MIN) {
                nProgramPopPressureMsg = PHCommon.MSG_PRESSURE_MIN;
            }
            aChartValue = nProgramPopPressureMsg;
        }

        refreshPressure(aChartValue);
    }

    private void doPressureUp() {
        int aChartValue = 0;

        if (nProgramPopMode == PHCommon.OP_MODE_EXP) {
            nProgramPopPressureExp++;
            if(nProgramPopPressureExp > PHCommon.EXP_PRESSURE_MAX) {
                nProgramPopPressureExp = PHCommon.EXP_PRESSURE_MAX;
            }
            aChartValue = nProgramPopPressureExp;

        } else if (nProgramPopMode == PHCommon.OP_MODE_MSG) {
            nProgramPopPressureMsg++;
            if(nProgramPopPressureMsg > PHCommon.arrPressureLimit[nProgramPopCycleMsg]) {
                nProgramPopPressureMsg = PHCommon.arrPressureLimit[nProgramPopCycleMsg];
            }
            aChartValue = nProgramPopPressureMsg;
        }

        refreshPressure(aChartValue);
    }

    private void doCycleDown() {
        int aChartValue = 0;

        if (nProgramPopMode == PHCommon.OP_MODE_EXP) {
            nProgramPopCycleExp--;
            if(nProgramPopCycleExp < PHCommon.EXP_CYCLE_MIN) {
                nProgramPopCycleExp = PHCommon.EXP_CYCLE_MIN;
            }
            aChartValue = nProgramPopCycleExp;

        } else if (nProgramPopMode == PHCommon.OP_MODE_MSG) {
            nProgramPopCycleMsg--;
            if(nProgramPopCycleMsg < PHCommon.MSG_CYCLE_MIN) {
                nProgramPopCycleMsg = PHCommon.MSG_CYCLE_MIN;
            }
            aChartValue = nProgramPopCycleMsg;

            if(nProgramPopPressureMsg > PHCommon.arrPressureLimit[nProgramPopCycleMsg]) {
                nProgramPopPressureMsg = PHCommon.arrPressureLimit[nProgramPopCycleMsg];
            }
            refreshPressure(nProgramPopPressureMsg);
        }

        refreshCycle(aChartValue);
    }

    private void doCycleUp() {
        int aChartValue = 0;

        if (nProgramPopMode == PHCommon.OP_MODE_EXP) {
            nProgramPopCycleExp++;
            if(nProgramPopCycleExp > PHCommon.EXP_CYCLE_MAX) {
                nProgramPopCycleExp = PHCommon.EXP_CYCLE_MAX;
            }
            aChartValue = nProgramPopCycleExp;

        } else if (nProgramPopMode == PHCommon.OP_MODE_MSG) {
            nProgramPopCycleMsg++;
            if(nProgramPopCycleMsg > PHCommon.MSG_CYCLE_MAX) {
                nProgramPopCycleMsg = PHCommon.MSG_CYCLE_MAX;
            }
            aChartValue = nProgramPopCycleMsg;

            if(nProgramPopPressureMsg > PHCommon.arrPressureLimit[nProgramPopCycleMsg]) {
                nProgramPopPressureMsg = PHCommon.arrPressureLimit[nProgramPopCycleMsg];
            }
            refreshPressure(nProgramPopPressureMsg);
        }

        refreshCycle(aChartValue);
    }

    private void refreshPressure(int a_ChartValue) {
        if (nProgramPopMode == PHCommon.OP_MODE_EXP) {
            seekBar_ActHome_Pressure.setMax(PHCommon.EXP_PRESSURE_MAX);

        } else if (nProgramPopMode == PHCommon.OP_MODE_MSG) {
            seekBar_ActHome_Pressure.setMax(PHCommon.arrPressureLimit[nProgramPopCycleMsg]+1);
        }
        seekBar_ActHome_Pressure.setProgress(a_ChartValue);
        textView_PopProgramEdit_Pressure.setText(String.format("%d", a_ChartValue + 1));
    }

    private void refreshCycle(int a_ChartValue) {
        if (nProgramPopMode == PHCommon.OP_MODE_EXP) {
            seekBar_ActHome_Cycle.setMax(PHCommon.EXP_CYCLE_MAX);
        } else if (nProgramPopMode == PHCommon.OP_MODE_MSG) {
            seekBar_ActHome_Cycle.setMax(PHCommon.MSG_CYCLE_MAX);
        }
        textView_PopProgramEdit_Cycle.setText(String.format("%d", a_ChartValue + 1));
        seekBar_ActHome_Cycle.setProgress(a_ChartValue);
    }

    private void refreshModeTap() {
        if (nProgramPopMode == PHCommon.OP_MODE_EXP) {
            imageView_PopProgramEdit_Expression.setImageResource(R.drawable.menu_ic_expr_ov);
            imageView_PopProgramEdit_Massage.setImageResource(R.drawable.menu_ic_mass);

            refreshPressure(nProgramPopPressureExp);
            refreshCycle(nProgramPopCycleExp);

        } else if (nProgramPopMode == PHCommon.OP_MODE_MSG) {
            imageView_PopProgramEdit_Expression.setImageResource(R.drawable.menu_ic_expr);
            imageView_PopProgramEdit_Massage.setImageResource(R.drawable.menu_ic_mass_ov);

            if(nProgramPopPressureMsg > PHCommon.arrPressureLimit[nProgramPopCycleMsg]) {
                nProgramPopPressureMsg = PHCommon.arrPressureLimit[nProgramPopCycleMsg];
            }
            refreshPressure(nProgramPopPressureMsg);
            refreshCycle(nProgramPopCycleMsg);
        }
    }

    private void goPrevSeqEdit(int a_Idx) {
        if (a_Idx < 0) {
            return;
        }
        initProgramEditPop(a_Idx);
    }

    private void goPrevSeqNext(int a_Idx) {
        if (a_Idx > arrData.size()-1) {
            return;
        }
        ProgramData programData = arrData.get(a_Idx-1);
        if(programData.nMode == OP_MODE_NONE) {
            PHLib.showMessageDialog(getString(R.string.PreviousSeqFirst), getActivity());
        } else {
            initProgramEditPop(a_Idx);
        }
    }

    private void initProgramEditPop(final int a_Idx) {
        LOGe(TAG, "initProgramEditPop:" + a_Idx);

        nCurEditIdx = a_Idx;

        TextView textView_PopProgramEdit_Title = (TextView) vPopDlg.findViewById(R.id.textView_PopProgramEdit_Title);
        textView_PopProgramEdit_Title.setText(String.format("%s %d", getString(R.string.Sequence), a_Idx + 1));

        seekBar_ActHome_Pressure = (SeekBar) vPopDlg.findViewById(R.id.seekBar_ActHome_Pressure);
        seekBar_ActHome_Cycle = (SeekBar) vPopDlg.findViewById(R.id.seekBar_ActHome_Cycle);

        //Expression, Massage
        imageView_PopProgramEdit_Expression = (ImageView) vPopDlg.findViewById(R.id.imageView_PopProgramEdit_Expression);
        imageView_PopProgramEdit_Massage = (ImageView) vPopDlg.findViewById(R.id.imageView_PopProgramEdit_Massage);
        imageView_PopProgramEdit_Single = (ImageView) vPopDlg.findViewById(R.id.imageView_PopProgramEdit_Single);
        imageView_PopProgramEdit_Double = (ImageView) vPopDlg.findViewById(R.id.imageView_PopProgramEdit_Double);

        linearLayout_PopProgramEdit_Expression = (LinearLayout) vPopDlg.findViewById(R.id.linearLayout_PopProgramEdit_Expression);
        linearLayout_PopProgramEdit_Massage = (LinearLayout) vPopDlg.findViewById(R.id.linearLayout_PopProgramEdit_Massage);
        linearLayout_PopProgramEdit_Single = (LinearLayout) vPopDlg.findViewById(R.id.linearLayout_PopProgramEdit_Single);
        linearLayout_PopProgramEdit_Double = (LinearLayout) vPopDlg.findViewById(R.id.linearLayout_PopProgramEdit_Double);
        linearLayout_PopProgramEdit_Prev = (LinearLayout) vPopDlg.findViewById(R.id.linearLayout_PopProgramEdit_Prev);
        linearLayout_PopProgramEdit_Next = (LinearLayout) vPopDlg.findViewById(R.id.linearLayout_PopProgramEdit_Next);

        if (a_Idx <= 0) {
            linearLayout_PopProgramEdit_Prev.setVisibility(View.GONE);
            linearLayout_PopProgramEdit_Next.setVisibility(View.VISIBLE);
        } else if (a_Idx >= arrData.size()-1) {
            linearLayout_PopProgramEdit_Prev.setVisibility(View.VISIBLE);
            linearLayout_PopProgramEdit_Next.setVisibility(View.GONE);
        } else {
            linearLayout_PopProgramEdit_Prev.setVisibility(View.VISIBLE);
            linearLayout_PopProgramEdit_Next.setVisibility(View.VISIBLE);
        }

        linearLayout_PopProgramEdit_Prev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goPrevSeqEdit(a_Idx-1);
            }
        });
        linearLayout_PopProgramEdit_Next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goPrevSeqNext(a_Idx+1);
            }
        });

        linearLayout_PopProgramEdit_Expression.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                nProgramPopMode = PHCommon.OP_MODE_EXP;

                refreshModeTap();
            }
        });
        linearLayout_PopProgramEdit_Massage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                nProgramPopMode = PHCommon.OP_MODE_MSG;

                refreshModeTap();
            }
        });

        linearLayout_PopProgramEdit_Single.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(PHCommon.MSG_SINGLEDOUBLE_RECV);
                intent.putExtra("singledouble_data", "single");
                getActivity().sendBroadcast(intent);

                selectOperationType(OP_TYPE_SINGLE);
            }
        });

        linearLayout_PopProgramEdit_Double.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(PHCommon.MSG_SINGLEDOUBLE_RECV);
                intent.putExtra("singledouble_data", "double");
                getActivity().sendBroadcast(intent);

                selectOperationType(OP_TYPE_DOUBLE);
            }
        });

        //Pressure, Cycle
        seekBar_ActHome_Pressure.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                if (nProgramPopMode == PHCommon.OP_MODE_EXP) {
                    nProgramPopPressureExp = i;
                } else if (nProgramPopMode == PHCommon.OP_MODE_MSG) {
                    nProgramPopPressureMsg = i;
                }
                refreshPressure(i);
            }
            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });

        seekBar_ActHome_Cycle.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                if (nProgramPopMode == PHCommon.OP_MODE_EXP) {
                    nProgramPopCycleExp = i;
                } else if (nProgramPopMode == PHCommon.OP_MODE_MSG) {
                    nProgramPopCycleMsg = i;

                    if(nProgramPopPressureMsg > PHCommon.arrPressureLimit[nProgramPopCycleMsg]) {
                        nProgramPopPressureMsg = PHCommon.arrPressureLimit[nProgramPopCycleMsg];
                    }
                    refreshPressure(nProgramPopPressureMsg);
                }
                refreshCycle(i);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });

        textView_PopProgramEdit_Pressure = (TextView) vPopDlg.findViewById(R.id.textView_PopProgramEdit_Pressure);
        textView_PopProgramEdit_Cycle = (TextView) vPopDlg.findViewById(R.id.textView_PopProgramEdit_Cycle);

        ImageButton imageButton_PopProgramEdit_PressureDown = (ImageButton) vPopDlg.findViewById(R.id.imageButton_PopProgramEdit_PressureDown);
        ImageButton imageButton_PopProgramEdit_PressureUp = (ImageButton) vPopDlg.findViewById(R.id.imageButton_PopProgramEdit_PressureUp);
        ImageButton imageButton_PopProgramEdit_CycleDown = (ImageButton) vPopDlg.findViewById(R.id.imageButton_PopProgramEdit_CycleDown);
        ImageButton imageButton_PopProgramEdit_CycleUp = (ImageButton) vPopDlg.findViewById(R.id.imageButton_PopProgramEdit_CycleUp);

        imageButton_PopProgramEdit_PressureDown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                doPressureDown();
            }
        });
        imageButton_PopProgramEdit_PressureUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                doPressureUp();
            }
        });
        imageButton_PopProgramEdit_CycleDown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                doCycleDown();
            }
        });
        imageButton_PopProgramEdit_CycleUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                doCycleUp();
            }
        });


        pickerViewDurationMin = (CharacterPickerView) vPopDlg.findViewById(R.id.pickerViewDurationMin);
        pickerViewDurationMin.setPicker(arrSeqMin);
        pickerViewDurationMin.setPickerTextColor(0xffe98a65, 0xffc5c5c5, getActivity().getResources().getColor(R.color.colorPrimary));
        pickerViewDurationSec = (CharacterPickerView) vPopDlg.findViewById(R.id.pickerViewDurationSec);
        pickerViewDurationSec.setPicker(arrSeqSec);
        pickerViewDurationSec.setPickerTextColor(0xffe98a65, 0xffc5c5c5, getActivity().getResources().getColor(R.color.colorPrimary));

        pickerViewDurationMin.setOnOptionChangedListener(new OnOptionChangedListener() {
            @Override
            public void onOptionChanged(int options1, int option2, int options3) {
//                LOGe(TAG, "pickerViewDurationSec onOptionChanged:" + options1 + " : " + option2 + " : " + options3);
                int []arrMin =  pickerViewDurationMin.getCurrentItems();
                if (arrMin[0] == 30) {
                    pickerViewDurationSec.setSelectOptions(0);
                }
            }
        });

        pickerViewDurationSec.setOnOptionChangedListener(new OnOptionChangedListener() {
            @Override
            public void onOptionChanged(int options1, int option2, int options3) {
//                LOGe(TAG, "pickerViewDurationSec onOptionChanged:" + options1 + " : " + option2 + " : " + options3);
                int []arrMin =  pickerViewDurationMin.getCurrentItems();
                if (arrMin[0] == 30) {
                    pickerViewDurationSec.setSelectOptions(0);
                }
            }
        });


        ImageButton imageButton_PopProgramEdit_Close = (ImageButton) vPopDlg.findViewById(R.id.imageButton_PopProgramEdit_Close);
        imageButton_PopProgramEdit_Close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dlgProgramEdit.cancel();
                nVoiceMode = VOICE_MODE_PROGRAM;
            }
        });
        Button button_PopProgramEdit_Cancel = (Button) vPopDlg.findViewById(R.id.button_PopProgramEdit_Cancel);
        button_PopProgramEdit_Cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dlgProgramEdit.cancel();
                nVoiceMode = VOICE_MODE_PROGRAM;
            }
        });
        Button button_PopProgramEdit_Save = (Button) vPopDlg.findViewById(R.id.button_PopProgramEdit_Save);
        button_PopProgramEdit_Save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dlgProgramEdit.cancel();
                nVoiceMode = VOICE_MODE_PROGRAM;

//                Menu02Activity actMenu02 = (Menu02Activity)getActivity();
//                actMenu02.selectOperationType(nProgramPopType);

//                selectOperationType(nProgramPopType);

                int []arrMin =  pickerViewDurationMin.getCurrentItems();
                int []arrSec =  pickerViewDurationSec.getCurrentItems();

                long aTime = arrMin[0]*60 + arrSec[0];
                if (aTime == 0) {
                    aTime = 1;
                }

                int aTimeSum = 0;
                for (int i = 0; i < arrData.size(); i++) {
                    ProgramData programData = arrData.get(i);
                    if (i != a_Idx) {
                        aTimeSum += programData.nLen;
                    }
                }

                LOGe(TAG, "aTimeSum:" + aTimeSum + " aTime:" + aTime);

                if (aTimeSum + aTime > 30*60) {
                    aTime = 30*60 - aTimeSum;
                    Toast.makeText(getActivity(), getString(R.string.ProgramTimeLimitTotal), Toast.LENGTH_SHORT).show();
                    if (aTime == 0) {
                        return ;
                    }
                }

                int aPrgOpType = PHCommon.OP_TYPE_SINGLE;
                switch (PHCommon.nCurSelPrgTabIdx) {
                    case 4:
                        if (nPrgOpType5 == PHCommon.OP_TYPE_NONE) {
                            nPrgOpType5 = nOperationType;
                        }
                        aPrgOpType = nPrgOpType5;
                        break;
                    case 5:
                        if (nPrgOpType6 == PHCommon.OP_TYPE_NONE) {
                            nPrgOpType6 = nOperationType;
                        }
                        aPrgOpType = nPrgOpType6;
                        break;
                    case 6:
                        if (nPrgOpType7 == PHCommon.OP_TYPE_NONE) {
                            nPrgOpType7 = nOperationType;
                        }
                        aPrgOpType = nPrgOpType7;
                        break;
                    case 7:
                        if (nPrgOpType8 == PHCommon.OP_TYPE_NONE) {
                            nPrgOpType8 = nOperationType;
                        }
                        aPrgOpType = nPrgOpType8;
                        break;
                    default:
                        break;
                }


                int aCode = (nTabIdx + 1) * 1000 + a_Idx;
                if (nProgramPopMode == PHCommon.OP_MODE_EXP) {
                    ProgramData programData = new ProgramData(aCode, nProgramPopMode, nProgramPopPressureExp, nProgramPopCycleExp, aTime, aPrgOpType);
                    setProgramData(a_Idx, programData);

                } else if (nProgramPopMode == PHCommon.OP_MODE_MSG) {
                    ProgramData programData = new ProgramData(aCode, nProgramPopMode, nProgramPopPressureMsg, nProgramPopCycleMsg, aTime, aPrgOpType);
                    setProgramData(a_Idx, programData);
                }

                ((Menu02Activity)getActivity()).refreshSaveDeleteAllButton();
            }
        });


        //Init Value
        ProgramData programData = arrData.get(a_Idx);
        nProgramPopMode = programData.nMode;
//        nProgramPopType = programData.nOpType;

        if (nProgramPopMode != PHCommon.OP_MODE_EXP && nProgramPopMode != PHCommon.OP_MODE_MSG) {
            nProgramPopMode = PHCommon.OP_MODE_EXP;
        }

        if(nProgramPopMode == PHCommon.OP_MODE_EXP) {
            nProgramPopPressureExp = programData.nPressure;
            nProgramPopCycleExp = programData.nCycle;

        } else if(nProgramPopMode == PHCommon.OP_MODE_MSG) {
            nProgramPopPressureMsg = programData.nPressure;
            nProgramPopCycleMsg = programData.nCycle;
        }

        refreshModeTap();

        refreshCurrentOpType();

        int aMin = (int) programData.nLen / 60;
        int aSec = (int) (programData.nLen % 60);

        if(aMin == 0 && aSec == 0) {
            aSec = 15;
        }

        pickerViewDurationMin.setSelectOptions(aMin);
        pickerViewDurationSec.setSelectOptions(aSec);
    }

    private void refreshCurrentOpType() {
        LOGe(TAG, "refreshCurrentOpType PHCommon.nCurSelPrgTabIdx:" + PHCommon.nCurSelPrgTabIdx);

        if (PHCommon.nCurSelPrgTabIdx == 0) {
            selectOperationType(PHCommon.OP_TYPE_SINGLE);

        } else if (PHCommon.nCurSelPrgTabIdx == 1) {
            selectOperationType(PHCommon.OP_TYPE_DOUBLE);

        } else if (PHCommon.nCurSelPrgTabIdx == 2) {
            selectOperationType(PHCommon.OP_TYPE_SINGLE);

        } else if (PHCommon.nCurSelPrgTabIdx == 3) {
            selectOperationType(PHCommon.OP_TYPE_DOUBLE);

        } else if (PHCommon.nCurSelPrgTabIdx == 4) {
            if (nPrgOpType5 == OP_TYPE_NONE) {
                selectOperationType(nOperationType);
            } else {
                selectOperationType(nPrgOpType5);
            }

        } else if (PHCommon.nCurSelPrgTabIdx == 5) {
            if (nPrgOpType6 == OP_TYPE_NONE) {
                selectOperationType(nOperationType);
            } else {
                selectOperationType(nPrgOpType6);
            }

        } else if (PHCommon.nCurSelPrgTabIdx == 6) {
            if (nPrgOpType7 == OP_TYPE_NONE) {
                selectOperationType(nOperationType);
            } else {
                selectOperationType(nPrgOpType7);
            }

        } else if (PHCommon.nCurSelPrgTabIdx == 7) {
            if (nPrgOpType8 == OP_TYPE_NONE) {
                selectOperationType(nOperationType);
            } else {
                selectOperationType(nPrgOpType8);
            }
        }
    }

    public void selectOperationType(int a_Type) {
        LOGe(TAG, "selectOperationType:" + a_Type);

        imageView_PopProgramEdit_Single.setBackgroundResource(R.drawable.menu_ic_single);
        imageView_PopProgramEdit_Double.setBackgroundResource(R.drawable.menu_ic_double);

        int aChangableTabIdx = 4;
//        if (Locale.getDefault().getLanguage().equals("fr")) {
//            aChangableTabIdx = 6;
//        }

        if (a_Type == OP_TYPE_SINGLE) {
            if (nCurSelPrgTabIdx >= aChangableTabIdx) {
                imageView_PopProgramEdit_Single.setBackgroundResource(R.drawable.menu_ic_single_ov);
            } else {
                imageView_PopProgramEdit_Double.setBackgroundResource(R.drawable.menu_ic_single_ov_disable);
            }

        } else if (a_Type == OP_TYPE_DOUBLE) {
            imageView_PopProgramEdit_Single.setBackgroundResource(R.drawable.menu_ic_single);
            if (nCurSelPrgTabIdx >= aChangableTabIdx) {
                imageView_PopProgramEdit_Double.setBackgroundResource(R.drawable.menu_ic_double_ov);
            } else {
                imageView_PopProgramEdit_Double.setBackgroundResource(R.drawable.menu_ic_double_ov_disable);
            }
        } else {
            imageView_PopProgramEdit_Single.setBackgroundResource(R.drawable.menu_ic_single);
            imageView_PopProgramEdit_Double.setBackgroundResource(R.drawable.menu_ic_double);
        }
    }

    private void showProgramEdit(final int a_Idx) {

        nVoiceMode = VOICE_MODE_PROGRAM_EDIT;

        dlgProgramEdit = new Dialog(getActivity(), R.style.fullDialog);
        LayoutInflater factory = LayoutInflater.from(getActivity());
        vPopDlg = factory.inflate(R.layout.pop_program_edit, null);
        dlgProgramEdit.setContentView(vPopDlg);
        dlgProgramEdit.getWindow().setLayout(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);

        initProgramEditPop(a_Idx);

        dlgProgramEdit.show();
    }

    private void setProgramData(int a_Idx, ProgramData a_Data) {
        ProgramData programData = arrData.get(a_Idx);
//        programData.nCode = a_Data.nCode;
        programData.nMode = a_Data.nMode;
        programData.nPressure = a_Data.nPressure;
        programData.nCycle = a_Data.nCycle;
        programData.nLen = a_Data.nLen;
        programData.nOpType = a_Data.nOpType;

//        LOGe(TAG, "programData.nCode:" + programData.nCode);
//        LOGe(TAG, "programData.nMode:" + programData.nMode);
//        LOGe(TAG, "programData.nPressure:" + programData.nPressure);
//        LOGe(TAG, "programData.nCycle:" + programData.nCycle);
//        LOGe(TAG, "programData.nLen:" + programData.nLen);

//        ProgramData programData01 = PHCommon.arrProgramData[nTabIdx].get(a_Idx);

//        LOGe(TAG, "programData01.nCode:" + programData01.nCode);
//        LOGe(TAG, "programData01.nMode:" + programData01.nMode);
//        LOGe(TAG, "programData01.nPressure:" + programData01.nPressure);
//        LOGe(TAG, "programData01.nCycle:" + programData01.nCycle);
//        LOGe(TAG, "programData01.nLen:" + programData01.nLen);

        adapterProgramList.notifyDataSetChanged();
        calcProgItemCount();
    }

}
