package com.bistos.bt150;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import com.bistos.bt150.com.PHCommon;
import com.bistos.bt150.com.PHLib;

import java.io.IOException;

/**
 * Created by lamb on 2017. 10. 10..
 */

public class IntroActivity extends Activity {

    private static final String TAG = "IntroActivity";

    private final static int APP_PERMISSIONS_REQ_STORAGE = 1100;
    private final static int APP_PERMISSIONS_REQ_CAMERA = 1200;
    private final static int ACCESS_FINE_LOCATION = 1300;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro);

        // create App directory
        try {
            PHLib.DirectoryCreate(getString(R.string.project_name) + "/camera");
            PHLib.DirectoryCreate(getString(R.string.project_name) + "/image");
            PHLib.DirectoryCreate(getString(R.string.project_name) + "/cache");
        } catch (IOException e) {
//			e.printStackTrace();
        }

        PHCommon.initProgramVar();

//        PHCommon.readProgramTable(this);

//        PHCommon.printProgramTable(this);

        checkAppAuth("android.permission.WRITE_EXTERNAL_STORAGE", APP_PERMISSIONS_REQ_STORAGE);

//        createMainActivity();

    }


    private void goAppStart() {
        createMainActivity();
    }

    private void createMainActivity() {

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(IntroActivity.this, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(intent);
                finish();
            }
        }, 500);
    }

    private void checkAppAuth(final String a_PermissionReq, final int a_ReqCode) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            int permissionResult = checkSelfPermission(a_PermissionReq);

            if (permissionResult == PackageManager.PERMISSION_DENIED) {
                if (shouldShowRequestPermissionRationale(a_PermissionReq)) {
                    requestPermissions(new String[]{a_PermissionReq}, a_ReqCode);
                } else {
                    requestPermissions(new String[]{a_PermissionReq}, a_ReqCode);
                }

            } else {
                doAfterAuthCheck(a_ReqCode);
            }

        } else {
            goAppStart();
        }

    }

    private void doAfterAuthCheck(int a_ReqCode) {

        Log.e(TAG, "doAfterAuthCheck:" + a_ReqCode);

        if (a_ReqCode == APP_PERMISSIONS_REQ_STORAGE) {
            checkAppAuth("android.permission.CAMERA", APP_PERMISSIONS_REQ_CAMERA);

        } else if (a_ReqCode == APP_PERMISSIONS_REQ_CAMERA) {
            checkAppAuth("android.permission.ACCESS_FINE_LOCATION", ACCESS_FINE_LOCATION);

        } else if (a_ReqCode == ACCESS_FINE_LOCATION) {
            goAppStart();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {

        if (grantResults.length > 0
                && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

            doAfterAuthCheck(requestCode);

        } else {
            // 권한 거부 : 사용자가 해당권한을 거부했을때 해주어야 할 동작을 수행합니다
            finish();
        }
    }
}
