package com.bistos.bt150.com;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bistos.bt150.MainActivity;
import com.bistos.bt150.R;
import com.bistos.bt150.menu.Menu01Activity;
import com.bistos.bt150.menu.Menu02Activity;
import com.bistos.bt150.menu.Menu03Activity;


/**
 * Created by lamb on 2017. 10. 11..
 */

public class TopMenu extends LinearLayout {

    private static final String TAG = "TopMenu";

    private Context mCtx = null;

    private LinearLayout linearLayout_CoTopMenu_Base;

    private LinearLayout linearLayout_CoTopMenu_Menu01;
    private LinearLayout linearLayout_CoTopMenu_Menu02;
    private LinearLayout linearLayout_CoTopMenu_Menu03;
    private LinearLayout linearLayout_CoTopMenu_Menu04;
    private LinearLayout linearLayout_CoTopMenu_Menu05;

    private ImageView imageView_CoTopMenu_Menu01;
    private ImageView imageView_CoTopMenu_Menu02;
    private ImageView imageView_CoTopMenu_Menu03;
    private ImageView imageView_CoTopMenu_Menu04;
    private ImageView imageView_CoTopMenu_Menu05;

    private int nSelIdx = 0;

    public static interface OnTabSelectListener {
        public void onSelect(int a_Idx);
    }

    private OnTabSelectListener mTabSelectListener;

    public void setOnTabSelectListener(OnTabSelectListener a_Listener) {
        mTabSelectListener = a_Listener;
    }

    public TopMenu(Context context) {
        super(context);

        initTopMenu(context);
    }

    public TopMenu(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);

        initTopMenu(context);
    }

    private void initTopMenu(Context context) {

        mCtx = context;

        String infService = Context.LAYOUT_INFLATER_SERVICE;
        LayoutInflater li = (LayoutInflater) getContext().getSystemService(infService);
        View v = li.inflate(R.layout.co_top_menu, this, false);
        addView(v);

        linearLayout_CoTopMenu_Base = v.findViewById(R.id.linearLayout_CoTopMenu_Base);

        linearLayout_CoTopMenu_Menu01 = v.findViewById(R.id.linearLayout_CoTopMenu_Menu01);
        linearLayout_CoTopMenu_Menu02 = v.findViewById(R.id.linearLayout_CoTopMenu_Menu02);
        linearLayout_CoTopMenu_Menu03 = v.findViewById(R.id.linearLayout_CoTopMenu_Menu03);
        linearLayout_CoTopMenu_Menu04 = v.findViewById(R.id.linearLayout_CoTopMenu_Menu04);
        linearLayout_CoTopMenu_Menu05 = v.findViewById(R.id.linearLayout_CoTopMenu_Menu05);

        linearLayout_CoTopMenu_Menu01.setOnClickListener(onClickMenu);
        linearLayout_CoTopMenu_Menu02.setOnClickListener(onClickMenu);
        linearLayout_CoTopMenu_Menu03.setOnClickListener(onClickMenu);
        linearLayout_CoTopMenu_Menu04.setOnClickListener(onClickMenu);
        linearLayout_CoTopMenu_Menu05.setOnClickListener(onClickMenu);


        imageView_CoTopMenu_Menu01 = v.findViewById(R.id.imageView_CoTopMenu_Menu01);
        imageView_CoTopMenu_Menu02 = v.findViewById(R.id.imageView_CoTopMenu_Menu02);
        imageView_CoTopMenu_Menu03 = v.findViewById(R.id.imageView_CoTopMenu_Menu03);
        imageView_CoTopMenu_Menu04 = v.findViewById(R.id.imageView_CoTopMenu_Menu04);
        imageView_CoTopMenu_Menu05 = v.findViewById(R.id.imageView_CoTopMenu_Menu05);

        selectMenu(1);

    }

    public void selectMenu(int a_Idx) {

        imageView_CoTopMenu_Menu01.setImageResource(R.drawable.menu_ic_1);
        imageView_CoTopMenu_Menu02.setImageResource(R.drawable.menu_ic_2);
        imageView_CoTopMenu_Menu03.setImageResource(R.drawable.menu_ic_3);
        imageView_CoTopMenu_Menu04.setImageResource(R.drawable.menu_ic_4);
        imageView_CoTopMenu_Menu05.setImageResource(R.drawable.menu_ic_5);

        linearLayout_CoTopMenu_Menu03.setVisibility(View.GONE);

        linearLayout_CoTopMenu_Base.setBackgroundResource(R.drawable.operation_bg);

        if (a_Idx == 1) {
            imageView_CoTopMenu_Menu02.setImageResource(R.drawable.menu_ic_2_ov);
//            linearLayout_CoTopMenu_Menu03.setVisibility(View.VISIBLE);
            linearLayout_CoTopMenu_Base.setBackgroundResource(R.drawable.operation_top_new);

        } else if (a_Idx == 2) {
            imageView_CoTopMenu_Menu04.setImageResource(R.drawable.menu_ic_4_ov);

        } else if (a_Idx == 3) {
            imageView_CoTopMenu_Menu05.setImageResource(R.drawable.menu_ic_5_ov);

        }

    }

    private OnClickListener onClickMenu = new OnClickListener() {
        @Override
        public void onClick(View view) {

            if (view.getId() == R.id.linearLayout_CoTopMenu_Menu02) {
                nSelIdx = 1;
            } else if (view.getId() == R.id.linearLayout_CoTopMenu_Menu04) {
                nSelIdx = 2;
            } else if (view.getId() == R.id.linearLayout_CoTopMenu_Menu05) {
                nSelIdx = 3;
            }
            mTabSelectListener.onSelect(nSelIdx);
            selectMenu(nSelIdx);

//            if (view.getId() == R.id.linearLayout_CoTopMenu_Menu01) {
//                goBackToMain();
//                return ;
//
//            } else if (view.getId() == R.id.linearLayout_CoTopMenu_Menu02) {
//                createOperationActivity();
//
////            } else if (view.getId() == R.id.linearLayout_CoTopMenu_Menu03) {
////                //Power
////                PowerDown();
////                return ;
//
//            } else if (view.getId() == R.id.linearLayout_CoTopMenu_Menu04) {
//                createProgramActivity();
//
//            } else if (view.getId() == R.id.linearLayout_CoTopMenu_Menu05) {
//                createStatisticActivity();
//            }

        }
    };

//    private void PowerDown() {
//        LOGe(TAG, "send power down!!!!!!!!!");
//        PHCommon.sendData(sCmdPowerDown);
//    }

    private void goBackToMain() {
        Intent intent = new Intent(mCtx, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_SINGLE_TOP);
        Bundle bundle = new Bundle();
        intent.putExtras(bundle);
        mCtx.startActivity(intent);
        ((Activity)mCtx).overridePendingTransition(0, 0);
    }

    private void createOperationActivity() {
        Intent intent = new Intent(mCtx, Menu01Activity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_SINGLE_TOP);
        Bundle bundle = new Bundle();
        intent.putExtras(bundle);
        mCtx.startActivity(intent);
        ((Activity)mCtx).overridePendingTransition(0, 0);
    }

    private void createProgramActivity() {
        Intent intent = new Intent(mCtx, Menu02Activity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_SINGLE_TOP);
        Bundle bundle = new Bundle();
        intent.putExtras(bundle);
        mCtx.startActivity(intent);
        ((Activity)mCtx).overridePendingTransition(0, 0);
    }

    private void createStatisticActivity() {
        Intent intent = new Intent(mCtx, Menu03Activity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_SINGLE_TOP);
        Bundle bundle = new Bundle();
        intent.putExtras(bundle);
        mCtx.startActivity(intent);
        ((Activity)mCtx).overridePendingTransition(0, 0);
    }
}
