package com.bistos.bt150.com;

import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 * Created by Lamb on 2017. 11. 13..
 * Philosys
 * philosys_dev@gmail.com
 */
public class MilkData {

    public long lDate;
    public int nAmtL;
    public int nAmtR;
    public long nTime;

    public MilkData() {
        initMilkData();
    }

    public MilkData(String a_Date, int a_Amt, long a_Time) {
        initMilkData(a_Date, a_Amt, a_Amt, a_Time);
    }

    public MilkData(String a_Date, int a_AmtL, int a_AmtR, long a_Time) {
        initMilkData(a_Date, a_AmtL, a_AmtR, a_Time);
    }

    public MilkData(long a_Date, int a_AmtL, int a_AmtR, long a_Time) {
        lDate = a_Date;
        nAmtL = a_AmtL;
        nAmtR = a_AmtR;
        nTime = a_Time;
    }

    public void initMilkData(String a_Date, int a_AmtL, int a_AmtR, long a_Time) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmm");
        formatter.setLenient(false);
        try {
            lDate = formatter.parse(a_Date).getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        nAmtL = a_AmtL;
        nAmtR = a_AmtR;
        nTime = a_Time;
    }

    public void initMilkData() {
        lDate = System.currentTimeMillis();
        nAmtL = 0;
        nAmtR = 0;
        nTime = 0;
    }

}
