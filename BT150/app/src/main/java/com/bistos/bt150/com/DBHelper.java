package com.bistos.bt150.com;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

import static com.bistos.bt150.com.PHLib.LOGe;

/**
 * Created by lamb on 2017. 10. 26..
 */

public class DBHelper extends SQLiteOpenHelper {

    private static final String TAG = "DBHelper";

    private static final String sDBName = "BT150.db";
    private static final int nDBVersion = 1;

    public static final String sTableProgram = "BT150_PROGRAM";
    private String sTableMilk    = "BT150_MILK";

    // DBHelper 생성자로 관리할 DB 이름과 버전 정보를 받음
    public DBHelper(Context context) {
        super(context, sDBName, null, nDBVersion);
    }

    // DB를 새로 생성할 때 호출되는 함수
    @Override
    public void onCreate(SQLiteDatabase db) {
        // 새로운 테이블 생성
        db.execSQL("CREATE TABLE " + sTableProgram + " (_id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "code INTEGER, " +
                "mode INTEGER, " +
                "pressure INTEGER, " +
                "cycle INTEGER, " +
                "duration REAL);");

        db.execSQL("CREATE TABLE " + sTableMilk + " (_id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "reg_date REAL, " +
                "left_amt INTEGER, " +
                "right_amt INTEGER, " +
                "duration REAL);");
    }

    // DB 업그레이드를 위해 버전이 변경될 때 호출되는 함수
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    //////////////////////////////////////////////////////////////////////////////
    //   Milk Data
    //
    public void insertMilkData(long a_Date, int a_Left, int a_Right, long a_Duration) {
        // 읽고 쓰기가 가능하게 DB 열기
        SQLiteDatabase db = getWritableDatabase();
        String aQuery = "INSERT INTO " + sTableMilk
                + " VALUES(null, " + a_Date
                + ", " + a_Left
                + ", " + a_Right
                + ", " + a_Duration + ");";
        LOGe(TAG, "aQuery:" + aQuery);
        // DB에 입력한 값으로 행 추가
        db.execSQL(aQuery);
        db.close();
    }

    public void updateMilkData(long a_Date, int a_Left, int a_Right, long a_Duration) {
        SQLiteDatabase db = getWritableDatabase();
        // 입력한 항목과 일치하는 행의 가격 정보 수정
        String aQuery = "UPDATE " + sTableMilk
                + " SET left_amt = " + a_Left
                + ", right_amt = " + a_Right
                + ", duration = " + a_Duration
                + " WHERE reg_date = " + a_Date + ";";
        LOGe(TAG, "aQuery:" + aQuery);
        db.execSQL(aQuery);
        db.close();
    }

    public void setMilkData(long a_Date, int a_Left, int a_Right, long a_Duration) {
        SQLiteDatabase db = getReadableDatabase();
        // DB에 있는 데이터를 쉽게 처리하기 위해 Cursor를 사용하여 테이블에 있는 모든 데이터 출력
        String aQuery = "SELECT reg_date FROM " + sTableMilk + " where reg_date = " + a_Date + ";";
        LOGe(TAG, "aQuery:" + aQuery);
        Cursor cursor = db.rawQuery(aQuery , null);
        int aCnt = 0;
        if (cursor != null) {
            aCnt = cursor.getCount();
        }
        cursor.close();
        db.close();

        if (aCnt > 0) {
            updateMilkData(a_Date, a_Left, a_Right, a_Duration);
        } else {
            insertMilkData(a_Date, a_Left, a_Right, a_Duration);
        }
    }

    public void deleteMilkData(long a_Date) {
        SQLiteDatabase db = getWritableDatabase();
        // 입력한 항목과 일치하는 행 삭제
        String aQuery = "DELETE FROM " + sTableMilk + " WHERE reg_date = " + a_Date + ";";
        LOGe(TAG, "aQuery:" + aQuery);
        db.execSQL(aQuery);
        db.close();
    }

//    public ArrayList<MilkData> getMilkData() {
//        ArrayList <MilkData> arrData = new ArrayList<MilkData>();
//        // 읽기가 가능하게 DB 열기
//        SQLiteDatabase db = getReadableDatabase();
//        // DB에 있는 데이터를 쉽게 처리하기 위해 Cursor를 사용하여 테이블에 있는 모든 데이터 출력
//        String aQuery = "SELECT * FROM " + sTableMilk;
//        LOGe(TAG, "aQuery:" + aQuery);
//        Cursor cursor = db.rawQuery(aQuery, null);
//        while (cursor.moveToNext()) {
//            ProgramData programData = new ProgramData();
//            arrData.add(new MilkData(cursor.getLong(1), cursor.getInt(2), cursor.getInt(3), cursor.getLong(4)));
//        }
//        cursor.close();
//        db.close();
//
//        return arrData;
//    }

    public ArrayList<MilkData> getMilkData(long a_From, long a_To) {
        SimpleDateFormat aSdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        LOGe(TAG, "getMilkData: from:" + aSdf.format(a_From) + " to:" + aSdf.format(a_To));

        ArrayList <MilkData> arrData = new ArrayList<MilkData>();
        // 읽기가 가능하게 DB 열기
        SQLiteDatabase db = getReadableDatabase();
        // DB에 있는 데이터를 쉽게 처리하기 위해 Cursor를 사용하여 테이블에 있는 모든 데이터 출력
        String aQuery = "SELECT * FROM " + sTableMilk +  " where reg_date between " + a_From + " and " + a_To;
        LOGe(TAG, "getMilkData aQuery:" + aQuery);
        Cursor cursor = db.rawQuery(aQuery, null);
        while (cursor.moveToNext()) {
            ProgramData programData = new ProgramData();
            arrData.add(new MilkData(cursor.getLong(1), cursor.getInt(2), cursor.getInt(3), cursor.getLong(4)));
        }
        cursor.close();
        db.close();

        return arrData;
    }


    //////////////////////////////////////////////////////////////////////////////
    //  Program Data
    //
    public void insertProgramData(int a_Code, int a_Mode, int a_Pressure, int a_Cycle, long a_Duration) {
        // 읽고 쓰기가 가능하게 DB 열기
        SQLiteDatabase db = getWritableDatabase();
        String aQuery = "INSERT INTO " + sTableProgram
                + " VALUES(null, " + a_Code
                + ", " + a_Mode
                + ", " + a_Pressure
                + ", " + a_Cycle
                + ", " + a_Duration + ");";
        LOGe(TAG, "aQuery:" + aQuery);
        // DB에 입력한 값으로 행 추가
        db.execSQL(aQuery);
        db.close();
    }

    public void updateProgramData(int a_Code, int a_Mode, int a_Pressure, int a_Cycle, long a_Duration) {
        SQLiteDatabase db = getWritableDatabase();
        // 입력한 항목과 일치하는 행의 가격 정보 수정
        String aQuery = "UPDATE " + sTableProgram
                + " SET mode = " + a_Mode
                + ", pressure = " + a_Pressure
                + ", cycle = " + a_Cycle
                + ", duration = " + a_Duration
                + " WHERE code = " + a_Code + ";";
        LOGe(TAG, "aQuery:" + aQuery);
        db.execSQL(aQuery);
        db.close();
    }

    public void setProgramData(int a_Code, int a_Mode, int a_Pressure, int a_Cycle, long a_Duration) {
        SQLiteDatabase db = getReadableDatabase();
        // DB에 있는 데이터를 쉽게 처리하기 위해 Cursor를 사용하여 테이블에 있는 모든 데이터 출력
        String aQuery = "SELECT code FROM " + sTableProgram + " where code = " + a_Code;
        LOGe(TAG, "aQuery:" + aQuery);
        Cursor cursor = db.rawQuery(aQuery , null);
        int aCnt = 0;
        if (cursor != null) {
            aCnt = cursor.getCount();
        }
        cursor.close();
        db.close();

        if (aCnt > 0) {
            updateProgramData(a_Code, a_Mode, a_Pressure, a_Cycle, a_Duration);
        } else {
            insertProgramData(a_Code, a_Mode, a_Pressure, a_Cycle, a_Duration);
        }
    }

    public void deleteProgramData(int a_No) {
        int aCodeS = a_No*1000;
        int aCodeE = (a_No+1)*1000;
        SQLiteDatabase db = getWritableDatabase();
        // 입력한 항목과 일치하는 행 삭제
        String aQuery = "DELETE FROM " + sTableProgram + " WHERE code >= " + aCodeS + " and code <" + aCodeE + ";";
        LOGe(TAG, "aQuery:" + aQuery);
        db.execSQL(aQuery);
        db.close();
    }

//    public ArrayList<ProgramData> getProgramData() {
//        ArrayList <ProgramData> arrData = new ArrayList<ProgramData>();
//        // 읽기가 가능하게 DB 열기
//        SQLiteDatabase db = getReadableDatabase();
//        // DB에 있는 데이터를 쉽게 처리하기 위해 Cursor를 사용하여 테이블에 있는 모든 데이터 출력
//        String aQuery = "SELECT * FROM " + sTableProgram;
//        LOGe(TAG, "aQuery:" + aQuery);
//        Cursor cursor = db.rawQuery(aQuery, null);
//        while (cursor.moveToNext()) {
//            ProgramData programData = new ProgramData();
//            arrData.add(new ProgramData(cursor.getInt(1), cursor.getInt(2), cursor.getInt(3), cursor.getInt(4), cursor.getLong(5)));
//        }
//        cursor.close();
//        db.close();
//
//        return arrData;
//    }

}

