package com.bistos.bt150.menu;

import android.content.Intent;
import android.os.Bundle;
import com.google.android.material.tabs.TabLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;
import android.widget.ImageButton;

import com.bistos.bt150.R;
import com.bistos.bt150.com.PHCommon;
import com.bistos.bt150.com.TopMenu;

import static com.bistos.bt150.com.PHCommon.ACT_MENU03;
import static com.bistos.bt150.com.PHCommon.VOICE_MODE_OPERATION;
import static com.bistos.bt150.com.PHCommon.nVoiceMode;

/**
 * Created by lamb on 2017. 10. 26..
 */

public class Menu03Activity extends AppCompatActivity {

    private static final String TAG = "Menu03Activity";

    private TopMenu topMenu;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private ImageButton imageButton_Toolbar_Add;
    private Menu03Fragment menu03Fragment;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu03);

        topMenu = (TopMenu) findViewById(R.id.topMenu);
        topMenu.setOnTabSelectListener(new TopMenu.OnTabSelectListener() {
            @Override
            public void onSelect(int a_Idx) {
                PHCommon.doMenuAction(a_Idx, Menu03Activity.this);
                finish();
            }
        });

        initMainLayout();
    }

    @Override
    protected void onResume() {
        super.onResume();

        PHCommon.nActivityNo = ACT_MENU03;
        nVoiceMode = VOICE_MODE_OPERATION;

        topMenu.selectMenu(3);
    }

    private void initMainLayout() {

        // Initializing the TabLayout
        tabLayout = (TabLayout) findViewById(R.id.tabLayout);
//        for (int i = 0; i < 8; i++) {
//            tabLayout.addTab(tabLayout.newTab().setText(String.format("%02d", i+1)));
//        }

        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.Day)));
        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.Week)));
        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.Month)));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        // Initializing ViewPager
        viewPager = (ViewPager) findViewById(R.id.pager);

        // Creating TabPagerAdapter adapter
        TabPagerAdapter pagerAdapter = new TabPagerAdapter(getSupportFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(pagerAdapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));

        // Set TabSelectedListener
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());

                Intent intent = new Intent(PHCommon.MSG_VIEW_SEL);
                intent.putExtra("position", tab.getPosition());
                sendBroadcast(intent);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });


    }

    public class TabPagerAdapter extends FragmentStatePagerAdapter {

        // Count number of tabs
        private int tabCount;

        public TabPagerAdapter(FragmentManager fm, int tabCount) {
            super(fm);
            this.tabCount = tabCount;
        }

        @Override
        public int getCount() {
            return tabCount;
        }

        @Override
        public Fragment getItem(int position) {

            menu03Fragment = new Menu03Fragment();
            Bundle args = new Bundle();
            args.putInt("position", position);
            menu03Fragment.setArguments(args);
            return menu03Fragment;
        }

    }

}