package com.bistos.bt150;

import android.app.Application;

import com.telit.terminalio.TIOManager;

/**
 * Created by lamb on 2017. 11. 1..
 */

public class BT150 extends Application {

    public static final String PERIPHERAL_ID_NAME = "com.telit.tiosample.peripheralId";

    @Override
    public void onCreate() {
        TIOManager.initialize(this.getApplicationContext());
    }

    @Override
    public void onTerminate() {
        TIOManager.getInstance().done();
    }

}
