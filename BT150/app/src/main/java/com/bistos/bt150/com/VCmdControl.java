package com.bistos.bt150.com;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.util.Log;

import com.bistos.bt150.R;

import java.util.ArrayList;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import static com.bistos.bt150.com.PHCommon.ACT_MENU01;
import static com.bistos.bt150.com.PHCommon.ACT_MENU02;
import static com.bistos.bt150.com.PHCommon.VOICE_MODE_PROGRAM_EDIT;
import static com.bistos.bt150.com.PHCommon.VOICE_REC_LANG_ENGLISH;
import static com.bistos.bt150.com.PHCommon.VOICE_REC_LANG_FRENCH;
import static com.bistos.bt150.com.PHCommon.VOICE_REC_LANG_KOREAN;
import static com.bistos.bt150.com.PHCommon.getVCmdResource;
import static com.bistos.bt150.com.PHCommon.nVoiceMode;
import static com.bistos.bt150.com.PHLib.LOGe;
import static com.bistos.bt150.com.VoiceCommand.CMD_CYCLE_DOWN;
import static com.bistos.bt150.com.VoiceCommand.CMD_CYCLE_UP;
import static com.bistos.bt150.com.VoiceCommand.CMD_LAMP;
import static com.bistos.bt150.com.VoiceCommand.CMD_MODE_CHANGE;
import static com.bistos.bt150.com.VoiceCommand.CMD_NONE;
import static com.bistos.bt150.com.VoiceCommand.CMD_NUMBER_1;
import static com.bistos.bt150.com.VoiceCommand.CMD_NUMBER_2;
import static com.bistos.bt150.com.VoiceCommand.CMD_NUMBER_3;
import static com.bistos.bt150.com.VoiceCommand.CMD_NUMBER_4;
import static com.bistos.bt150.com.VoiceCommand.CMD_NUMBER_5;
import static com.bistos.bt150.com.VoiceCommand.CMD_NUMBER_6;
import static com.bistos.bt150.com.VoiceCommand.CMD_NUMBER_7;
import static com.bistos.bt150.com.VoiceCommand.CMD_NUMBER_8;
import static com.bistos.bt150.com.VoiceCommand.CMD_PLAY;
import static com.bistos.bt150.com.VoiceCommand.CMD_POWER;
import static com.bistos.bt150.com.VoiceCommand.CMD_PRESSURE_DOWN;
import static com.bistos.bt150.com.VoiceCommand.CMD_PRESSURE_UP;
import static com.bistos.bt150.com.VoiceCommand.CMD_PROGRAMID_N;
import static com.bistos.bt150.com.VoiceCommand.CMD_SKIP;
import static com.bistos.bt150.com.VoiceCommand.CMD_STOP;
import static com.bistos.bt150.com.VoiceCommand.CMD_TYPE_NORMAL;
import static com.bistos.bt150.com.VoiceCommand.CMD_TYPE_STEP;

/**
 * Created by Lamb on 11/10/2018.
 * Philosys
 * philosys_dev@gmail.com
 */
public class VCmdControl {

    private static final String TAG = "VCmdControl";

    private static String sLastVCmdTrim = "";

    public static ArrayList <VoiceCommand> arrCmd = new ArrayList<VoiceCommand>();
    public static VoiceCommand cmdNone = new VoiceCommand(CMD_NONE, "");

//    public static HashMap<String, VoiceCommand> mapCmd = new HashMap <>();

//    private static Resources getVCmdResource(Context a_Ctx, String a_Language) {
//        Resources res = PHLib.getLocalizedResources(a_Ctx, Locale.KOREAN);
//        if (a_Language.equals(VOICE_REC_LANG_KOREAN)) {
//            res = PHLib.getLocalizedResources(a_Ctx, Locale.KOREAN);
//        } else {
//            res = PHLib.getLocalizedResources(a_Ctx, Locale.US);
//        }
//
//        return res;
//    }

    public static void initVoiceCommand(Context a_Ctx) {

        arrCmd.clear();

        Resources res = getVCmdResource(a_Ctx, PHCommon.curSpeechLanguage);

        arrCmd.add(new VoiceCommand(CMD_MODE_CHANGE, res.getString(R.string.CMD_MODE_CHANGE)));
//        arrCmd.add(new VoiceCommand(CMD_MODE_EXPRESS, res.getString(R.string.CMD_MODE_EXPRESS)));
//        arrCmd.add(new VoiceCommand(CMD_MODE_MASSAGE, res.getString(R.string.CMD_MODE_MASSAGE)));

        arrCmd.add(new VoiceCommand(CMD_PRESSURE_UP, res.getString(R.string.CMD_PRESSURE_UP)));
        arrCmd.add(new VoiceCommand(CMD_PRESSURE_DOWN, res.getString(R.string.CMD_PRESSURE_DOWN)));
//        arrCmd.add(new VoiceCommand(CMD_PRESSURE_N, res.getString(R.string.CMD_PRESSURE_N), CMD_TYPE_STEP));

        arrCmd.add(new VoiceCommand(CMD_CYCLE_UP, res.getString(R.string.CMD_CYCLE_UP)));
        arrCmd.add(new VoiceCommand(CMD_CYCLE_DOWN, res.getString(R.string.CMD_CYCLE_DOWN)));
//        arrCmd.add(new VoiceCommand(CMD_CYCLE_N, res.getString(R.string.CMD_CYCLE_N), CMD_TYPE_STEP));

//        arrCmd.add(new VoiceCommand(CMD_PROGRAMID_UP, res.getString(R.string.CMD_PROGRAMID_UP)));
//        arrCmd.add(new VoiceCommand(CMD_ID_UP, res.getString(R.string.CMD_ID_UP)));
//        arrCmd.add(new VoiceCommand(CMD_PROGRAMID_DOWN, res.getString(R.string.CMD_PROGRAMID_DOWN)));
//        arrCmd.add(new VoiceCommand(CMD_ID_DOWN, res.getString(R.string.CMD_ID_DOWN)));
        arrCmd.add(new VoiceCommand(CMD_PROGRAMID_N, res.getString(R.string.CMD_PROGRAMID_N), CMD_TYPE_STEP));

//        arrCmd.add(new VoiceCommand(CMD_ID_N, res.getString(R.string.CMD_ID_N), CMD_TYPE_STEP));

//        arrCmd.add(new VoiceCommand(CMD_PROGRAM_PLAY, res.getString(R.string.CMD_PROGRAM_PLAY)));
        arrCmd.add(new VoiceCommand(CMD_PLAY, res.getString(R.string.CMD_PLAY)));
//        arrCmd.add(new VoiceCommand(CMD_PROGRAM_STOP, res.getString(R.string.CMD_PROGRAM_STOP)));
        arrCmd.add(new VoiceCommand(CMD_STOP, res.getString(R.string.CMD_STOP)));
//        arrCmd.add(new VoiceCommand(CMD_PROGRAM_SKIP, res.getString(R.string.CMD_PROGRAM_SKIP)));
        arrCmd.add(new VoiceCommand(CMD_SKIP, res.getString(R.string.CMD_SKIP)));

        arrCmd.add(new VoiceCommand(CMD_POWER, res.getString(R.string.CMD_POWER)));
        arrCmd.add(new VoiceCommand(CMD_LAMP, res.getString(R.string.CMD_LAMP)));

//        arrCmd.add(new VoiceCommand(CMD_PROGRAM_ERASE, res.getString(R.string.CMD_PROGRAM_ERASE)));
//        arrCmd.add(new VoiceCommand(CMD_ERASE, res.getString(R.string.CMD_ERASE)));
//        arrCmd.add(new VoiceCommand(CMD_PROGRAM_START, res.getString(R.string.CMD_PROGRAM_START)));
//        arrCmd.add(new VoiceCommand(CMD_START, res.getString(R.string.CMD_START)));
//        arrCmd.add(new VoiceCommand(CMD_START, res.getString(R.string.CMD_START1)));
//        arrCmd.add(new VoiceCommand(CMD_PROGRAM_END, res.getString(R.string.CMD_PROGRAM_END)));
//        arrCmd.add(new VoiceCommand(CMD_END, res.getString(R.string.CMD_END)));
//        arrCmd.add(new VoiceCommand(CMD_END, res.getString(R.string.CMD_END1)));
//        arrCmd.add(new VoiceCommand(CMD_PRESSURE_SAVE, res.getString(R.string.CMD_PRESSURE_SAVE)));
//        arrCmd.add(new VoiceCommand(CMD_CYCLE_SAVE, res.getString(R.string.CMD_CYCLE_SAVE)));
//        arrCmd.add(new VoiceCommand(CMD_SECOND_UP, res.getString(R.string.CMD_SECOND_UP)));
//        arrCmd.add(new VoiceCommand(CMD_SECOND_DOWN, res.getString(R.string.CMD_SECOND_DOWN)));
//        arrCmd.add(new VoiceCommand(CMD_MINUTE_UP, res.getString(R.string.CMD_MINUTE_UP)));
//        arrCmd.add(new VoiceCommand(CMD_MINUTE_DOWN, res.getString(R.string.CMD_MINUTE_DOWN)));
//        arrCmd.add(new VoiceCommand(CMD_NEXT_SEQ, res.getString(R.string.CMD_NEXT_SEQ)));
//        arrCmd.add(new VoiceCommand(CMD_NEXT, res.getString(R.string.CMD_NEXT)));


        arrCmd.add(new VoiceCommand(CMD_NUMBER_1, res.getString(R.string.CMD_NUMBER_1)));
        arrCmd.add(new VoiceCommand(CMD_NUMBER_2, res.getString(R.string.CMD_NUMBER_2)));
        arrCmd.add(new VoiceCommand(CMD_NUMBER_3, res.getString(R.string.CMD_NUMBER_3)));
        arrCmd.add(new VoiceCommand(CMD_NUMBER_4, res.getString(R.string.CMD_NUMBER_4)));
        arrCmd.add(new VoiceCommand(CMD_NUMBER_5, res.getString(R.string.CMD_NUMBER_5)));
        arrCmd.add(new VoiceCommand(CMD_NUMBER_6, res.getString(R.string.CMD_NUMBER_6)));
        arrCmd.add(new VoiceCommand(CMD_NUMBER_7, res.getString(R.string.CMD_NUMBER_7)));
        arrCmd.add(new VoiceCommand(CMD_NUMBER_8, res.getString(R.string.CMD_NUMBER_8)));

    }

    public static void initAddingPhrase(String a_Language) {

        if (a_Language.equals(VOICE_REC_LANG_KOREAN)) {

            addPhrase(CMD_PRESSURE_UP, "늦게");

            addPhrase(CMD_PRESSURE_DOWN, "맞게");
            addPhrase(CMD_PRESSURE_DOWN, "낱개");
            addPhrase(CMD_PRESSURE_DOWN, "맛게");
            addPhrase(CMD_PRESSURE_DOWN, "업계");

            addPhrase(CMD_CYCLE_UP, "다르게");

            addPhrase(CMD_PROGRAMID_N, "프로그램 id");
            addPhrase(CMD_PROGRAMID_N, "변호");

            addPhrase(CMD_NUMBER_1, "일본");
            addPhrase(CMD_NUMBER_1, "일번");

            addPhrase(CMD_NUMBER_2, "이번");
            addPhrase(CMD_NUMBER_3, "삼번");
            addPhrase(CMD_NUMBER_4, "사번");
            addPhrase(CMD_NUMBER_5, "오번");
            addPhrase(CMD_NUMBER_6, "육번");
            addPhrase(CMD_NUMBER_7, "칠번");
            addPhrase(CMD_NUMBER_8, "팔번");


            addPhrase(CMD_NUMBER_4, "변호사");

        } else if (a_Language.equals(VOICE_REC_LANG_ENGLISH)) {
            addPhrase(CMD_MODE_CHANGE, "changing");
            addPhrase(CMD_MODE_CHANGE, "change it");
            addPhrase(CMD_MODE_CHANGE, "changey");

            //lower, hello, no, law, lou, blue, lowe, lowes
            addPhrase(CMD_PRESSURE_UP, "Hi");
            addPhrase(CMD_PRESSURE_DOWN, "lower");
            addPhrase(CMD_PRESSURE_DOWN, "hello");
            addPhrase(CMD_PRESSURE_DOWN, "no");
            addPhrase(CMD_PRESSURE_DOWN, "law");
            addPhrase(CMD_PRESSURE_DOWN, "lou");
            addPhrase(CMD_PRESSURE_DOWN, "blue");
            addPhrase(CMD_PRESSURE_DOWN, "lowe");
            addPhrase(CMD_PRESSURE_DOWN, "lowes");
            addPhrase(CMD_PRESSURE_DOWN, "lo");

            addPhrase(CMD_CYCLE_UP, "fest");
            addPhrase(CMD_CYCLE_UP, "best");
            addPhrase(CMD_CYCLE_UP, "first");
            addPhrase(CMD_CYCLE_UP, "past");
            addPhrase(CMD_CYCLE_UP, "pest");
            addPhrase(CMD_CYCLE_UP, "speedy");

            addPhrase(CMD_CYCLE_DOWN, "snow");
            addPhrase(CMD_CYCLE_DOWN, "below");

            addPhrase(CMD_LAMP, "lymph");
            addPhrase(CMD_LAMP, "lemfo");
            addPhrase(CMD_LAMP, "limp");
            addPhrase(CMD_LAMP, "light");
            addPhrase(CMD_LAMP, "right");
            addPhrase(CMD_LAMP, "length");
            addPhrase(CMD_LAMP, "planet");

            addPhrase(CMD_POWER, "paul");
            addPhrase(CMD_POWER, "how");
            addPhrase(CMD_POWER, "are");

            addPhrase(CMD_POWER, "poem");
            addPhrase(CMD_POWER, "piper");

        } else if (a_Language.equals(VOICE_REC_LANG_FRENCH)) {

        }

        printPhrase();
    }

    public static void printPhrase() {
        for (VoiceCommand aCmd : arrCmd) {
            aCmd.printPhrase();
        }
    }

    private static void addPhrase(int a_CmdID, String a_Phrase) {
        for (VoiceCommand aCmd : arrCmd) {
            if (aCmd.nCmdCode == a_CmdID) {
                aCmd.addPhrase(a_Phrase);
                break;
            }
        }
    }

    private static boolean checkCommandString(String a_Search, VoiceCommand a_Cmd) {

        String aChunk = a_Search.replace(" ", "");
        if (a_Cmd.checkCommand(aChunk)) {
            Log.e(TAG, "checkCommandString: aChunk:"  + aChunk);
            return true;
        }
        return false;
    }

    public static VoiceCommand findVoiceCommand(String a_Search, Context a_Ctx) {

        String aTrim = a_Search.trim();

        for (VoiceCommand aCmd : arrCmd) {

            if(PHCommon.curSpeechLanguage.equals(VOICE_REC_LANG_ENGLISH)) {
                int aStepNo = checkNumericValue(aTrim, a_Ctx);
                if (aStepNo > 0) {
                    if (sLastVCmdTrim.trim().toLowerCase().equals("number")) {
                        aTrim = sLastVCmdTrim + " " + aTrim;
                    }
                }
            }

            if (checkCommandString(aTrim, aCmd)) {
                if (aCmd.nCmdType == CMD_TYPE_NORMAL) {
                    return aCmd;
                } else {
                    //CMD_TYPE_STEP
                    String aStepVal = "";
                    if(PHCommon.curSpeechLanguage.equals(VOICE_REC_LANG_KOREAN)) {
                        aStepVal = aTrim.substring(aTrim.length()-1, aTrim.length());
                        Log.e(TAG, "aTrim:" + aTrim);
                        Log.e(TAG, "aStepVal:" + aStepVal);

                    } else {
                        String []aWord = aTrim.split(" ");
                        aStepVal = aWord[aWord.length-1];
                    }
                    int aStepNo = checkNumericValue(aStepVal, a_Ctx);
                    LOGe(TAG, "aStepVal:" + aStepVal + " aStepNo:" + aStepNo);
                    if (aStepNo > 0) {
                        aCmd.nStepVal = aStepNo;
                        return aCmd;
                    }
                }
            }
        }
        sLastVCmdTrim = aTrim;

        return cmdNone;
    }


    public static int checkNumericValue(String a_Value, Context a_Ctx) {

        int aRetVal = -1;

        if (PHLib.isNumeric(a_Value)) {
            aRetVal = PHLib.convertToInt(a_Value);

        } else {
            Resources res = getVCmdResource(a_Ctx, PHCommon.curSpeechLanguage);
            String aValue = a_Value.toLowerCase();

            if (aValue.equals(res.getString(R.string.SPEECH_NUMBER_ONE))) {
                aRetVal = 1;
            } else if (aValue.equals(res.getString(R.string.SPEECH_NUMBER_TWO))
                    || aValue.equals(res.getString(R.string.SPEECH_NUMBER_TWO1))
                    || aValue.equals(res.getString(R.string.SPEECH_NUMBER_TWO2))
                    || aValue.equals(res.getString(R.string.SPEECH_NUMBER_TWO3))
                    || aValue.equals(res.getString(R.string.SPEECH_NUMBER_TWO4))
                    || aValue.equals(res.getString(R.string.SPEECH_NUMBER_TWO5))
            ) {
                aRetVal = 2;
            } else if (aValue.equals(res.getString(R.string.SPEECH_NUMBER_THREE))) {
                aRetVal = 3;
            } else if (aValue.equals(res.getString(R.string.SPEECH_NUMBER_Four))
                    || aValue.equals(res.getString(R.string.SPEECH_NUMBER_Four1))
                    || aValue.equals(res.getString(R.string.SPEECH_NUMBER_Four2))
                    || aValue.equals(res.getString(R.string.SPEECH_NUMBER_Four3))
                    || aValue.equals(res.getString(R.string.SPEECH_NUMBER_Four4))
                    || aValue.equals(res.getString(R.string.SPEECH_NUMBER_Four5))
                    || aValue.equals(res.getString(R.string.SPEECH_NUMBER_Four6))
                    || aValue.equals(res.getString(R.string.SPEECH_NUMBER_Four7))
                    || aValue.equals(res.getString(R.string.SPEECH_NUMBER_Four8))
                    || aValue.equals(res.getString(R.string.SPEECH_NUMBER_Four9))
            ) {
                aRetVal = 4;
            } else if (aValue.equals(res.getString(R.string.SPEECH_NUMBER_Five))
                    || aValue.equals(res.getString(R.string.SPEECH_NUMBER_Five1))
                    || aValue.equals(res.getString(R.string.SPEECH_NUMBER_Five2))
            ) {
                aRetVal = 5;
            } else if (aValue.equals(res.getString(R.string.SPEECH_NUMBER_SIX))) {
                aRetVal = 6;
            } else if (aValue.equals(res.getString(R.string.SPEECH_NUMBER_SEVEN))) {
                aRetVal = 7;
            } else if (aValue.equals(res.getString(R.string.SPEECH_NUMBER_EIGHT))) {
                aRetVal = 8;
            } else if (aValue.equals(res.getString(R.string.SPEECH_NUMBER_NINE))) {
                aRetVal = 9;
            } else if (aValue.equals(res.getString(R.string.SPEECH_NUMBER_TEN))) {
                aRetVal = 10;
            }
        }

        return aRetVal;
    }

    private static void broadcastVoiceCommand(VoiceCommand a_Cmd, Context a_Ctx) {
        Intent intent = new Intent(PHCommon.MSG_VCMD_RECV);
        intent.putExtra("CmdCode", a_Cmd.nCmdCode);
        intent.putExtra("CmdType", a_Cmd.nCmdType);
        intent.putExtra("StepVal", a_Cmd.nStepVal);
        a_Ctx.sendBroadcast(intent);
    }

    private static String getCommandName(int a_Cmd, Context a_Ctx) {

        String aCommandName = "";
        Resources res = getVCmdResource(a_Ctx, PHCommon.curSpeechLanguage);

        switch (a_Cmd) {
            case CMD_MODE_CHANGE:
                aCommandName = res.getString(R.string.CMD_MODE_CHANGE);
                break;
//            case CMD_MODE_EXPRESS:
//                aCommandName = res.getString(R.string.CMD_MODE_EXPRESS);
//                break;
//            case CMD_MODE_MASSAGE:
//                aCommandName = res.getString(R.string.CMD_MODE_MASSAGE);
//                break;
            case CMD_PRESSURE_UP:
                aCommandName = res.getString(R.string.CMD_PRESSURE_UP);
                break;
            case CMD_PRESSURE_DOWN:
                aCommandName = res.getString(R.string.CMD_PRESSURE_DOWN);
                break;
            case CMD_CYCLE_UP:
                aCommandName = res.getString(R.string.CMD_CYCLE_UP);
                break;
            case CMD_CYCLE_DOWN:
                aCommandName = res.getString(R.string.CMD_CYCLE_DOWN);
                break;
//            case CMD_PROGRAMID_UP:
//                aCommandName = res.getString(R.string.CMD_PROGRAMID_UP);
//                break;
//            case CMD_ID_UP:
//                aCommandName = res.getString(R.string.CMD_ID_UP);
//                break;
//            case CMD_PROGRAMID_DOWN:
//                aCommandName = res.getString(R.string.CMD_PROGRAMID_DOWN);
//                break;
//            case CMD_ID_DOWN:
//                aCommandName = res.getString(R.string.CMD_ID_DOWN);
//                break;
//            case CMD_PROGRAM_PLAY:
//                aCommandName = res.getString(R.string.CMD_PROGRAM_PLAY);
//                break;
            case CMD_PLAY:
                aCommandName = res.getString(R.string.CMD_PLAY);
                break;
//            case CMD_PROGRAM_STOP:
//                aCommandName = res.getString(R.string.CMD_PROGRAM_STOP);
//                break;
            case CMD_STOP:
                aCommandName = res.getString(R.string.CMD_STOP);
                break;
//            case CMD_PROGRAM_SKIP:
//                aCommandName = res.getString(R.string.CMD_PROGRAM_SKIP);
//                break;
            case CMD_SKIP:
                aCommandName = res.getString(R.string.CMD_SKIP);
                break;
//            case CMD_PROGRAM_ERASE:
//                aCommandName = res.getString(R.string.CMD_PROGRAM_ERASE);
//                break;
//            case CMD_ERASE:
//                aCommandName = res.getString(R.string.CMD_ERASE);
//                break;
//            case CMD_PROGRAM_START:
//                aCommandName = res.getString(R.string.CMD_PROGRAM_START);
//                break;
//            case CMD_START:
//                aCommandName = res.getString(R.string.CMD_START);
//                break;
//            case CMD_PROGRAM_END:
//                aCommandName = res.getString(R.string.CMD_PROGRAM_END);
//                break;
//            case CMD_END:
//                aCommandName = res.getString(R.string.CMD_END);
//                break;
//            case CMD_PRESSURE_SAVE:
//                aCommandName = res.getString(R.string.CMD_PRESSURE_SAVE);
//                break;
//            case CMD_CYCLE_SAVE:
//                aCommandName = res.getString(R.string.CMD_CYCLE_SAVE);
//                break;
//            case CMD_SECOND_UP:
//                aCommandName = res.getString(R.string.CMD_SECOND_UP);
//                break;
//            case CMD_SECOND_DOWN:
//                aCommandName = res.getString(R.string.CMD_SECOND_DOWN);
//                break;
//            case CMD_MINUTE_UP:
//                aCommandName = res.getString(R.string.CMD_MINUTE_UP);
//                break;
//            case CMD_MINUTE_DOWN:
//                aCommandName = res.getString(R.string.CMD_MINUTE_DOWN);
//                break;
//            case CMD_NEXT_SEQ:
//                aCommandName = res.getString(R.string.CMD_NEXT_SEQ);
//                break;
//            case CMD_NEXT:
//                aCommandName = res.getString(R.string.CMD_NEXT);
//                break;
            default:
                aCommandName = "";
                break;
        }

        return aCommandName;
    }

    public static boolean handleVoiceCommand(VoiceCommand a_Cmd, Context a_Ctx) {

        boolean aRet = true;
//        VoiceCommand aCmd = findVoiceCommand(a_Phrase, a_Ctx);
        LOGe(TAG, "handleVoiceCommand:" + getCommandName(a_Cmd.nCmdCode, a_Ctx));
        if (!a_Cmd.equals(cmdNone)) {
            if (a_Cmd.nCmdType == CMD_TYPE_NORMAL) {
                switch (a_Cmd.nCmdCode) {
                    case CMD_POWER:
                    case CMD_LAMP:
                        if (PHCommon.nActivityNo != ACT_MENU01) {
                            PHCommon.createOperationActivity(a_Ctx);
                        }
                        sendMessageWhenActivityIsStarted(ACT_MENU01, a_Cmd, a_Ctx);
//                        PHCommon.toggleLamp();
                        break;
                    case CMD_MODE_CHANGE:
                        if (nVoiceMode == VOICE_MODE_PROGRAM_EDIT) {
                            broadcastVoiceCommand(a_Cmd, a_Ctx);

                        } else {
                            if (PHCommon.nActivityNo != ACT_MENU01) {
                                PHCommon.createOperationActivity(a_Ctx);
                            }
                            sendMessageWhenActivityIsStarted(ACT_MENU01, a_Cmd, a_Ctx);
                        }
                        break;
//                    case CMD_MODE_EXPRESS:
//                        if (nVoiceMode == VOICE_MODE_PROGRAM_EDIT) {
//                            broadcastVoiceCommand(a_Cmd, a_Ctx);
//
//                        } else {
//                            if (PHCommon.nActivityNo != ACT_MENU01) {
//                                PHCommon.createOperationActivity(a_Ctx);
//                            }
//                            sendMessageWhenActivityIsStarted(ACT_MENU01, a_Cmd, a_Ctx);
//                        }
//                        break;
//                    case CMD_MODE_MASSAGE:
//                        if (nVoiceMode == VOICE_MODE_PROGRAM_EDIT) {
//                            broadcastVoiceCommand(a_Cmd, a_Ctx);
//
//                        } else {
//                            if (PHCommon.nActivityNo != ACT_MENU01) {
//                                PHCommon.createOperationActivity(a_Ctx);
//                            }
//                            sendMessageWhenActivityIsStarted(ACT_MENU01, a_Cmd, a_Ctx);
//                        }
//                        break;
                    case CMD_PRESSURE_UP:
                        if (nVoiceMode == VOICE_MODE_PROGRAM_EDIT) {
                            broadcastVoiceCommand(a_Cmd, a_Ctx);

                        } else {
                            if (PHCommon.nActivityNo != ACT_MENU01) {
                                PHCommon.createOperationActivity(a_Ctx);
                            }
                            sendMessageWhenActivityIsStarted(ACT_MENU01, a_Cmd, a_Ctx);
                        }
                        break;
                    case CMD_PRESSURE_DOWN:
                        if (nVoiceMode == VOICE_MODE_PROGRAM_EDIT) {
                            broadcastVoiceCommand(a_Cmd, a_Ctx);

                        } else {
                            if (PHCommon.nActivityNo != ACT_MENU01) {
                                PHCommon.createOperationActivity(a_Ctx);
                            }
                            sendMessageWhenActivityIsStarted(ACT_MENU01, a_Cmd, a_Ctx);
                        }
                        break;
                    case CMD_CYCLE_UP:
                        if (nVoiceMode == VOICE_MODE_PROGRAM_EDIT) {
                            broadcastVoiceCommand(a_Cmd, a_Ctx);

                        } else {
                            if (PHCommon.nActivityNo != ACT_MENU01) {
                                PHCommon.createOperationActivity(a_Ctx);
                            }
                            sendMessageWhenActivityIsStarted(ACT_MENU01, a_Cmd, a_Ctx);
                        }
                        break;
                    case CMD_CYCLE_DOWN:
                        if (nVoiceMode == VOICE_MODE_PROGRAM_EDIT) {
                            broadcastVoiceCommand(a_Cmd, a_Ctx);

                        } else {
                            if (PHCommon.nActivityNo != ACT_MENU01) {
                                PHCommon.createOperationActivity(a_Ctx);
                            }
                            sendMessageWhenActivityIsStarted(ACT_MENU01, a_Cmd, a_Ctx);
                        }
                        break;
//                    case CMD_PROGRAMID_UP:
//                    case CMD_ID_UP:
//                        if (nVoiceMode == VOICE_MODE_PROGRAM_EDIT) {
//                        } else {
//                            if (PHCommon.nActivityNo != ACT_MENU02) {
//                                PHCommon.createProgramActivity(a_Ctx);
//                            }
//                            sendMessageWhenActivityIsStarted(ACT_MENU02, a_Cmd, a_Ctx);
//                        }
//                        break;
//                    case CMD_PROGRAMID_DOWN:
//                    case CMD_ID_DOWN:
//                        if (nVoiceMode == VOICE_MODE_PROGRAM_EDIT) {
//
//                        } else {
//                            if (PHCommon.nActivityNo != ACT_MENU02) {
//                                PHCommon.createProgramActivity(a_Ctx);
//                            }
//                            sendMessageWhenActivityIsStarted(ACT_MENU02, a_Cmd, a_Ctx);
//                        }
//                        break;
//                    case CMD_PROGRAM_PLAY:
                    case CMD_PLAY:
                        if (nVoiceMode == VOICE_MODE_PROGRAM_EDIT) {

                        } else {
                            if (PHCommon.nActivityNo != ACT_MENU02) {
                                PHCommon.createProgramActivity(a_Ctx);
                            }
                            sendMessageWhenActivityIsStarted(ACT_MENU02, a_Cmd, a_Ctx);
                        }
                        break;
//                    case CMD_PROGRAM_STOP:
                    case CMD_STOP:
                        if (nVoiceMode == VOICE_MODE_PROGRAM_EDIT) {

                        } else {
                            if (PHCommon.nActivityNo != ACT_MENU02) {
                                PHCommon.createProgramActivity(a_Ctx);
                            }
                            sendMessageWhenActivityIsStarted(ACT_MENU02, a_Cmd, a_Ctx);
                        }
                        break;
//                    case CMD_PROGRAM_SKIP:
                    case CMD_SKIP:
                        if (nVoiceMode == VOICE_MODE_PROGRAM_EDIT) {
                        } else {
                            if (PHCommon.nActivityNo != ACT_MENU02) {
                                PHCommon.createProgramActivity(a_Ctx);
                            }
                            sendMessageWhenActivityIsStarted(ACT_MENU02, a_Cmd, a_Ctx);
                        }
                        break;
//                    case CMD_PROGRAM_ERASE:
//                    case CMD_ERASE:
//                        if (nVoiceMode == VOICE_MODE_PROGRAM_EDIT) {
//                        } else {
//                            if (PHCommon.nActivityNo != ACT_MENU02) {
//                                PHCommon.createProgramActivity(a_Ctx);
//                            }
//                            sendMessageWhenActivityIsStarted(ACT_MENU02, a_Cmd, a_Ctx);
//                        }
//                        break;
//                    case CMD_PROGRAM_START:
//                    case CMD_START:
//                        if (nVoiceMode != VOICE_MODE_PROGRAM_EDIT || nVoiceMode != VOICE_MODE_PROGRAM) {
//                            PHCommon.createProgramActivity(a_Ctx);
//                        }
//                        sendMessageWhenActivityIsStarted(ACT_MENU02, a_Cmd, a_Ctx);
//                        break;
//                    case CMD_PROGRAM_END:
//                    case CMD_END:
//                        if (nVoiceMode == VOICE_MODE_PROGRAM_EDIT) {
//                            PHCommon.createProgramActivity(a_Ctx);
//                            broadcastVoiceCommand(a_Cmd, a_Ctx);
//                        }
//                        break;
//                    case CMD_PRESSURE_SAVE:
//                        if (nVoiceMode == VOICE_MODE_PROGRAM_EDIT) {
//                            broadcastVoiceCommand(a_Cmd, a_Ctx);
//                        }
//                        break;
//                    case CMD_CYCLE_SAVE:
//                        if (nVoiceMode == VOICE_MODE_PROGRAM_EDIT) {
//                            broadcastVoiceCommand(a_Cmd, a_Ctx);
//                        }
//                        break;
//                    case CMD_SECOND_UP:
//                        if (nVoiceMode == VOICE_MODE_PROGRAM_EDIT) {
//                            broadcastVoiceCommand(a_Cmd, a_Ctx);
//                        }
//                        break;
//                    case CMD_SECOND_DOWN:
//                        if (nVoiceMode == VOICE_MODE_PROGRAM_EDIT) {
//                            broadcastVoiceCommand(a_Cmd, a_Ctx);
//                        }
//                        break;
//                    case CMD_MINUTE_UP:
//                        if (nVoiceMode == VOICE_MODE_PROGRAM_EDIT) {
//                            broadcastVoiceCommand(a_Cmd, a_Ctx);
//                        }
//                        break;
//                    case CMD_MINUTE_DOWN:
//                        if (nVoiceMode == VOICE_MODE_PROGRAM_EDIT) {
//                            broadcastVoiceCommand(a_Cmd, a_Ctx);
//                        }
//                        break;
//                    case CMD_NEXT_SEQ:
//                        if (nVoiceMode == VOICE_MODE_PROGRAM_EDIT) {
//                            broadcastVoiceCommand(a_Cmd, a_Ctx);
//                        }
//                        break;
//                    case CMD_NEXT:
//                        if (nVoiceMode == VOICE_MODE_PROGRAM_EDIT) {
//                            broadcastVoiceCommand(a_Cmd, a_Ctx);
//                        }
//                        break;
                    case CMD_NUMBER_1:
                    case CMD_NUMBER_2:
                    case CMD_NUMBER_3:
                    case CMD_NUMBER_4:
                    case CMD_NUMBER_5:
                    case CMD_NUMBER_6:
                    case CMD_NUMBER_7:
                    case CMD_NUMBER_8:
                        if (nVoiceMode != VOICE_MODE_PROGRAM_EDIT) {
                            if (PHCommon.nActivityNo != ACT_MENU02) {
                                PHCommon.createProgramActivity(a_Ctx);
                            }
                            sendMessageWhenActivityIsStarted(ACT_MENU02, a_Cmd, a_Ctx);
                        }
                        break;
                    default:
                        aRet = false;
                        break;
                }

            } else if (a_Cmd.nCmdType == CMD_TYPE_STEP) {

                switch (a_Cmd.nCmdCode) {
//                    case CMD_PRESSURE_N:
//                        if (nVoiceMode != VOICE_MODE_PROGRAM_EDIT) {
//                            if (PHCommon.nActivityNo != ACT_MENU01) {
//                                PHCommon.createOperationActivity(a_Ctx);
//                            }
//                            sendMessageWhenActivityIsStarted(ACT_MENU01, a_Cmd, a_Ctx);
//                        }
//                        break;
//                    case CMD_CYCLE_N:
//                        if (nVoiceMode != VOICE_MODE_PROGRAM_EDIT) {
//                            if (PHCommon.nActivityNo != ACT_MENU01) {
//                                PHCommon.createOperationActivity(a_Ctx);
//                            }
//                            sendMessageWhenActivityIsStarted(ACT_MENU01, a_Cmd, a_Ctx);
//                        }
//                        break;
                    case CMD_PROGRAMID_N:
//                    case CMD_ID_N:
                        if (nVoiceMode != VOICE_MODE_PROGRAM_EDIT) {
                            if (PHCommon.nActivityNo != ACT_MENU02) {
                                PHCommon.createProgramActivity(a_Ctx);
                            }
                            sendMessageWhenActivityIsStarted(ACT_MENU02, a_Cmd, a_Ctx);
                        }
                        break;
                    default:
                        aRet = false;
                        break;
                }

            } else {
                aRet = false;
            }

        } else {
            aRet = false;
        }

        return aRet;
    }

    private static Timer timerSendMessage;
    private static int nTimerTaskSendMessageMax = 30;

    private static void stopSendMessageTimer() {
        nTimerTaskSendMessageMax = 30;

        if (timerSendMessage != null) {
            timerSendMessage.cancel();
            timerSendMessage.purge();
            timerSendMessage = null;
        }
    }

    private static void sendMessageWhenActivityIsStarted(final int a_ActID, final VoiceCommand a_Cmd, final Context a_Ctx) {

        stopSendMessageTimer();

        TimerTask timerTaskSendMessage = new TimerTask() {
            @Override
            public void run() {
                if(PHCommon.nActivityNo == a_ActID) {
                    stopSendMessageTimer();

                    broadcastVoiceCommand(a_Cmd, a_Ctx);
                }

                if (nTimerTaskSendMessageMax < 0) {
                    stopSendMessageTimer();
                }

                nTimerTaskSendMessageMax--;
            }
        };

        timerSendMessage = new Timer();
        timerSendMessage.schedule(timerTaskSendMessage, 100, 100);
    }

}
