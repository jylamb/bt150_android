package com.bistos.bt150.com;

/**
 * Created by kobonghwan on 2016. 11. 16..
 */

class DataChunkQue {

    private int QMAX = 128;
    public int nFront = 0;
    public int nRear = 0;
    private String[] dataChunkList = null;
    public boolean bUsed = false;

    public DataChunkQue() {
        initVars(QMAX);
        dataChunkList = new String[QMAX];
    }

    public DataChunkQue(int a_Max) {
        initVars(a_Max);
        dataChunkList = new String[QMAX];
    }

    private void initVars(int a_Max) {
        QMAX = a_Max;
        bUsed = false;
        InitQueue();
    }

    public void InitQueue() {
        nFront = 0;
        nRear = 0;
    }

    public String Put(String data) {
        //queue is full
        if ((nRear + 1) % QMAX == nFront) {
            return null;
        }
        dataChunkList[nRear] = data;
        nRear = ++nRear % QMAX;
        return data;
    }

    public String Get() {
        //queue is empty
        if (nFront == nRear) {
            return null;
        }
        String data = dataChunkList[nFront];
        nFront = ++nFront % QMAX;
        return data;
    }

    public boolean isEmpty () {
        return (nFront == nRear) ? true : false;
    }

}
