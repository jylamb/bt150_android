package com.bistos.bt150.com;

import static com.bistos.bt150.com.PHCommon.arrCycleExp;
import static com.bistos.bt150.com.PHCommon.arrCycleMsg;

/**
 * Created by lamb on 2017. 10. 15..
 */

public class ProgramData {

    public int nCode;
    public int nMode;
    public int nPressure;
    public int nCycle;
    public long nLen;
    public boolean bDownloaded;
    public int nOpType;

//    public int []arrCycleExp = {36, 40, 44, 48, 52, 56, 60};
//    public int []arrCycleMsg = {70, 80, 90};

    public ProgramData() {
        initProgramData();
    }

    public ProgramData(int a_Code, int a_Mode, int a_Pressure, int a_Cycle, long a_Len, int a_OpType) {
        nCode = a_Code;
        nMode = a_Mode;
        nPressure = a_Pressure;
        nCycle = a_Cycle;
        nLen = a_Len;
        bDownloaded = false;
        nOpType = a_OpType;
    }

    public void initProgramData() {

        nCode = 0;
        nMode = PHCommon.OP_MODE_NONE;
        nPressure = 0;
        nCycle = 0;
        nLen = 0;
        bDownloaded = false;
        nOpType = PHCommon.OP_TYPE_SINGLE;
    }

    public int getCycleValue() {

        if (nMode == PHCommon.OP_MODE_EXP
                && nCycle >= 0 && nCycle < arrCycleExp.length ) {
            return arrCycleExp[nCycle];

        } else if (nMode == PHCommon.OP_MODE_MSG
                && nCycle >= 0 && nCycle < arrCycleMsg.length ) {
            return arrCycleMsg[nCycle];
        }

        return 0;
    }


}
