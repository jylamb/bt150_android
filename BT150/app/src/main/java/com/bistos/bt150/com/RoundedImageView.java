package com.bistos.bt150.com;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.ImageView;

/**
 * Created by lamb on 2017. 10. 26..
 */

@SuppressLint("AppCompatCustomView")
public class RoundedImageView extends ImageView {

    public static final String TAG = "RoundedImageView";
//	private static int IMAGE_MAX_SIZE = 1024;

    public RoundedImageView(Context context) {
        super(context);
        // TODO Auto-generated constructor stub
    }

    public RoundedImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public RoundedImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        Drawable drawable = getDrawable();

        if (drawable == null) {
            return;
        }

        if (getWidth() == 0 || getHeight() == 0) {
            return;
        }
        Bitmap b = ((BitmapDrawable) drawable).getBitmap() ;
        if (b == null) {
            return;
        }
        Bitmap bitmap = b.copy(Bitmap.Config.ARGB_8888, true);
        int w = getWidth(), h = getHeight();

        Bitmap roundBitmap =  getCroppedBitmap(bitmap, w);
        canvas.drawBitmap(roundBitmap, 0,0, null);
    }

    public static Bitmap getCroppedBitmap(Bitmap bmp, int radius) {

//		Log.w(TAG, "radius:" + radius);
//		Log.w(TAG, "w:" + bmp.getWidth() + " h:" + bmp.getHeight());

        Bitmap sbmp = bmp;

        Bitmap output = Bitmap.createBitmap(radius, radius, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(output);

//		final int color = 0xffa19774;
        final Paint paint = new Paint();
        final Rect rectDst = new Rect(0, 0, radius, radius);

        Rect rectSrc;

        if(sbmp.getWidth() > sbmp.getHeight()) {
            int nLeft = (sbmp.getWidth()-sbmp.getHeight())/2;
            rectSrc = new Rect(nLeft, 0, nLeft+sbmp.getHeight(), sbmp.getHeight());
        } else {
            int nTop = (sbmp.getHeight()-sbmp.getWidth())/2;
            rectSrc = new Rect(0, nTop, sbmp.getWidth(), nTop+sbmp.getWidth());
        }
//		Log.w(TAG, String.format("%d %d %d %d", rectSrc.left, rectSrc.top));

        paint.setAntiAlias(true);
        paint.setFilterBitmap(true);
        paint.setDither(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(Color.parseColor("#BAB399"));
        canvas.drawCircle(radius/2, radius/2, radius/2, paint);
        paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
        canvas.drawBitmap(sbmp, rectSrc, rectDst, paint);

        return output;
    }

//	public static Bitmap drawableToBitmap (Drawable drawable) {
//		if (drawable instanceof BitmapDrawable) {
//			return ((BitmapDrawable)drawable).getBitmap();
//		}
//
//		Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Config.ARGB_8888);
//		Canvas canvas = new Canvas(bitmap);
//		drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
//		drawable.draw(canvas);
//
//		return bitmap;
//	}
}