package com.bistos.bt150.com;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import com.bistos.bt150.MainActivity;
import com.bistos.bt150.R;
import com.bistos.bt150.menu.Menu01Activity;
import com.bistos.bt150.menu.Menu02Activity;
import com.bistos.bt150.menu.Menu03Activity;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.utils.Utils;
import com.telit.terminalio.TIOConnection;
import com.telit.terminalio.TIOPeripheral;

import java.util.ArrayList;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.PriorityBlockingQueue;

import static com.bistos.bt150.com.PHLib.LOG;
import static com.bistos.bt150.com.PHLib.LOGe;

/**
 * Created by lamb on 2017. 10. 12..
 */

public class PHCommon {

    private static final String TAG = "PHCommon";


    public static final String MSG_DATA_RECV        = "MSG_DATA_RECV";
    public static final String MSG_VIEW_SEL         = "MSG_VIEW_SEL";
    public static final String MSG_PRGLIST_RELOAD   = "MSG_PRGLIST_RELOAD";
    public static final String MSG_VCMD_RECV        = "MSG_VCMD_RECV";
    public static final String MSG_CMD_DISCONNECT   = "MSG_CMD_DISCONNECT";
    public static final String MSG_SINGLEDOUBLE_RECV= "MSG_SINGLEDOUBLE_RECV";


    public static final int FW_BLOCK_SIZE = 0x80;
    public static final short FW_BLOCK_SIZE_HALF = 0x40;

    public static final int OP_MODE_NONE = 999;
    public static final int OP_MODE_MSG = 0;
    public static final int OP_MODE_EXP = 1;

    public static final int OP_TYPE_NONE = 9;
    public static final int OP_TYPE_SINGLE = 0;
    public static final int OP_TYPE_DOUBLE = 1;

    public static final int OP_PRESSURE = 300;
    public static final int OP_CYCLE    = 400;
    public static final int OP_TIMER    = 500;

    public static final int ST_WHICH_LEFT = 1000;
    public static final int ST_WHICH_RIGH = 2000;

    public static final int EXP_PRESSURE_MIN = 0;
    public static final int EXP_PRESSURE_MAX = 15;

    public static final int MSG_PRESSURE_MIN = 0;
//    public static final int MSG_PRESSURE_MAX = 7;

    public static final int EXP_CYCLE_MIN = 0;
//    public static final int EXP_CYCLE_MAX = 6;
    public static final int EXP_CYCLE_MAX = 5;

    public static final int MSG_CYCLE_MIN = 0;
//    public static final int MSG_CYCLE_MAX = 5;
    public static final int MSG_CYCLE_MAX = 2;

    public static final int ELAPSED_TIME_MAX = 60*30;


    public static final int []arrCycleExp = {36, 40, 44, 48, 52, 56, 60};
    public static final int []arrCycleMsg = {36, 40, 44, 48, 52, 56, 60};

//    public static final int []arrPressureLimit = {14, 12, 10, 9, 8, 6};
    public static final int []arrPressureLimit = {15, 15, 15, 15, 15, 15};

    public static final String sCmdModeChange = "M;";
    public static final String sCmdPressureUp = "PU;";
    public static final String sCmdPressureDown = "PD;";
    public static final String sCmdCycleUp = "CU;";
    public static final String sCmdCycleDown = "CD;";

    public static final String cCmdProgramRun = "R";
    public static final String sCmdProgramStop = "E;";
    public static final String sCmdProgramSkip = "S;";

    public static final String sCmdElapsedTime = "T;";
    public static final String sCmdInqueryStatus = "Q;";

    public static final String sCmdPowerDown = "G;";
    public static final String sCmdToggleLamp = "L;";
    public static final String sCmdInqueryBattery = "B;";

    public static final byte sCmdFWReadySend_Idle = 0x7f;
    public static final byte sCmdFWReadySend_Ready = 0x7e;
    public static final byte sCmdFWReadySend_Z = 'Z';
    public static final byte sCmdFWReadySend_X = 'X';
    public static final byte sCmdFWReadySend_V = 'V';
    public static final byte sCmdFWReadySend_U = 'U';


    public static final byte sCmdFWReadyRecv_R = 'R';
    public static final byte sCmdFWReadyRecv_E = 'E';
    public static final byte sCmdFWReadyRecv_D = 'D';
    public static final byte sCmdFWReadyRecv_Y = 'Y';
    public static final byte sCmdFWReadyRecv_CR = 0x0D;
    public static final byte sCmdFWReadyRecv_ERR = 0x3f;

    public static final byte sCmdFWEraseDelay = 0x7d;
    public static final byte sCmdFWErase = 'e';
    public static final byte sCmdFWErase0 = 0x7d;
    public static final byte sCmdFWErase1 = 0x7c;
    public static final byte sCmdFWErase10 = 0x7b;
    public static final byte sCmdFWAddress = 'A';
    public static final byte sCmdFWBinary = 'B';
    public static final byte sCmdFWFlash = 'F';
    public static final byte sCmdFWEnd = 0x7e;

    public static ArrayList<ProgramData> [] arrProgramData = new ArrayList[8];

    public static int nPrgReqCnt5 = 0;
    public static int nPrgReqCnt6 = 0;
    public static int nPrgReqCnt7 = 0;
    public static int nPrgReqCnt8 = 0;

    public static int nPrgOpType5 = OP_TYPE_DOUBLE;
    public static int nPrgOpType6 = OP_TYPE_DOUBLE;
    public static int nPrgOpType7 = OP_TYPE_NONE;
    public static int nPrgOpType8 = OP_TYPE_NONE;

    public static final int[] PIECHART_COLORS = new int[]{0xffffa500, 0xffb2aca1};

    public static String gRecvData = "";
    public static String gRecvBattery = "";
    public static int nElapsedTime = 0;
    public static int nExpressionTimeS = 0;
    public static int nOperationModeCur = PHCommon.OP_MODE_NONE;
    public static boolean bProgramDataLoaded = false;
    public static boolean bRecvProgramCnt = false;
    public static boolean bProcessingProgramData = false;
    public static boolean bDeviceWakeUp = false;

    public static int nCurSelPrgTabIdx = 0;
    public static boolean bProgramRunning = false;

    public static TIOPeripheral mPeripheral;
    public static TIOConnection mConnection;

    public static int nOperationType = PHCommon.OP_TYPE_SINGLE;
    public static int nOperationMode = PHCommon.OP_MODE_NONE;
    public static int nPressureExp = 0;
    public static int nCycleExp = 0;
    public static int nPressureMsg = 0;
    public static int nCycleMsg = 0;

    public static final int ACT_MAIN      = 100;
    public static final int ACT_MENU01    = 200;
    public static final int ACT_MENU02    = 300;
    public static final int ACT_MENU03    = 400;

    public static final int VOICE_MODE_OPERATION       = 100;
    public static final int VOICE_MODE_PROGRAM         = 200;
    public static final int VOICE_MODE_PROGRAM_EDIT    = 300;

    public static final String VOICE_REC_LANG_KOREAN = "ko-KR";
    public static final String VOICE_REC_LANG_ENGLISH = "en-US";
    public static final String VOICE_REC_LANG_FRENCH = "fr-FR";

    public static int nVoiceMode = VOICE_MODE_OPERATION;

    public static int nActivityNo = ACT_MAIN;

    public static String curSpeechLanguage = VOICE_REC_LANG_KOREAN;
    public static int curDevSearchTimeOut = 1;
    public static long curDevSearchTime = 0;
    public static boolean bVCmdUseOnline = true;
    public static boolean bVCmdShowResult = true;
    public static boolean bVCmdEnabled = true;

    public static int nProgramLastTime = 0;

    public static double toOZ  = 0.033814;
    public static double toML  = 29.57353;

    private static DataChunkQue dataChunkQue = new DataChunkQue();
    private static Thread sendBLEThread = null;
    private static boolean bSendBLEThread = false;

    private static Timer mTimerElapsed = null;
    private static TimerTask taskElapsed = null;

    public static byte sCmdFWLast = 0x00;
    public static byte sCmdFWRecv = sCmdFWReadySend_Idle;
    public static short nSendDataIdx = 0;


    public static void initProgramVar() {
        for(int i = 0; i < 8; i++) {
            arrProgramData[i] = new ArrayList<ProgramData>();
            for (int j = 0; j < 8; j++) {
                arrProgramData[i].add(new ProgramData());
            }
        }
    }

    public static void showExitAlert(final Activity a_Act) {

        String alertTitle = a_Act.getResources().getString(R.string.app_name);
        String buttonMessage = a_Act.getResources().getString(R.string.QuitAlert);
        String buttonYes = a_Act.getResources().getString(R.string.YES);
        String buttonNo = a_Act.getResources().getString(R.string.NO);

        new AlertDialog.Builder(a_Act)
                .setTitle(alertTitle)
                .setMessage(buttonMessage)
                .setPositiveButton(buttonYes, new DialogInterface.OnClickListener()
                {

                    @Override
                    public void onClick(DialogInterface dialog, int which)
                    {
                        PHLib.killApp(a_Act);
                    }
                })
                .setNegativeButton(buttonNo, null)
                .show();


    }

    public static void drawGraphItem(ArrayList<ProgramData> arrProgramData, int a_Type, ArrayList<ILineDataSet> a_DataSet, Context a_Ctx) {
        int aIdx = 0;
        float fPreValue = 0f;

        ArrayList<Entry> arrPressureExp = new ArrayList<Entry>();
        for (int i = 0; i < arrProgramData.size(); i++) {
            ProgramData programData = arrProgramData.get(i);
            for (int j = 0; j < programData.nLen; j++) {
                if (aIdx == 1) {
                    arrPressureExp.add(new Entry(aIdx-0.1f, fPreValue, a_Ctx.getResources().getDrawable(R.drawable.ic_offline_pin_black_18dp)));
                }
                if (programData.nMode == PHCommon.OP_MODE_EXP) {
                    if (a_Type == PHCommon.OP_PRESSURE) {
                        fPreValue = 100f*((float) programData.nPressure/16f);

                    } else if (a_Type == PHCommon.OP_CYCLE) {
//                        fPreValue = 100f*((float)programData.nCycle/7f);
                        fPreValue = programData.getCycleValue();
                    }
                    arrPressureExp.add(new Entry(aIdx, fPreValue, a_Ctx.getResources().getDrawable(R.drawable.ic_offline_pin_black_18dp)));
                } else {
                    arrPressureExp.add(new Entry(aIdx, 0f, a_Ctx.getResources().getDrawable(R.drawable.ic_offline_pin_black_18dp)));
                    fPreValue = 0f;
                }
                aIdx++;
            }
        }
        a_DataSet.add(addData(arrPressureExp, a_Type, PHCommon.OP_MODE_EXP, a_Ctx));

        aIdx = 0;
        fPreValue = 0f;
        ArrayList<Entry> arrPressureMsg = new ArrayList<Entry>();
        for (int i = 0; i < arrProgramData.size(); i++) {
            ProgramData programData = arrProgramData.get(i);
            for (int j = 0; j < programData.nLen; j++) {
                if (aIdx == 1) {
                    arrPressureMsg.add(new Entry(aIdx-0.1f, fPreValue, a_Ctx.getResources().getDrawable(R.drawable.ic_offline_pin_black_18dp)));
                }
                if (programData.nMode == PHCommon.OP_MODE_MSG) {
                    if (a_Type == PHCommon.OP_PRESSURE) {
                        fPreValue = 100f*((float) programData.nPressure/16f);
                    } else if (a_Type == PHCommon.OP_CYCLE) {
//                        fPreValue = 100f*((float)programData.nCycle/7f);
                        fPreValue = programData.getCycleValue();
                    }
                    arrPressureMsg.add(new Entry(aIdx, fPreValue, a_Ctx.getResources().getDrawable(R.drawable.ic_offline_pin_black_18dp)));
                } else {
                    arrPressureMsg.add(new Entry(aIdx, 0f, a_Ctx.getResources().getDrawable(R.drawable.ic_offline_pin_black_18dp)));
                    fPreValue = 0f;
                }
                aIdx++;
            }
        }
        a_DataSet.add(addData(arrPressureMsg, a_Type, PHCommon.OP_MODE_MSG, a_Ctx));

        aIdx = 0;
        fPreValue = 0f;
        ArrayList<Entry> arrPressure = new ArrayList<Entry>();
        for (int i = 0; i < arrProgramData.size(); i++) {
            ProgramData programData = arrProgramData.get(i);
            for (int j = 0; j < programData.nLen; j++) {
                if (a_Type == PHCommon.OP_PRESSURE) {
                    fPreValue = 100f*((float) programData.nPressure/16f);
                } else if (a_Type == PHCommon.OP_CYCLE) {
//                    fPreValue = 100f*((float)programData.nCycle/7f);
                    fPreValue = programData.getCycleValue();
                }
                arrPressure.add(new Entry(aIdx, fPreValue, a_Ctx.getResources().getDrawable(R.drawable.ic_offline_pin_black_18dp)));
                aIdx++;
            }
        }
        a_DataSet.add(addData(arrPressure, a_Type, PHCommon.OP_MODE_NONE, a_Ctx));
    }

    private static LineDataSet addData(ArrayList<Entry> a_Data, int a_Type, int a_Mode, Context a_Ctx) {

        LineDataSet lineDataSet = new LineDataSet(a_Data, "");

        lineDataSet.setDrawIcons(false);
        lineDataSet.setColor(Color.TRANSPARENT);
        lineDataSet.setCircleColor(Color.TRANSPARENT);
        lineDataSet.setLineWidth(0f);
        lineDataSet.setCircleRadius(1f);
        lineDataSet.setDrawCircleHole(false);
        lineDataSet.setValueTextSize(9f);
        lineDataSet.setDrawFilled(true);
        lineDataSet.setMode(LineDataSet.Mode.STEPPED);
        lineDataSet.setDrawValues(false);
        lineDataSet.setDrawCircles(false);

        if (Utils.getSDKInt() >= 18) {
            int aColor = Color.BLACK;
            if (a_Type == PHCommon.OP_PRESSURE) {
                aColor = a_Ctx.getResources().getColor(R.color.colorOrange);
            } else if (a_Type == PHCommon.OP_CYCLE) {
                aColor = Color.BLUE;
            }

            lineDataSet.setColor(aColor);
            lineDataSet.setCircleColor(aColor);
            lineDataSet.setValueTextColor(aColor);


            if (a_Mode == PHCommon.OP_MODE_EXP) {
//                Drawable drawable = ContextCompat.getDrawable(a_Ctx, R.drawable.fade_red);
//                lineDataSet.setFillDrawable(drawable);
                lineDataSet.setFillColor(a_Ctx.getResources().getColor(R.color.colorAccent));
                lineDataSet.setColor(Color.TRANSPARENT);

            } else if (a_Mode == PHCommon.OP_MODE_MSG) {
//                Drawable drawable = ContextCompat.getDrawable(a_Ctx, R.drawable.fade_blue);
//                lineDataSet.setFillDrawable(drawable);
                lineDataSet.setFillColor(a_Ctx.getResources().getColor(R.color.colorPrimary));
                lineDataSet.setColor(Color.TRANSPARENT);

            } else {
                lineDataSet.setLineWidth(1f);
                lineDataSet.setDrawFilled(false);
                lineDataSet.setDrawValues(true);
                lineDataSet.setDrawCircles(true);
            }

        }
        else {
            lineDataSet.setFillColor(Color.BLACK);
        }

        return lineDataSet;
    }

    public static int getCycleValue(int a_Mode, int a_CycleStep) {

        if (a_Mode == PHCommon.OP_MODE_EXP
                && a_CycleStep >= 0 && a_CycleStep < arrCycleExp.length ) {
            return arrCycleExp[a_CycleStep];

        } else if (a_Mode == PHCommon.OP_MODE_MSG
                && a_CycleStep >= 0 && a_CycleStep < arrCycleMsg.length ) {
            return arrCycleMsg[a_CycleStep];
        }

        return 0;
    }

//    public static void readProgramTable(Context a_Ctx) {
//        DBHelper glcHelper = new DBHelper(a_Ctx);	//DB 헬퍼 할당.
//        ArrayList<ProgramData> programDataList = glcHelper.getProgramData();
//        for (int i = 0; i < programDataList.size(); i++) {
//            ProgramData programData = programDataList.get(i);
//            int aIdx = programData.nCode / 1000 - 1;
//            LOGe(TAG, "aIdx:" + aIdx);
//            if (aIdx >= 0 && aIdx < 8) {
//                int aSeq = programData.nCode % (1000 * (aIdx + 1));
//                LOGe(TAG, "aSeq:" + aSeq);
//                arrProgramData[aIdx].get(aSeq).nCode = programData.nCode;
//                arrProgramData[aIdx].get(aSeq).nMode = programData.nMode;
//                arrProgramData[aIdx].get(aSeq).nPressure = programData.nPressure;
//                arrProgramData[aIdx].get(aSeq).nCycle = programData.nCycle;
//                arrProgramData[aIdx].get(aSeq).nLen = programData.nLen;
//            }
//            LOGe(TAG,i + " : " + programData.nCode + " " + programData.nMode + " " + programData.nPressure + " " + programData.nCycle + " " + programData.nLen);
//        }
//
//        glcHelper.close();
//    }

//    public static void printProgramTable(Context a_Ctx) {
//
//        DBHelper glcHelper = new DBHelper(a_Ctx);	//DB 헬퍼 할당.
////        SQLiteDatabase db = glcHelper.getReadableDatabase();
////
////        Cursor cursor;
////        cursor = db.rawQuery("SELECT code, mode, pressure, cycle, duration FROM "
////                + DBHelper.sTableProgram
////                + " order by code desc", null);
////
////        while(cursor.moveToNext()) {
////            int aCode = cursor.getInt(0);
////            int aMode = cursor.getInt(1);
////            int aPressure = cursor.getInt(2);
////            int aCycle = cursor.getInt(3);
////            long aDuration = cursor.getLong(4);
////
////            LOGe(TAG, "***********************************");
////            LOGe(TAG, "aCode:" + aCode);
////            LOGe(TAG, "aMode:" + aMode);
////            LOGe(TAG, "aPressure:" + aPressure);
////            LOGe(TAG, "aCycle:" + aCycle);
////            LOGe(TAG, "aDuration:" + aDuration);
////        }
////        cursor.close();
//
//        ArrayList<ProgramData> programDataList = glcHelper.getProgramData();
//        for (int i = 0; i < programDataList.size(); i++) {
//            ProgramData programData = programDataList.get(i);
//            LOGe(TAG,i + " : " + programData.nCode + " " + programData.nMode + " " + programData.nPressure + " " + programData.nCycle + " " + programData.nLen);
//        }
//
//        glcHelper.close();
//    }

    public static void initSendingThread() {

//        if(!bSendBLEThread) {
//            bSendBLEThread = true;
//
//            sendBLEThread = new Thread(new Runnable() {
//                @Override
//                public void run() {
//                    LOG(TAG, "!!!!!!!!!!!!!!!!!!!!! sendBLEThread Started !!!!!!!!!!!!!!!!!!!!!");
//                    doRecvParsingThread();
//                }
//            }, "Send Data BLE Thread");
//            sendBLEThread.setDaemon(true);
//            sendBLEThread.start();
//        }

        doRecvParsingThread();
    }

    public static void stopSendingThread() {
//        bSendBLEThread = false;
//
//        if(sendBLEThread != null) {
//            sendBLEThread.interrupt();
//            sendBLEThread = null;
//        }

        stopElapsedTimer();
    }

//    public static int nBLESendInterval = 50;
//    public static int nBLESendInterval = 75;
//    public static int nBLESendInterval = 250;
    public static int nBLESendInterval = 100;

    private static void doRecvParsingThread() {

//        while (bSendBLEThread) {
//            PHLib.Sleep(100);
//
//            while (!dataChunkQue.isEmpty()) {
//                String aData = dataChunkQue.Get();
//                sendDataBLE(aData);
//            }
//        }

        stopElapsedTimer();

        taskElapsed = new TimerTask() {
            public void run() {
                if (mPeripheral != null && mPeripheral.getConnectionState() == TIOConnection.STATE_CONNECTED) {
                    String aData = dataChunkQue.Get();
                    if (aData != null && aData.length() > 0) {
                        sendDataBLE(aData);
                    }
                }
            }
        };
        mTimerElapsed = new Timer();
        mTimerElapsed.schedule(taskElapsed, nBLESendInterval, nBLESendInterval);
    }

    public static void powerDown(final Context a_Ctx) {
        LOGe(TAG, "send power down!!!!!!!!!");

        goBackToMain(a_Ctx);

        PHCommon.sendData(sCmdPowerDown);

//        new Handler().postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                a_Ctx.sendBroadcast(new Intent(PHCommon.MSG_CMD_DISCONNECT));
//            }
//        }, 500);
    }

    public static void toggleLamp() {
        LOGe(TAG, "send toggle Lamp!!!!!!!!!");
        PHCommon.sendData(sCmdToggleLamp);
    }

    public static void sendData(String a_Data) {
        dataChunkQue.Put(a_Data);
    }

    public static void sendDataBLE(final String a_Data) {
        try {
            byte[] data = a_Data.getBytes("CP1252");
            LOG(TAG, "sendDataBLE: " + a_Data);
            LOG(TAG, PHLib.buildStringFromBytes(data, data.length));
            mConnection.transmit(data);
        } catch (Exception ex) {
            Log.e(TAG,ex.toString() );
        }
//        sendDataBLE(a_Data, 10);
    }

    private static void stopElapsedTimer() {
        if (mTimerElapsed != null) {
            mTimerElapsed.cancel();
            mTimerElapsed.purge();
            mTimerElapsed = null;
        }
    }

    public static String getUnit(Context a_Ctx) {
        SharedPreferences pref = a_Ctx.getSharedPreferences(a_Ctx.getString(R.string.project_name), Activity.MODE_PRIVATE);
        String aUnit = pref.getString("unit", "ml");
        return aUnit;
    }

    public static void setUnit(String a_Unit, Context a_Ctx) {
        SharedPreferences pref = a_Ctx.getSharedPreferences(a_Ctx.getString(R.string.project_name), Activity.MODE_PRIVATE);
        SharedPreferences.Editor ePref = pref.edit();
        ePref.putString("unit", a_Unit);
        ePref.commit();
    }


//    public static void sendDataBLE(final String a_Data, int a_Delay) {
//        LOG(TAG, "sendDataBLE: " + a_Data);
//
//        new Handler().postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                try {
//                    byte[] data = a_Data.getBytes("CP-1252");
//                    mConnection.transmit(data);
//                } catch (Exception ex) {
//                    Log.e(TAG,ex.toString() );
//                }
//            }
//        }, a_Delay);
//    }

    public static void doMenuAction(int a_SelIdx, Context a_Ctx) {

        if (a_SelIdx == 0) {
            goBackToMain(a_Ctx);

        } else if (a_SelIdx == 1) {
            createOperationActivity(a_Ctx);

        } else if (a_SelIdx == 2) {
            createProgramActivity(a_Ctx);

        } else if (a_SelIdx == 3) {
            createStatisticActivity(a_Ctx);

        }
    }

    public static void goBackToMain(Context a_Ctx) {
        Intent intent = new Intent(a_Ctx, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_SINGLE_TOP);
        Bundle bundle = new Bundle();
        intent.putExtras(bundle);
        a_Ctx.startActivity(intent);
        ((Activity)a_Ctx).overridePendingTransition(0, 0);
    }

    public static void createOperationActivity(Context a_Ctx) {
        Intent intent = new Intent(a_Ctx, Menu01Activity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_SINGLE_TOP);
        Bundle bundle = new Bundle();
        intent.putExtras(bundle);
        a_Ctx.startActivity(intent);
        ((Activity)a_Ctx).overridePendingTransition(0, 0);
    }

    public static void createProgramActivity(Context a_Ctx) {
        Intent intent = new Intent(a_Ctx, Menu02Activity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_SINGLE_TOP);
        Bundle bundle = new Bundle();
        intent.putExtras(bundle);
        a_Ctx.startActivity(intent);
        ((Activity)a_Ctx).overridePendingTransition(0, 0);
    }

    public static void createStatisticActivity(Context a_Ctx) {
        Intent intent = new Intent(a_Ctx, Menu03Activity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_SINGLE_TOP);
        Bundle bundle = new Bundle();
        intent.putExtras(bundle);
        a_Ctx.startActivity(intent);
        ((Activity)a_Ctx).overridePendingTransition(0, 0);
    }

    public static void sendSavePacket(Context a_Ctx) {
        String aCmd = "A";

        for (int i = 4; i < arrProgramData.length; i++) {
            ArrayList<ProgramData> aSeqData = arrProgramData[i];
            int aCnt = 0;
            for (int j = 0; j < aSeqData.size(); j++) {
                if (aSeqData.get(j).nMode == OP_MODE_NONE) {
                    break;
                }
                aCnt++;
            }

            if (i == 4) {
                nPrgReqCnt5 = aCnt;
            } else if (i == 5) {
                nPrgReqCnt6 = aCnt;
            } else if (i == 6) {
                nPrgReqCnt7 = aCnt;
            } else if (i == 7) {
                nPrgReqCnt8 = aCnt;
            }
//            aCmd += String.format("%d", aCnt);
        }

        aCmd += String.format("%d", nPrgReqCnt5);
        aCmd += String.format("%d", nPrgReqCnt6);
        aCmd += String.format("%d", nPrgReqCnt7);
        aCmd += String.format("%d", nPrgReqCnt8);

        if (nPrgOpType5 == OP_TYPE_NONE) {
            aCmd += String.format("%d", nOperationType);
        } else {
            aCmd += String.format("%d", nPrgOpType5);
        }

        if (nPrgOpType6 == OP_TYPE_NONE) {
            aCmd += String.format("%d", nOperationType);
        } else {
            aCmd += String.format("%d", nPrgOpType6);
        }

        if (nPrgOpType7 == OP_TYPE_NONE) {
            aCmd += String.format("%d", nOperationType);
        } else {
            aCmd += String.format("%d", nPrgOpType7);
        }

        if (nPrgOpType8 == OP_TYPE_NONE) {
            aCmd += String.format("%d", nOperationType);
        } else {
            aCmd += String.format("%d", nPrgOpType8);
        }

        LOGe(TAG, String.format("sendSavePacket nPrgReqCnt: %d %d %d %d", nPrgReqCnt5, nPrgReqCnt6, nPrgReqCnt7, nPrgReqCnt8));
        LOGe(TAG, String.format("sendSavePacket nPrgOpType: %d %d %d %d", nPrgOpType5, nPrgOpType6, nPrgOpType7, nPrgOpType8));

        LOGe(TAG, "sendSavePacket:" + aCmd + ";");
        sendData(aCmd + ";");

        SharedPreferences pref = a_Ctx.getSharedPreferences(a_Ctx.getString(R.string.project_name), Activity.MODE_PRIVATE);
        SharedPreferences.Editor ePref = pref.edit();
        ePref.putString("program_cnt_optype", aCmd);
        ePref.commit();
    }

    public static Resources getVCmdResource(Context a_Ctx, String a_Language) {
        Resources res = PHLib.getLocalizedResources(a_Ctx, Locale.KOREAN);
        if (a_Language.equals(VOICE_REC_LANG_KOREAN)) {
            res = PHLib.getLocalizedResources(a_Ctx, Locale.KOREAN);
        } else if (a_Language.equals(VOICE_REC_LANG_FRENCH)) {
            res = PHLib.getLocalizedResources(a_Ctx, Locale.FRENCH);
        } else {
            res = PHLib.getLocalizedResources(a_Ctx, Locale.ENGLISH);
        }

        return res;
    }

    public static void sendProgramDataAll(Context a_Ctx) {

        LOGe(TAG, "sendProgramDataAll-1");

//        sendData("A00000000;");

        sendSavePacket(a_Ctx);

        if (getProgramListCount(4) > 0) {
            saveProgramData(a_Ctx,4);
        }

        if (getProgramListCount(5) > 0) {
            saveProgramData(a_Ctx,5);
        }

        if (getProgramListCount(6) > 0) {
            saveProgramData(a_Ctx,6);
        }

        if (getProgramListCount(7) > 0) {
            saveProgramData(a_Ctx,7);
        }

    }

    public static byte[] hexStringToByteArray(String s) {
        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                    + Character.digit(s.charAt(i+1), 16));
        }
        return data;
    }

    private static int getProgramListCount(int a_TabIx) {
        ArrayList<ProgramData> aData = PHCommon.arrProgramData[a_TabIx];
        int aCnt = 0;
        for(int i = 0; i < aData.size(); i++) {
            if (aData.get(i).nMode != PHCommon.OP_MODE_NONE) {
                aCnt++;
            }
        }
        return aCnt;
    }

    private static void saveProgramData(Context a_Ctx, int a_TabIx) {

        ArrayList<ProgramData> aData = PHCommon.arrProgramData[a_TabIx];
        for(int i = 0; i < aData.size(); i++) {
            if (i == aData.size()-1) {
//                if (!Locale.getDefault().getLanguage().equals("fr") ||
//                        !(a_TabIx == 4 || a_TabIx == 5)
//                ) {
//                    aData.get(i).nLen = PHCommon.nProgramLastTime;
//                }
                sendProgramData(i, a_TabIx);
                break;

            } else if(i < aData.size()-1 && aData.get(i+1).nMode == PHCommon.OP_MODE_NONE) {
//                if (!Locale.getDefault().getLanguage().equals("fr") ||
//                        !(a_TabIx == 4 || a_TabIx == 5)
//                ) {
//                    aData.get(i).nLen = PHCommon.nProgramLastTime;
//                }
                sendProgramData(i, a_TabIx);
                break;

            } else {
                sendProgramData(i, a_TabIx);
            }
        }

        sendSavePacket(a_Ctx);
    }

    private static void sendProgramData(int a_idx, int a_TabIdx) {

        ProgramData aData = PHCommon.arrProgramData[a_TabIdx].get(a_idx);
        LOGe(TAG, "sendProgramData: " + aData.nCode + " " + aData.nMode + " " + aData.nPressure + " " + aData.nCycle + " " + aData.nLen);

        String aCmd = String.format("I%dS%dM%dP%02dC%dT%04d;",
                PHCommon.nCurSelPrgTabIdx,
                a_idx,
                aData.nMode,
                aData.nPressure,
                aData.nCycle,
                aData.nLen);

        LOGe(TAG, "sendProgramData:" + aCmd);
        PHCommon.sendData(aCmd);

    }



}
