package com.bistos.bt150.menu;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;

import com.google.android.material.tabs.TabLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.bistos.bt150.R;
import com.bistos.bt150.com.PHCommon;
import com.bistos.bt150.com.PHLib;
import com.bistos.bt150.com.ProgramData;
import com.bistos.bt150.com.TopMenu;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import static com.bistos.bt150.com.PHCommon.ACT_MENU02;
import static com.bistos.bt150.com.PHCommon.OP_TYPE_DOUBLE;
import static com.bistos.bt150.com.PHCommon.OP_TYPE_NONE;
import static com.bistos.bt150.com.PHCommon.OP_TYPE_SINGLE;
import static com.bistos.bt150.com.PHCommon.VOICE_MODE_PROGRAM;
import static com.bistos.bt150.com.PHCommon.nCurSelPrgTabIdx;
import static com.bistos.bt150.com.PHCommon.nOperationType;
import static com.bistos.bt150.com.PHCommon.nPrgReqCnt5;
import static com.bistos.bt150.com.PHCommon.nPrgReqCnt6;
import static com.bistos.bt150.com.PHCommon.nPrgReqCnt7;
import static com.bistos.bt150.com.PHCommon.nPrgReqCnt8;
import static com.bistos.bt150.com.PHCommon.nPrgOpType5;
import static com.bistos.bt150.com.PHCommon.nPrgOpType6;
import static com.bistos.bt150.com.PHCommon.nPrgOpType7;
import static com.bistos.bt150.com.PHCommon.nPrgOpType8;
import static com.bistos.bt150.com.PHCommon.nVoiceMode;
import static com.bistos.bt150.com.PHCommon.sendSavePacket;
import static com.bistos.bt150.com.PHLib.LOG;
import static com.bistos.bt150.com.PHLib.LOGe;
import static com.bistos.bt150.com.VoiceCommand.CMD_NUMBER_1;
import static com.bistos.bt150.com.VoiceCommand.CMD_NUMBER_2;
import static com.bistos.bt150.com.VoiceCommand.CMD_NUMBER_3;
import static com.bistos.bt150.com.VoiceCommand.CMD_NUMBER_4;
import static com.bistos.bt150.com.VoiceCommand.CMD_NUMBER_5;
import static com.bistos.bt150.com.VoiceCommand.CMD_NUMBER_6;
import static com.bistos.bt150.com.VoiceCommand.CMD_NUMBER_7;
import static com.bistos.bt150.com.VoiceCommand.CMD_NUMBER_8;
import static com.bistos.bt150.com.VoiceCommand.CMD_PLAY;
import static com.bistos.bt150.com.VoiceCommand.CMD_PROGRAMID_N;
import static com.bistos.bt150.com.VoiceCommand.CMD_SKIP;
import static com.bistos.bt150.com.VoiceCommand.CMD_STOP;
import static com.bistos.bt150.com.VoiceCommand.CMD_TYPE_NORMAL;
import static com.bistos.bt150.com.VoiceCommand.CMD_TYPE_STEP;

/**
 * Created by lamb on 2017. 10. 26..
 */

public class Menu02Activity extends AppCompatActivity {

    private static final String TAG = "Menu02Activity";

    private TopMenu topMenu;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private TabPagerAdapter pagerAdapter;

    private Button button_ActMenu02_Save;
    private Button button_ActMenu02_DelAll;
    private ImageButton imageButton_ActMenu02_ProgramPlay;
    private ImageButton imageButton_ActMenu02_ProgramStop;
    private ImageButton imageButton_ActMenu02_ProgramSkip;

    private ImageView imageView_ActMenu02_Single;
    private ImageView imageView_ActMenu02_Double;


    private boolean bRecvFirst = false;
//    private Menu02Fragment menu02Fragment;
    private ArrayList<Menu02Fragment> arrMenu02Fragment = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu02);

        topMenu = (TopMenu) findViewById(R.id.topMenu);
        topMenu.setOnTabSelectListener(new TopMenu.OnTabSelectListener() {
            @Override
            public void onSelect(int a_Idx) {
                PHCommon.doMenuAction(a_Idx, Menu02Activity.this);
                finish();
            }
        });

        button_ActMenu02_Save = (Button) findViewById(R.id.button_ActMenu02_Save);
        button_ActMenu02_DelAll = (Button) findViewById(R.id.button_ActMenu02_DelAll);

        imageButton_ActMenu02_ProgramPlay = (ImageButton) findViewById(R.id.imageButton_ActMenu02_ProgramPlay);
        imageButton_ActMenu02_ProgramStop = (ImageButton) findViewById(R.id.imageButton_ActMenu02_ProgramStop);
        imageButton_ActMenu02_ProgramSkip = (ImageButton) findViewById(R.id.imageButton_ActMenu02_ProgramSkip);

        imageView_ActMenu02_Single = (ImageView) findViewById(R.id.imageView_ActMenu02_Single);
        imageView_ActMenu02_Double = (ImageView) findViewById(R.id.imageView_ActMenu02_Double);


        button_ActMenu02_Save.setOnClickListener(onClickListener);
        button_ActMenu02_DelAll.setOnClickListener(onClickListener);
        imageButton_ActMenu02_ProgramPlay.setOnClickListener(onClickListener);
        imageButton_ActMenu02_ProgramStop.setOnClickListener(onClickListener);
        imageButton_ActMenu02_ProgramSkip.setOnClickListener(onClickListener);

        imageView_ActMenu02_Single.setOnClickListener(onClickListener);
        imageView_ActMenu02_Double.setOnClickListener(onClickListener);

        initMainLayout();

        registerReceiver(dataReceiver, new IntentFilter(PHCommon.MSG_DATA_RECV));
        registerReceiver(vcmdReceiver, new IntentFilter(PHCommon.MSG_VCMD_RECV));
        registerReceiver(singleDoubleReceiver, new IntentFilter(PHCommon.MSG_SINGLEDOUBLE_RECV));
    }

    @Override
    protected void onDestroy() {
        unregisterReceiver(dataReceiver);
        unregisterReceiver(vcmdReceiver);

        super.onDestroy();
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);

        if (hasFocus) {
            bRecvFirst = true;

            if (PHCommon.bProgramRunning) {
                parseRecvData(PHCommon.gRecvData);
            }
        }
    }

    public void selectOperationType(int a_Type) {

        imageView_ActMenu02_Single.setBackgroundResource(R.drawable.menu_ic_single);
        imageView_ActMenu02_Double.setBackgroundResource(R.drawable.menu_ic_double);

        int aChangableTabIdx = 4;
//        if (Locale.getDefault().getLanguage().equals("fr")) {
//            aChangableTabIdx = 6;
//        }

        if (a_Type == OP_TYPE_SINGLE) {
            if (nCurSelPrgTabIdx >= aChangableTabIdx) {
                imageView_ActMenu02_Single.setBackgroundResource(R.drawable.menu_ic_single_ov);
            } else {
                imageView_ActMenu02_Single.setBackgroundResource(R.drawable.menu_ic_single_ov_disable);
            }

        } else if (a_Type == OP_TYPE_DOUBLE) {
            imageView_ActMenu02_Single.setBackgroundResource(R.drawable.menu_ic_single);
            if (nCurSelPrgTabIdx >= aChangableTabIdx) {
                imageView_ActMenu02_Double.setBackgroundResource(R.drawable.menu_ic_double_ov);
            } else {
                imageView_ActMenu02_Double.setBackgroundResource(R.drawable.menu_ic_double_ov_disable);
            }
        } else {
            imageView_ActMenu02_Single.setBackgroundResource(R.drawable.menu_ic_single);
            imageView_ActMenu02_Double.setBackgroundResource(R.drawable.menu_ic_double);
        }
    }

    private int getCurListCount() {
        ArrayList<ProgramData> aData = PHCommon.arrProgramData[PHCommon.nCurSelPrgTabIdx];
        int aCnt = 0;
        for(int i = 0; i < aData.size(); i++) {
            if (aData.get(i).nMode != PHCommon.OP_MODE_NONE) {
                aCnt++;
            }
        }
        return aCnt;
    }

    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {

            if (view.getId() == R.id.button_ActMenu02_Save) {
                doProgramSave();

            } else if (view.getId() == R.id.button_ActMenu02_DelAll) {
                doProgramEraseAll();

            } else if (view.getId() == R.id.imageButton_ActMenu02_ProgramPlay) {
                doProgramPlay();

            } else if (view.getId() == R.id.imageButton_ActMenu02_ProgramStop) {
                doProgramStop();

            } else if (view.getId() == R.id.imageButton_ActMenu02_ProgramSkip) {
                doProgramSkip();
            } else if (view.getId() == R.id.imageView_ActMenu02_Single) {
                changeOperationType(OP_TYPE_SINGLE);
            } else if (view.getId() == R.id.imageView_ActMenu02_Double) {
                changeOperationType(OP_TYPE_DOUBLE);
            }
        }
    };

    private void changeOperationType(int a_OpType) {
        int aChangableTabIdx = 4;
//        if (Locale.getDefault().getLanguage().equals("fr")) {
//            aChangableTabIdx = 6;
//        }

        if (nCurSelPrgTabIdx >= aChangableTabIdx) {
            if (PHCommon.nCurSelPrgTabIdx == 4) {
                nPrgOpType5 = a_OpType;
            } else if (PHCommon.nCurSelPrgTabIdx == 5) {
                nPrgOpType6 = a_OpType;
            } else if (PHCommon.nCurSelPrgTabIdx == 6) {
                nPrgOpType7 = a_OpType;
            } else if (PHCommon.nCurSelPrgTabIdx == 7) {
                nPrgOpType8 = a_OpType;
            }
            selectOperationType(a_OpType);
            PHCommon.sendSavePacket(this);

            sendBroadcast(new Intent(PHCommon.MSG_PRGLIST_RELOAD));
        }
    }

    private void doProgramSave() {
        int aChangableTabIdx = 4;
//        if (Locale.getDefault().getLanguage().equals("fr")) {
//            aChangableTabIdx = 6;
//        }

        if(PHCommon.nCurSelPrgTabIdx >= aChangableTabIdx) {
            int aCnt = getCurListCount();
            if (aCnt > 0) {
                PHLib.showYesNoDialog(getString(R.string.WantSave), Menu02Activity.this, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        saveProgramData();
                    }
                });
            }

        }
    }

    private void doProgramPlay() {
        PHCommon.nElapsedTime = 0;
        PHCommon.sendData(PHCommon.cCmdProgramRun + String.format("%d", PHCommon.nCurSelPrgTabIdx) + ";");
    }

    private void doProgramStop() {
        LOGe(TAG, "Send Stop!!!!!!");

        PHCommon.sendData(PHCommon.sCmdProgramStop);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                PHCommon.sendData(PHCommon.sCmdInqueryStatus);
            }
        }, 100);
    }

    private void doProgramSkip() {
//        PHCommon.sendData(PHCommon.sCmdProgramSkip);

        if (PHCommon.bProgramRunning) {
            for(int i = 0; i < arrMenu02Fragment.size(); i++) {
                Menu02Fragment aFrag = arrMenu02Fragment.get(i);
                if (aFrag.nTabIdx == nCurSelPrgTabIdx) {
                    LOGe(TAG, "onDataReceived aFrag.nCurSelIdx:" + aFrag.nCurSelIdx + " nPrgReqCnt:" + aFrag.nPrgReqCnt);
                    if (aFrag.nCurSelIdx == aFrag.nPrgReqCnt-1) {
                        doProgramStop();
                    } else {
                        PHCommon.sendData(PHCommon.sCmdProgramSkip);
                    }
                    break;
                }
//                LOGe(TAG, "i:" + i + " nTabIdx:" + aFrag.nTabIdx + " nCurSelIdx:" + aFrag.nCurSelIdx);
            }
        }
    }

    private void doProgramEraseAll() {
        int aChangableTabIdx = 4;
//        if (Locale.getDefault().getLanguage().equals("fr")) {
//            aChangableTabIdx = 6;
//        }

        if(PHCommon.nCurSelPrgTabIdx >= aChangableTabIdx) {
            int aCnt = getCurListCount();
            if (aCnt > 0) {
                PHLib.showYesNoDialog(getString(R.string.deleteAllSeq), Menu02Activity.this, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        clearProgramSequece();

                        refreshSaveDeleteAllButton();
                    }
                });
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        PHCommon.nActivityNo = ACT_MENU02;

        nVoiceMode = VOICE_MODE_PROGRAM;

        topMenu.selectMenu(2);

        setPageSelect(PHCommon.nCurSelPrgTabIdx);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                PHCommon.sendData(PHCommon.sCmdInqueryStatus);
            }
        }, 100);

        refreshCurrentOpType();
    }

    private  void clearProgramSequece() {

        ArrayList<ProgramData> aData = PHCommon.arrProgramData[PHCommon.nCurSelPrgTabIdx];
        for(int i = 0; i < aData.size(); i++) {
            aData.get(i).initProgramData();
        }

        switch (PHCommon.nCurSelPrgTabIdx) {
            case 4:
                nPrgReqCnt5 = 0;
                nPrgOpType5 = OP_TYPE_DOUBLE;
                break;
            case 5:
                nPrgReqCnt6 = 0;
                nPrgOpType6 = OP_TYPE_DOUBLE;
                break;
            case 6:
                nPrgReqCnt7 = 0;
                nPrgOpType7 = OP_TYPE_NONE;
                break;
            case 7:
                nPrgReqCnt8 = 0;
                nPrgOpType8 = OP_TYPE_NONE;
                break;
            default:
                break;
        }

        SharedPreferences pref = getSharedPreferences(getString(R.string.project_name), Activity.MODE_PRIVATE);
        SharedPreferences.Editor ePref = pref.edit();
        for (int idx = 0; idx < 8; idx++) {
            ePref.remove(String.format("programdata_%d_%d", PHCommon.nCurSelPrgTabIdx, idx));
        }
        ePref.commit();

        sendBroadcast(new Intent(PHCommon.MSG_PRGLIST_RELOAD));

        sendSavePacket(this);

        refreshCurrentOpType();
    }

    public void saveProgramData() {

        ArrayList<ProgramData> aData = PHCommon.arrProgramData[PHCommon.nCurSelPrgTabIdx];
        for(int i = 0; i < aData.size(); i++) {
            if (i == aData.size()-1) {
//                if (!Locale.getDefault().getLanguage().equals("fr") ||
//                        !(PHCommon.nCurSelPrgTabIdx == 4 || PHCommon.nCurSelPrgTabIdx == 5)
//                ) {
//                    aData.get(i).nLen = PHCommon.nProgramLastTime;
//                }
                setProgramData(i);
                break;

            } else if(i < aData.size()-1 && aData.get(i+1).nMode == PHCommon.OP_MODE_NONE) {
//                if (!Locale.getDefault().getLanguage().equals("fr") ||
//                        !(PHCommon.nCurSelPrgTabIdx == 4 || PHCommon.nCurSelPrgTabIdx == 5)
//                ) {
//                    aData.get(i).nLen = PHCommon.nProgramLastTime;
//                }
                setProgramData(i);
                break;

            } else {
                setProgramData(i);
            }
        }

        sendSavePacket(this);

        refreshCurrentOpType();

        sendBroadcast(new Intent(PHCommon.MSG_PRGLIST_RELOAD));

//        PHCommon.printProgramTable(this);
    }

//    private void sendSavePacket() {
//        String aCmd = "A";
//        for (int i = 0; i < PHCommon.arrProgramData.length; i++) {
//            ArrayList<ProgramData> aSeqData = PHCommon.arrProgramData[i];
//            int aCnt = 0;
//            for (int j = 0; j < aSeqData.size(); j++) {
//                if (aSeqData.get(j).nMode == PHCommon.OP_MODE_NONE) {
//                    break;
//                }
//                aCnt++;
//            }
//            aCmd += String.format("%d", aCnt);
//        }
//        LOGe(TAG, "sendSavePacket:" + aCmd + ";");
//        PHCommon.sendData(aCmd + ";");
//    }

    private void setProgramData(int a_Idx) {

        ProgramData aData = PHCommon.arrProgramData[PHCommon.nCurSelPrgTabIdx].get(a_Idx);
//        LOGe(TAG, "setProgramData: " + aData.nCode + " " + aData.nMode + " " + aData.nPressure + " " + aData.nCycle + " " + aData.nLen);

        String aCmd = String.format("I%dS%dM%dP%02dC%dT%04d;",
                PHCommon.nCurSelPrgTabIdx,
                a_Idx,
                aData.nMode,
                aData.nPressure,
                aData.nCycle,
                aData.nLen);

        LOGe(TAG, "setProgramData:" + aCmd);
        PHCommon.sendData(aCmd);

        String aPrefSaveKey = String.format("programdata_%d_%d", PHCommon.nCurSelPrgTabIdx, a_Idx);

        SharedPreferences pref = getSharedPreferences(getString(R.string.project_name), Activity.MODE_PRIVATE);
        SharedPreferences.Editor ePref = pref.edit();
        ePref.putString(aPrefSaveKey, aCmd);
        ePref.commit();


        LOGe(TAG, "setProgramData :" + aCmd + " save:" + aPrefSaveKey);

        String aRead = pref.getString(aPrefSaveKey, "");
        LOGe(TAG, "setProgramData read:" + aPrefSaveKey + ": " + aRead);


//        DBHelper dbHelper = new DBHelper(this);	//DB 헬퍼 할당.
//        dbHelper.setProgramData(a_Data.nCode, a_Data.nMode, a_Data.nPressure, a_Data.nCycle, a_Data.nLen);
//        dbHelper.close();
    }

    public void setPageSelect(int a_Idx) {
        viewPager.setCurrentItem(a_Idx, true);
    }

    private void initMainLayout() {

        // Initializing the TabLayout
        tabLayout = (TabLayout) findViewById(R.id.tabLayout);
        for (int i = 0; i < 8; i++) {
            tabLayout.addTab(tabLayout.newTab().setText(String.format("%02d", i+1)));
        }
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        // Initializing ViewPager
        viewPager = (ViewPager) findViewById(R.id.pager);

        // Creating TabPagerAdapter adapter
        pagerAdapter = new TabPagerAdapter(getSupportFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(pagerAdapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));

        // Set TabSelectedListener
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

                if (PHCommon.bProgramRunning) {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            tabLayout.getTabAt(PHCommon.nCurSelPrgTabIdx).select();
                            setPageSelect(PHCommon.nCurSelPrgTabIdx);
                        }
                    }, 100);
                    return;
                }

                PHCommon.nCurSelPrgTabIdx = tab.getPosition();

                LOGe(TAG, "nPrgOpType5:" + nPrgOpType5 + " nPrgOpType6:" + nPrgOpType6 + " nPrgOpType7:" + nPrgOpType7 + " nPrgOpType8:" + nPrgOpType8);

                refreshCurrentOpType();

                PHCommon.sendData(String.format("Y%d;", PHCommon.nCurSelPrgTabIdx));

                refreshSaveDeleteAllButton();

                viewPager.setCurrentItem(tab.getPosition(), true);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    public void refreshSaveDeleteAllButton() {
        int aCnt = getCurListCount();

        int aChangableTabIdx = 4;
//        if (Locale.getDefault().getLanguage().equals("fr")) {
//            aChangableTabIdx = 6;
//        }

        if (PHCommon.nCurSelPrgTabIdx < aChangableTabIdx || aCnt == 0) {
            button_ActMenu02_Save.setBackgroundColor(getResources().getColor(R.color.menulinegraylight));
            button_ActMenu02_DelAll.setBackgroundColor(getResources().getColor(R.color.menulinegraylight));
        } else {
            button_ActMenu02_Save.setBackgroundColor(getResources().getColor(R.color.colorAccent));
            button_ActMenu02_DelAll.setBackgroundColor(getResources().getColor(R.color.colorAccent));
        }
    }

    public class TabPagerAdapter extends FragmentStatePagerAdapter {

        // Count number of tabs
        private int tabCount;

        public TabPagerAdapter(FragmentManager fm, int tabCount) {
            super(fm);
            this.tabCount = tabCount;
        }

        @Override
        public int getCount() {
            return tabCount;
        }

        @Override
        public Fragment getItem(int position) {
            Menu02Fragment menu02Fragment = new Menu02Fragment();
            Bundle args = new Bundle();
            args.putInt("position", position);
            menu02Fragment.setArguments(args);
//            menu02Fragment.setReceiveDataHandlerFragment(handlerFragmentData);
            arrMenu02Fragment.add(menu02Fragment);
            return menu02Fragment;
        }
    }

//    public Handler handlerFragmentData = new Handler() {
//        @Override
//        public void handleMessage(Message msg) {
//            String aData = (String)msg.obj;
//
//            switch (msg.arg1) {
//                case 0:
//                    break;
//
//                default:
//                    break;
//            }
//
//
//        }
//    };

    private void parseRecvData(String a_Recv) {

//        LOGe(TAG, "parseRecvData:" + a_Recv);
        if (a_Recv.length() >= 14
                && a_Recv.substring(0, 1).equals("M")
                && a_Recv.substring(2, 3).equals("P")
                && a_Recv.substring(5, 6).equals("C")
                && a_Recv.substring(7, 8).equals("R")) {

            String aRun = a_Recv.substring(8, 9);
            int aNo = PHLib.convertToInt(a_Recv.substring(10, 11));
            int aSeq = PHLib.convertToInt(a_Recv.substring(12, 13));
            LOGe(TAG, "Activity parseRecvData: aRun:" + aRun + " aNo:" + aNo + " aSeq:" + aSeq + " PHCommon.nCurSelPrgTabIdx:" + PHCommon.nCurSelPrgTabIdx);

            if (aRun.equals("2")) {
                PHCommon.bProgramRunning = true;
            } else {
                PHCommon.bProgramRunning = false;
            }

            if (PHCommon.nCurSelPrgTabIdx != aNo) {
                LOGe(TAG, "Activity parseRecvData: setPageSelect:aNo:" + aNo);
                PHCommon.nCurSelPrgTabIdx = aNo;
                setPageSelect(aNo);
            }

            if (a_Recv.length() > 15 && a_Recv.substring(13, 14).equals("W")) {
                String aOpType = a_Recv.substring(14, 15);
                if (aOpType.equals("0")) {
                    nOperationType = OP_TYPE_SINGLE;
                } else {
                    nOperationType = OP_TYPE_DOUBLE;
                }
            }

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    refreshCurrentOpType();
                }
            });
        }
    }

    private void refreshCurrentOpType() {
        LOGe(TAG, "refreshCurrentOpType PHCommon.nCurSelPrgTabIdx:" + PHCommon.nCurSelPrgTabIdx);

        if (PHCommon.nCurSelPrgTabIdx == 0) {
            selectOperationType(OP_TYPE_DOUBLE);

        } else if (PHCommon.nCurSelPrgTabIdx == 1) {
            selectOperationType(OP_TYPE_DOUBLE);

        } else if (PHCommon.nCurSelPrgTabIdx == 2) {
            selectOperationType(OP_TYPE_SINGLE);

        } else if (PHCommon.nCurSelPrgTabIdx == 3) {
            selectOperationType(OP_TYPE_SINGLE);

        } else if (PHCommon.nCurSelPrgTabIdx == 4) {
            if (nPrgOpType5 == OP_TYPE_NONE) {
                selectOperationType(nOperationType);
            } else {
                selectOperationType(nPrgOpType5);
            }

        } else if (PHCommon.nCurSelPrgTabIdx == 5) {
            if (nPrgOpType6 == OP_TYPE_NONE) {
                selectOperationType(nOperationType);
            } else {
                selectOperationType(nPrgOpType6);
            }

        } else if (PHCommon.nCurSelPrgTabIdx == 6) {
            if (nPrgOpType7 == OP_TYPE_NONE) {
                selectOperationType(nOperationType);
            } else {
                selectOperationType(nPrgOpType7);
            }

        } else if (PHCommon.nCurSelPrgTabIdx == 7) {
            if (nPrgOpType8 == OP_TYPE_NONE) {
                selectOperationType(nOperationType);
            } else {
                selectOperationType(nPrgOpType8);
            }
        }
    }

    private final BroadcastReceiver singleDoubleReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent a_intent) {
            Bundle bundle = a_intent.getExtras();
            String aData = bundle.getString("singledouble_data");
            if (aData != null) {
                if (aData.equals("single")) {
                    changeOperationType(OP_TYPE_SINGLE);
                } else if (aData.equals("double")) {
                    changeOperationType(OP_TYPE_DOUBLE);
                }
            }
        }
    };

    private final BroadcastReceiver dataReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent a_intent) {
            Bundle bundle = a_intent.getExtras();
            String aData = bundle.getString("recv_data");
            if (aData != null) {
                parseRecvData(PHCommon.gRecvData);
            }
        }
    };

    private final BroadcastReceiver vcmdReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent a_intent) {

            Bundle bundle = a_intent.getExtras();
            int aCmdCode = bundle.getInt("CmdCode");
            int aCmdType = bundle.getInt("CmdType");
            int aStepVal = bundle.getInt("StepVal");
            LOGe(TAG, "onReceive: aCmdCode:" + aCmdCode + " aCmdType:" + aCmdType + " aStepVal:" + aStepVal);

            if (aCmdType == CMD_TYPE_NORMAL) {
                switch (aCmdCode) {
//                    case CMD_PROGRAMID_UP:
//                    case CMD_ID_UP:
//                        doProgramIDUp();
//                        break;
//                    case CMD_PROGRAMID_DOWN:
//                    case CMD_ID_DOWN:
//                        doProgramIDDown();
//                        break;
//                    case CMD_PROGRAM_PLAY:
                    case CMD_PLAY:
                        doProgramPlay();
                        break;
//                    case CMD_PROGRAM_STOP:
                    case CMD_STOP:
                        doProgramStop();
                        break;
//                    case CMD_PROGRAM_SKIP:
                    case CMD_SKIP:
                        doProgramSkip();
                        break;
//                    case CMD_PROGRAM_ERASE:
//                    case CMD_ERASE:
//                        doProgramEraseAll();
//                        break;
                    case CMD_NUMBER_1:
                        doProgramIDN(1);
                        break;
                    case CMD_NUMBER_2:
                        doProgramIDN(2);
                        break;
                    case CMD_NUMBER_3:
                        doProgramIDN(3);
                        break;
                    case CMD_NUMBER_4:
                        doProgramIDN(4);
                        break;
                    case CMD_NUMBER_5:
                        doProgramIDN(5);
                        break;
                    case CMD_NUMBER_6:
                        doProgramIDN(6);
                        break;
                    case CMD_NUMBER_7:
                        doProgramIDN(7);
                        break;
                    case CMD_NUMBER_8:
                        doProgramIDN(8);
                        break;
                    default:
                        break;
                }

            } else if (aCmdType == CMD_TYPE_STEP) {
                switch (aCmdCode) {
                    case CMD_PROGRAMID_N:
//                    case CMD_ID_N:
                        doProgramIDN(aStepVal);
                        break;
                    default:
                        break;
                }
            }
        }
    };

    private void doProgramIDUp() {
        int aIdx = PHCommon.nCurSelPrgTabIdx + 1;
        setProgramID(aIdx);
    }

    private void doProgramIDDown() {
        int aIdx = PHCommon.nCurSelPrgTabIdx - 1;
        setProgramID(aIdx);
    }

    private void setProgramID(int a_Idx) {
        int aIdx = a_Idx;
        if (aIdx > 7) {
            aIdx = 7;
        } else if (aIdx < 0) {
            aIdx = 0;
        }
        PHCommon.nCurSelPrgTabIdx = aIdx;
        PHCommon.sendData(String.format("Y%d;", PHCommon.nCurSelPrgTabIdx));

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                setPageSelect(PHCommon.nCurSelPrgTabIdx);
            }
        });
    }

    private static Timer timerInitActivityView;
    private static int nTimerInitActivityViewMax = 30;

    private static void stopInitActivityViewTimer() {
        nTimerInitActivityViewMax = 30;

        if (timerInitActivityView != null) {
            timerInitActivityView.cancel();
            timerInitActivityView.purge();
            timerInitActivityView = null;
        }
    }

    private void doProgramIDN(final int a_ID) {
        stopInitActivityViewTimer();

        TimerTask timerTaskInitDataReceived = new TimerTask() {
            @Override
            public void run() {

                LOGe(TAG, "timerTaskInitDataReceived:" + bRecvFirst +  " nTimerInitActivityViewMax:" + nTimerInitActivityViewMax);

                if(bRecvFirst) {
                    stopInitActivityViewTimer();
                    setProgramID(a_ID-1);
                }

                if (nTimerInitActivityViewMax < 0) {
                    stopInitActivityViewTimer();
                }

                nTimerInitActivityViewMax--;
            }
        };

        timerInitActivityView = new Timer();
        timerInitActivityView.schedule(timerTaskInitDataReceived, 500, 500);
    }
}