package com.bistos.bt150.com;

import android.util.Log;

import java.util.ArrayList;

import static com.bistos.bt150.com.PHLib.LOGe;

/**
 * Created by Lamb on 10/10/2018.
 * Philosys
 * philosys_dev@gmail.com
 */
public class VoiceCommand {

    private static final String TAG = "VoiceCommand";


    public final static int CMD_TYPE_NORMAL         = 100;
    public final static int CMD_TYPE_STEP           = 200;

    public final static int CMD_NONE            = 0;

    public final static int CMD_MODE_CHANGE     = 1001;
//    public final static int CMD_MODE_EXPRESS    = 1002;
//    public final static int CMD_MODE_MASSAGE    = 1004;

    public final static int CMD_PRESSURE_UP     = 1006;
    public final static int CMD_PRESSURE_DOWN   = 1007;
//    public final static int CMD_PRESSURE_N      = 1008;

    public final static int CMD_CYCLE_UP        = 1009;
    public final static int CMD_CYCLE_DOWN      = 1010;
//    public final static int CMD_CYCLE_N         = 1011;

//    public final static int CMD_PROGRAMID_UP    = 1012;
//    public final static int CMD_ID_UP           = 1013;
//    public final static int CMD_PROGRAMID_DOWN  = 1014;
//    public final static int CMD_ID_DOWN         = 1015;
    public final static int CMD_PROGRAMID_N     = 1016;
//    public final static int CMD_ID_N            = 1017;

//    public final static int CMD_PROGRAM_PLAY    = 1018;
    public final static int CMD_PLAY            = 1019;
//    public final static int CMD_PROGRAM_STOP    = 1020;
    public final static int CMD_STOP            = 1021;
//    public final static int CMD_PROGRAM_SKIP    = 1022;
    public final static int CMD_SKIP            = 1023;

//    public final static int CMD_PROGRAM_ERASE   = 1024;
//    public final static int CMD_ERASE           = 1025;
//
//    public final static int CMD_PROGRAM_START   = 1026;
//    public final static int CMD_START           = 1027;
//    public final static int CMD_PROGRAM_END     = 1028;
//    public final static int CMD_END             = 1029;
//    public final static int CMD_PRESSURE_SAVE   = 1030;
//    public final static int CMD_CYCLE_SAVE      = 1031;
//    public final static int CMD_SECOND_UP       = 1032;
//    public final static int CMD_SECOND_DOWN     = 1033;
//    public final static int CMD_MINUTE_UP       = 1034;
//    public final static int CMD_MINUTE_DOWN     = 1035;
//    public final static int CMD_NEXT_SEQ        = 1036;
//    public final static int CMD_NEXT            = 1037;

    public final static int CMD_POWER           = 1038;
    public final static int CMD_LAMP            = 1039;

    public final static int CMD_NUMBER_1        = 1040;
    public final static int CMD_NUMBER_2        = 1041;
    public final static int CMD_NUMBER_3        = 1042;
    public final static int CMD_NUMBER_4        = 1043;
    public final static int CMD_NUMBER_5        = 1044;
    public final static int CMD_NUMBER_6        = 1045;
    public final static int CMD_NUMBER_7        = 1046;
    public final static int CMD_NUMBER_8        = 1047;


    public int nCmdCode = 0;
    public int nCmdType = CMD_TYPE_NORMAL;
    public int nStepVal = 0;
    public ArrayList <String> arrCmdPhrase = new ArrayList<String>();

    public VoiceCommand(int a_CmdCode, String a_Phrase){
        nCmdCode = a_CmdCode;
        addPhrase(a_Phrase);
    }

    public VoiceCommand(int a_CmdCode, String a_Phrase, int a_Type){
        nCmdCode = a_CmdCode;
        nCmdType = a_Type;
        addPhrase(a_Phrase);
    }

    public void addPhrase(String a_Phrase) {
        for (String aPhrase : arrCmdPhrase) {
            if (aPhrase.equals(a_Phrase)) {
                return;
            }
        }
        arrCmdPhrase.add(a_Phrase);
    }

    public void printPhrase() {
        for (String aPhrase : arrCmdPhrase) {
            Log.e(TAG, nCmdCode + " " + aPhrase);
        }
    }

    public boolean isMatch(String a_Phrase) {
        for (String aCmd : arrCmdPhrase) {
            if (aCmd.equalsIgnoreCase(a_Phrase)) {
                return true;
            }
        }
        return false;
    }

    public boolean isContains(String a_Phrase) {
        for (String aCmd : arrCmdPhrase) {
            String aChunk = aCmd.replace(" ", "");
            if (aChunk.toLowerCase().contains(a_Phrase.toLowerCase())) {
                return true;
            }
        }
        return false;
    }

    public boolean checkCommand(String a_Phrase) {
        for (String aCmd : arrCmdPhrase) {
            String aChunk = aCmd.replace(" ", "");
            if (a_Phrase.toLowerCase().contains(aChunk.toLowerCase())) {
//            int aIdx = a_Phrase.toLowerCase().indexOf(aChunk.toLowerCase());
//            if ( aIdx != -1 ) {
                if (a_Phrase.toLowerCase().equals("slow") && aChunk.toLowerCase().equals("low")) {
                    return false;
                }
                LOGe(TAG, "checkCommand: a_Phrase:" + a_Phrase);
                return true;
            }
        }
        return false;
    }


    public void initCmdPhrase() {
        arrCmdPhrase.clear();
    }

    public int getCmdPhraseCount() {
        return arrCmdPhrase.size();
    }

}

