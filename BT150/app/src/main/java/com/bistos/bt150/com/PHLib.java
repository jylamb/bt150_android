package com.bistos.bt150.com;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PermissionGroupInfo;
import android.content.pm.PermissionInfo;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import androidx.annotation.NonNull;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.inputmethod.InputMethodManager;

import com.bistos.bt150.R;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import static android.content.Context.TELEPHONY_SERVICE;

/**
 * Created by kobonghwan on 2016. 11. 4..
 */

public class PHLib {

    private static final String TAG = "PHLib";

//    public static final boolean IS_JBMR2 = Build.VERSION.SDK_INT == Build.VERSION_CODES.JELLY_BEAN_MR2;
//    public static final boolean IS_ISC = Build.VERSION.SDK_INT == Build.VERSION_CODES.ICE_CREAM_SANDWICH;
//    public static final boolean IS_GINGERBREAD_MR1 = Build.VERSION.SDK_INT == Build.VERSION_CODES.GINGERBREAD_MR1;

    public static String szUUID = "";
    public static String szPhoneNo = "";

    static int IMAGE_MAX_SIZE = 1024;

    public static void LOGe(String a_Log) {
        LOG(a_Log, TAG, "e");
    }

    public static void LOG(String a_Log) {
        LOG(a_Log, TAG, "w");
    }

    public static void LOGe(String a_Tag, String a_Log) {
        LOG(a_Tag, a_Log, "e");
    }

    public static void LOG(String a_Tag, String a_Log) {
        LOG(a_Tag, a_Log, "w");
    }

    public static void LOG(String a_Tag, String a_Log, String a_Level) {
        if (a_Level.equals("v")) {
            Log.v(a_Tag, a_Log);

        } else if (a_Level.equals("d")) {
            Log.d(a_Tag, a_Log);

        } else if (a_Level.equals("i")) {
            Log.i(a_Tag, a_Log);

        } else if (a_Level.equals("w")) {
            Log.w(a_Tag, a_Log);

        } else if (a_Level.equals("e")) {
            Log.e(a_Tag, a_Log);
        }
    }

    public static void Sleep(int a_MilliSec) {
        try {
            Thread.sleep(a_MilliSec);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void DirectoryCreate(String directoryName) throws IOException {
        String state = Environment.getExternalStorageState();
        if(!state.equals(Environment.MEDIA_MOUNTED))  {
            throw new IOException("SD Card is not mounted.  It is " + state + ".");
        }
        File dir = new File(Environment.getExternalStorageDirectory().getAbsolutePath(), directoryName) ;
        if (!dir.isDirectory()) {
            if (!dir.mkdirs()) {
                throw new IOException("Path to file could not be created.");
            }
        }
    }

    public static void showMessageDialog(String message, Context dContext) {
        showMessageDialog(message, dContext.getString(R.string.app_name), dContext);
    }

    public static void showMessageDialog(String message, String dTitle, Context dContext) {

        if (dContext != null) {
            new AlertDialog.Builder(dContext)
                    .setTitle(dTitle)
                    .setMessage(message)
                    .setPositiveButton(dContext.getString(R.string.OK), null)
                    .show();
        }
    }

    public static AlertDialog showYesNoDialog(String message, Context dContext, final DialogInterface.OnClickListener a_Listener) {
        return showYesNoDialog(message, dContext.getString(R.string.app_name), dContext, a_Listener);
    }

    public static AlertDialog showYesNoDialog(String message, String dTitle, Context dContext, final DialogInterface.OnClickListener a_Listener) {
        return showYesNoDialog(message, dTitle, dContext.getResources().getString(R.string.YES), dContext.getResources().getString(R.string.NO), dContext, a_Listener);
    }

    public static AlertDialog showYesNoDialog(String message, String dTitle, String a_Yes, String a_No, Context dContext, final DialogInterface.OnClickListener a_Listener) {
        return showYesNoDialog(message, dTitle, a_Yes, a_No, dContext, a_Listener, null);
    }

    public static AlertDialog showYesNoDialog(String message, String dTitle, String a_Yes, String a_No, Context dContext, final DialogInterface.OnClickListener a_ListenerYes, final DialogInterface.OnClickListener a_ListenerNo) {

        AlertDialog dlg = new AlertDialog.Builder(dContext)
                .setTitle(dTitle)
                .setMessage(message)
                .setPositiveButton(a_Yes, a_ListenerYes)
                .setNegativeButton(a_No, a_ListenerNo)
                .show();
        return dlg;
    }

    public static AlertDialog showConfirmDialog(String message, Context dContext, final DialogInterface.OnClickListener a_Listener) {
        return showConfirmDialog(message, dContext.getString(R.string.app_name), dContext, a_Listener);
    }

    public static AlertDialog showConfirmDialog(String message, String dTitle, Context dContext, final DialogInterface.OnClickListener a_Listener) {

        AlertDialog dlg = new AlertDialog.Builder(dContext)
                .setTitle(dTitle)
                .setMessage(message)
                .setPositiveButton(dContext.getResources().getString(R.string.YES), a_Listener)
                .show();
        return dlg;
    }

    public static void onPopSelect(String a_Title, String items[], Context a_Ctx, DialogInterface.OnClickListener a_Listener) {

        AlertDialog.Builder ab = new AlertDialog.Builder(a_Ctx);
        if (a_Title != null && a_Title.length() > 0) {
            ab.setTitle(a_Title);
        }
        ab.setSingleChoiceItems(items, -1, a_Listener);
        ab.setNegativeButton(a_Ctx.getResources().getString(R.string.CANCEL), null);
        ab.show();
    }

    private static final float DEFAULT_HDIP_DENSITY_SCALE = 1.5f;

//	public static int DPFromPixel(int pixel, Context mContext) {
//		float scale = mContext.getResources().getDisplayMetrics().density;
//		Integer dpValue = 0;
//
//		if (scale == 1.0) {
//			dpValue = pixel;
//		} else {
//			dpValue = (int)(pixel / DEFAULT_HDIP_DENSITY_SCALE * scale);
//		}
//		return dpValue;
//	}
//
//	public static int PixelFromDP(int DP, Context mContext) {
//		float scale = mContext.getResources().getDisplayMetrics().density;
//		Integer pixelValue = 0;
//
//		if (scale == 1.0) {
//			pixelValue = (int)(DP / scale * DEFAULT_HDIP_DENSITY_SCALE);
//		} else {
//			pixelValue = DP;
//		}
//		return pixelValue;
//	}

    public static String getNullCheck(String value) {
        if (value == null) {
            return "";
        }
        if (value.equals("null")) {
            return "";
        }
        return value;
    }

    public static void DeleteFolderFile(String folderPath, Boolean deleteFolder) {
        File targetFile = null;
        File folder = new File(folderPath);
        String[] list = null;
        if (folder.isDirectory()) {
            list = folder.list();
            for (int i=0; i<list.length; i++) {
                targetFile = new File(folder.toString() + "/" + list[i]);
                targetFile.delete();
            }
        }
        if (folder.exists() && deleteFolder) {
            folder.delete();
        }
    }

//	public static void changeDensity(Context dContext) {
//		//desiredDensity : ldpi = 0.75 (120dpi) , mdpi = 1 (160dpi), hdpi = 1.5 (240dpi), xhdpi = 2.0 (320dpi)
//		DisplayMetrics metrics = dContext.getResources().getDisplayMetrics();
//
//		if (metrics.density == 1 && metrics.densityDpi == 160) {
//			int value1 = metrics.widthPixels;
//			int value2 = metrics.heightPixels;
//			if ((value1 < value2 && value1 >= 600) || (value1 > value2 && value2 >= 600)) {
//				float desiredDensity = (float) 1.5;
//
//				metrics.density = desiredDensity;
//				metrics.xdpi = desiredDensity * 160;
//				metrics.ydpi = desiredDensity * 160;
//				metrics.densityDpi = (int) (desiredDensity * 160);
//
//				dContext.getResources().updateConfiguration(null, null);
//
//				MPlusApplication.isChangedDensity = true;
//			}
//		}
//	}

    public static String getDeviceName() {
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        if (model.startsWith(manufacturer)) {
            return model;
        } else {
            return manufacturer + " " + model;
        }
    }

    public static String AddCommaDeli(String a_Val) {
        if(a_Val == null) {
            return "0";
        }
        if (a_Val.equals("null")) {
            return "0";
        }
        String a_Remove = a_Val.replaceAll(",", "");
        if(!isNumeric(a_Remove)) {
            return a_Val;
        }
        return AddCommaDeli(Long.parseLong(a_Remove));
    }

    public static String AddCommaDeli(long a_Val) {
        DecimalFormat df = new DecimalFormat("###,###");
        String result = df.format(a_Val);
        return result;
    }


//	public static String makePhoneString(String a_Val, String a_Deli) {
//		if(a_Val == null) {
//			return a_Val;
//		}
//
//		String result = "";
//		if(a_Val.length() == 6) {
//
//		} else if(a_Val.length() == 6) {
//
//		}
//		return result;
//	}

    public static String makeDateString(String a_Val) {
        return makeDateString(a_Val, "-");
    }

    public static String makeDateString(String a_Val, String a_Deli) {
        if(a_Val == null) {
            return a_Val;
        }

        String result = "";
        if(a_Val.length() == 6) {
            result = a_Val.substring(0, 4) + a_Deli + a_Val.substring(4, 6);
        } else if(a_Val.length() == 8) {
            result = a_Val.substring(0, 4) + a_Deli + a_Val.substring(4, 6) + a_Deli + a_Val.substring(6, 8);
        } else {
            result = a_Val;
        }
        return result;
    }

    public static float convertPixelsToDp(float px, Context a_Ctx){
        Resources resources = a_Ctx.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float dp = px / ((float)metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
        return dp;
    }

    public static float convertDpToPixel(float dp, Context a_Ctx){
        Resources resources = a_Ctx.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float px = dp * ((float)metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
        return px;
    }

    public static int dpToPixel(int dp, Context a_Ctx) {
        Float px = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, a_Ctx.getResources().getDisplayMetrics());
        return px.intValue();
    }

    public static int convertToInt(String a_Str) {
        String aStr = getNullCheck(a_Str);
        if(aStr.trim().length() == 0) {
            return 0;
        }
        return Integer.parseInt(aStr);
    }

    public static double convertToDouble(String a_Str) {
        String aStr = getNullCheck(a_Str);
        if(aStr.trim().length() == 0) {
            return 0;
        }
        return Double.parseDouble(aStr);
    }

    public static boolean isNumeric(String str) {
        if(str == null || str.length() == 0) {
            return false;
        }
        for (char c : str.toCharArray()) {
            if (!Character.isDigit(c)) return false;
        }
        return true;
    }

    public static Bitmap getResizedBitmap(Bitmap bm, int newHeight, int newWidth) {
        int width = bm.getWidth();
        int height = bm.getHeight();
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        // CREATE A MATRIX FOR THE MANIPULATION
        Matrix matrix = new Matrix();
        // RESIZE THE BIT MAP
        matrix.postScale(scaleWidth, scaleHeight);

        // "RECREATE" THE NEW BITMAP
        Bitmap resizedBitmap = Bitmap.createBitmap(bm, 0, 0, width, height, matrix, false);
        return resizedBitmap;
    }

    public static Bitmap decodeFile(File a_File){
        Bitmap b = null;
        try {
            //Decode image size
            BitmapFactory.Options bitOption = new BitmapFactory.Options();
            bitOption.inJustDecodeBounds = true;

            FileInputStream fis = new FileInputStream(a_File);
            BitmapFactory.decodeStream(fis, null, bitOption);
            fis.close();

            int scale = 1;
            if (bitOption.outHeight > IMAGE_MAX_SIZE || bitOption.outWidth > IMAGE_MAX_SIZE) {
                scale = (int) Math.pow(2, (int) Math.round(Math.log(IMAGE_MAX_SIZE / (double) Math.max(bitOption.outHeight, bitOption.outWidth)) / Math.log(0.5)));
            }

            //Decode with inSampleSize
            BitmapFactory.Options bitOption2 = new BitmapFactory.Options();
            bitOption2.inSampleSize = scale;
            fis = new FileInputStream(a_File);
            b = BitmapFactory.decodeStream(fis, null, bitOption2);
            fis.close();
        } catch (IOException e) {
        }
        return b;
    }

//    public static String getMACAddress(Context a_Ctx) {
//
//        WifiManager mng = (WifiManager) a_Ctx.getSystemService(WIFI_SERVICE);
//        WifiInfo info = mng.getConnectionInfo();
//        String mac = info.getMacAddress();
//        LOGe(TAG, "getMACAddress:" + mac);
//        return mac;
//    }

    public static void setUUIDNPhonNo(Context a_Ctx) {

        String android_id = Settings.Secure.getString(a_Ctx.getContentResolver(), Settings.Secure.ANDROID_ID);
        TelephonyManager telephonyManager = (TelephonyManager)a_Ctx.getSystemService(Context.TELEPHONY_SERVICE);
        String device_id = getNullCheck(telephonyManager.getDeviceId());    //IMEI
        String phone_no = getNullCheck(telephonyManager.getLine1Number());
        if (!phone_no.matches("")) {
            phone_no = phone_no.replace("+82", "0");
        }
        szPhoneNo = phone_no;

        if (getNullCheck(android_id).matches("9774d56d682e549c")) {
            szUUID = device_id;
        } else {
            if (getNullCheck(device_id).matches("")) {
                szUUID = android_id;
            } else {
                szUUID = device_id;
            }
        }

        if(szUUID.equals("")) {
            szUUID = android_id;
        }
    }

    public static String getIMEI(Context a_Ctx) {
        TelephonyManager telephonyManager = (TelephonyManager)a_Ctx.getSystemService(Context.TELEPHONY_SERVICE);
        String aIMEI = getNullCheck(telephonyManager.getDeviceId());    //IMEI
        return aIMEI;
    }

    public static String getUUID(Context a_Ctx) {
        if(szUUID.equals("")) {
            setUUIDNPhonNo(a_Ctx);
        }
        return szUUID;
    }

    public static String getPhoneNo(Context a_Ctx) {
        if(szUUID.equals("")) {
            setUUIDNPhonNo(a_Ctx);
        }
        if(szPhoneNo.length() == 0) {
            szPhoneNo = "0";
        }
        return szPhoneNo;
    }

    public static void HideKeyboardInput(Activity a_Act) {
        View v = a_Act.getCurrentFocus();
        if(v != null) {
            HideKeyboardInput(v, a_Act);
        }
    }

    public static void HideKeyboardInput(View a_View, Activity a_Act) {
        InputMethodManager inputManager = (InputMethodManager)a_Act.getSystemService(Context.INPUT_METHOD_SERVICE);
        inputManager.hideSoftInputFromWindow(a_View.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
    }

    public static void ShowKeyboardInput(Activity a_Act) {
        InputMethodManager inputManager = (InputMethodManager)a_Act.getSystemService(Context.INPUT_METHOD_SERVICE);
        View v = a_Act.getCurrentFocus();
        if(v != null) {
//	    	inputManager.hideSoftInputFromWindow(v.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
            inputManager.showSoftInput(v, InputMethodManager.SHOW_FORCED);
        }
    }

    public static class OptionItem {
        public String Code;
        public String CodeName;
        public OptionItem() {
            Code = "";
            CodeName = "";
        };
        public OptionItem(String a_Code, String a_CodeName) {
            Code = a_Code;
            CodeName = a_CodeName;
        };
    }

    public static Rect getViewRect(View a_View) {
        Rect w_Rect = new Rect(0,0,0,0);
        if(a_View != null) {
//			int[] nPos = new int[2];
//		    a_View.getLocationOnScreen(nPos);
//		    w_Rect.left = nPos[0];
//		    w_Rect.top = nPos[1];
            w_Rect.right = a_View.getWidth();
            w_Rect.bottom = a_View.getHeight();
        }
        return w_Rect;
    }

    public static String EUCKRToUTF8(String str) {
        if(str.equals(""))
            return "";
        String result = null;

        try {
            byte[] raws = str.getBytes("euc-kr");
            result = new String( raws, "utf-8" );
        }catch( java.io.UnsupportedEncodingException e ) {
        }
        return result;
    }

    public static int getApplicationVersionCode(Context context) {
        PackageManager packageManager = context.getPackageManager();
        try {
            PackageInfo packageInfo = packageManager.getPackageInfo(context.getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException ex) {} catch(Exception e){}
        return 0;
    }

    public static String getApplicationVersionName(Context context) {
        PackageManager packageManager = context.getPackageManager();
        try {
            PackageInfo packageInfo = packageManager.getPackageInfo(context.getPackageName(), 0);
            return packageInfo.versionName;
        } catch (PackageManager.NameNotFoundException ex) {} catch(Exception e){}
        return "";
    }

    public static ArrayList<String> getGalleryImagesPathAll(Activity activity) {

        ArrayList<String> listOfAllImages = new ArrayList<String>();

        Uri uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
        String[] projection = { MediaStore.MediaColumns.DATA, MediaStore.Images.Media.BUCKET_DISPLAY_NAME };
        Cursor cursor = activity.getContentResolver().query(uri, projection, null, null, null);

        int column_index_data = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
        int column_index_folder_name = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.BUCKET_DISPLAY_NAME);

        String PathOfImage = null;
        while (cursor.moveToNext()) {
            PathOfImage = cursor.getString(column_index_data);
            listOfAllImages.add(PathOfImage);
        }
        return listOfAllImages;
    }

    public static Bitmap getGalleryThumbnail(ContentResolver cr, String path) {
        Cursor cursor = cr.query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, new String[] { MediaStore.MediaColumns._ID }, MediaStore.MediaColumns.DATA + "=?", new String[] { path }, null);
        if (cursor != null && cursor.moveToFirst()) {
            int id = cursor.getInt(cursor.getColumnIndex(MediaStore.MediaColumns._ID));
            cursor.close();
            return MediaStore.Images.Thumbnails.getThumbnail(cr, id, MediaStore.Images.Thumbnails.MINI_KIND, null);
        }
        cursor.close();
        return null;
    }

    public static String getGalleryBitmapPath(Uri a_Uri, Context a_Ctx) {

        String[] filePathColumn = { MediaStore.Images.Media.DATA };
        Cursor cursor = a_Ctx.getContentResolver().query(a_Uri, filePathColumn, null, null, null);
        cursor.moveToFirst();
        int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
        String szPath = cursor.getString(columnIndex);
        cursor.close();

        return szPath;
    }

    public static Rect getResizeDimen(Uri a_Uri, int a_Max, Boolean bAlbum, Context a_Ctx) {

        Rect rectRes = new Rect(0,0,640,480);
        String szPath = "";
        if(bAlbum) {
            String[] filePathColumn = { MediaStore.Images.Media.DATA };
            Cursor cursor = a_Ctx.getContentResolver().query(a_Uri, filePathColumn, null, null, null);
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            szPath = cursor.getString(columnIndex);
            cursor.close();
        } else {
            szPath = a_Uri.getPath();
        }
        Log.w(TAG, "getResizeRatio-0: path:" + szPath);

        Bitmap bmRes = BitmapFactory.decodeFile(szPath);
//        Log.w(TAG, "getResizeRatio-1: w:" + bmRes.getWidth() + " h:" + bmRes.getHeight());

        double fRatio = 1;
        if(bmRes.getWidth() > bmRes.getHeight()) {
            if (bmRes.getWidth() > a_Max) {
                fRatio = bmRes.getWidth() / a_Max;
            }
        } else {
            if (bmRes.getHeight() > a_Max) {
                fRatio = bmRes.getHeight() / a_Max;
            }
        }

        rectRes.right = (int) (bmRes.getWidth() / fRatio);
        rectRes.bottom = (int) (bmRes.getHeight() / fRatio);
//        Log.w(TAG, "getResizeRatio-2: w:" + rectRes.width() + " h:" + rectRes.height());

        return rectRes;
    }

    public static int getStatusBarHeight(Context context) {
        int result = 0;
        int resourceId = context.getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = context.getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    public static Bitmap rotateBitmap(Bitmap src, float degree) {
        Matrix matrix = new Matrix();
//		matrix.preScale(-1.0f, 1.0f);
        matrix.postRotate(degree);
        return Bitmap.createBitmap(src, 0, 0, src.getWidth(), src.getHeight(), matrix, true);

    }

    public static void printAndroidPermission(Context a_Ctx)
    {
        Context context = a_Ctx;
        PackageManager pm = context.getPackageManager();
        CharSequence csPermissionGroupLabel;
        CharSequence csPermissionLabel;

        List<PermissionGroupInfo> lstGroups = pm.getAllPermissionGroups(0);
        for (PermissionGroupInfo pgi : lstGroups) {
            csPermissionGroupLabel = pgi.loadLabel(pm);
            Log.e("perm", pgi.name + ": " + csPermissionGroupLabel.toString());

            try {
                List<PermissionInfo> lstPermissions = pm.queryPermissionsByGroup(pgi.name, 0);
                for (PermissionInfo pi : lstPermissions) {
                    csPermissionLabel = pi.loadLabel(pm);
                    Log.e("perm", "   " + pi.name + ": " + csPermissionLabel.toString());
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    public static String buildString(char [] a_Data, int a_Len) {

        StringBuffer sb = new StringBuffer();
        for(int i = 0; i < a_Len; i++) {
            sb.append(a_Data[i]);
        }
        return sb.toString();
    }

    public static String buildStringFromBytes(byte [] a_Data, int a_Len) {

        StringBuffer sb = new StringBuffer();
        for(int i = 0; i < a_Len; i++) {
//            sb.append(Integer.toHexString(a_Data[i]) + " ");
            sb.append(String.format("%02x ", a_Data[i]));
        }
        return sb.toString();
    }

    public static void hackHardwareMenuButton(Activity a_Act) {
        try {
            ViewConfiguration config = ViewConfiguration.get(a_Act);
            Field menuKeyField = ViewConfiguration.class.getDeclaredField("sHasPermanentMenuKey");
            if (menuKeyField != null) {
                menuKeyField.setAccessible(true);
                menuKeyField.setBoolean(config, false);
            }
        } catch (Exception ignored) {
        }
    }

    public static void killApp(Activity a_Act) {
        a_Act.finish();
        System.runFinalization();
        System.exit(0);
    }

    public static long convertToTimeInMilliseconds(SimpleDateFormat a_Sdf, String a_DateTime) {
        long timeInMilliseconds = 0;
        try {
            timeInMilliseconds = a_Sdf.parse(a_DateTime).getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return timeInMilliseconds;
    }

    public static String checkCountryCode(Context a_Ctx) {
        TelephonyManager tm = (TelephonyManager) a_Ctx.getSystemService(TELEPHONY_SERVICE);
        //현재 등록된 망 사업자의 MCC(Mobile Country Code)에 대한 ISO 국가코드 반환
        LOGe(TAG, "getNETWORKCountryIso :" + tm.getNetworkCountryIso());
        LOGe(TAG, "getSimCountryIso :" + tm.getSimCountryIso());
        LOGe(TAG, "Locale.getDefault().getCountry() :" + Locale.getDefault().getCountry());

        String aCode = tm.getNetworkCountryIso();
        if (aCode == null || aCode.length() == 0) {
            aCode = tm.getSimCountryIso();
            if (aCode == null || aCode.length() == 0) {
                aCode = Locale.getDefault().getCountry();
            }
        }
        LOGe(TAG, "checkCountryCode:" + aCode);
        return aCode;
    }

    @NonNull
    public static Resources getLocalizedResources(Context context, Locale desiredLocale) {
        Configuration conf = context.getResources().getConfiguration();
        conf = new Configuration(conf);
        conf.setLocale(desiredLocale);
        Context localizedContext = context.createConfigurationContext(conf);
        return localizedContext.getResources();
    }

    public static long getDaysFromDateToDate(Date a_From, Date a_To) {
        long aDiff = a_To.getTime() - a_From.getTime();
        long aDays = TimeUnit.DAYS.convert(aDiff, TimeUnit.MILLISECONDS);

        return aDays;
    }
}
