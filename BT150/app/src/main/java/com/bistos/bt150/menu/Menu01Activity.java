package com.bistos.bt150.menu;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import androidx.appcompat.app.AppCompatActivity;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bistos.bt150.R;
import com.bistos.bt150.com.PHCommon;
import com.bistos.bt150.com.PHLib;
import com.bistos.bt150.com.TopMenu;
import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.telit.terminalio.TIOConnection;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import static com.bistos.bt150.com.PHCommon.ACT_MENU01;
import static com.bistos.bt150.com.PHCommon.OP_TYPE_DOUBLE;
import static com.bistos.bt150.com.PHCommon.OP_TYPE_SINGLE;
import static com.bistos.bt150.com.PHCommon.VOICE_MODE_OPERATION;
import static com.bistos.bt150.com.PHCommon.mPeripheral;
import static com.bistos.bt150.com.PHCommon.nCycleExp;
import static com.bistos.bt150.com.PHCommon.nCycleMsg;
import static com.bistos.bt150.com.PHCommon.nOperationMode;
import static com.bistos.bt150.com.PHCommon.nOperationType;
import static com.bistos.bt150.com.PHCommon.nPressureExp;
import static com.bistos.bt150.com.PHCommon.nPressureMsg;
import static com.bistos.bt150.com.PHCommon.nVoiceMode;
import static com.bistos.bt150.com.PHLib.LOGe;
import static com.bistos.bt150.com.VoiceCommand.CMD_CYCLE_DOWN;
import static com.bistos.bt150.com.VoiceCommand.CMD_CYCLE_UP;
import static com.bistos.bt150.com.VoiceCommand.CMD_LAMP;
import static com.bistos.bt150.com.VoiceCommand.CMD_MODE_CHANGE;
import static com.bistos.bt150.com.VoiceCommand.CMD_POWER;
import static com.bistos.bt150.com.VoiceCommand.CMD_PRESSURE_DOWN;
import static com.bistos.bt150.com.VoiceCommand.CMD_PRESSURE_UP;
import static com.bistos.bt150.com.VoiceCommand.CMD_TYPE_NORMAL;
import static com.bistos.bt150.com.VoiceCommand.CMD_TYPE_STEP;

/**
 * Created by lamb on 2017. 10. 26..
 */

public class Menu01Activity extends AppCompatActivity {

    private static final String TAG = "Menu01Activity";
    private static final float nChartTextDefSize = 2.5f;
    private static final float nChartTextTimerSize = 2.5f;

    private TopMenu topMenu;

    private PieChart mChartPressure;
    private PieChart mChartCycle;
    private PieChart mChartTimer;

    private LinearLayout linearLayout_ActMenu01_Expression;
    private LinearLayout linearLayout_ActMenu01_Message;

    private ImageView imageView_ActMenu01_Expression;
    private ImageView imageView_ActMenu01_Message;

    private LinearLayout linearLayout_ActMenu01_Single;
    private LinearLayout linearLayout_ActMenu01_Double;

    private ImageView imageView_ActMenu01_Single;
    private ImageView imageView_ActMenu01_Double;

    private ImageView imageView_ActMenu01_Power;
    private LinearLayout linearLayout_ActMenu01_Power;
    private LinearLayout linearLayout_ActMenu01_Lamp;
    private ImageView imageView_ActMenu01_Battery;

    private ImageButton imageButton_ActMenu01_PressureDown;
    private ImageButton imageButton_ActMenu01_PressureUp;
    private ImageButton imageButton_ActMenu01_CycleDown;
    private ImageButton imageButton_ActMenu01_CycleUp;

    private TextView textView_ActMenu01_PressureMin;
    private TextView textView_ActMenu01_PressureMax;
    private TextView textView_ActMenu01_CycleMin;
    private TextView textView_ActMenu01_CycleMax;

//    private ImageButton imageButton_ActMenu01_Power;

    private Timer mTimerElapsed = null;
    private TimerTask taskElapsed = null;

    private boolean bRecvFirst = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu01);

        nOperationMode = PHCommon.OP_MODE_EXP;

        topMenu = (TopMenu) findViewById(R.id.topMenu);
        topMenu.setOnTabSelectListener(new TopMenu.OnTabSelectListener() {
            @Override
            public void onSelect(int a_Idx) {
                linearLayout_ActMenu01_Power.setVisibility(View.INVISIBLE);
                PHCommon.doMenuAction(a_Idx, Menu01Activity.this);
                finish();
            }
        });

        mChartPressure = (PieChart) findViewById(R.id.chart1);
        mChartCycle = (PieChart) findViewById(R.id.chart2);
        mChartTimer = (PieChart) findViewById(R.id.chart3);


        initChart(mChartPressure);
        initChart(mChartCycle);
        initChart(mChartTimer, false);

        linearLayout_ActMenu01_Expression = (LinearLayout) findViewById(R.id.linearLayout_ActMenu01_Expression);
        linearLayout_ActMenu01_Message = (LinearLayout) findViewById(R.id.linearLayout_ActMenu01_Message);

        imageView_ActMenu01_Expression = (ImageView) findViewById(R.id.imageView_ActMenu01_Expression);
        imageView_ActMenu01_Message = (ImageView) findViewById(R.id.imageView_ActMenu01_Message);

        linearLayout_ActMenu01_Single = (LinearLayout) findViewById(R.id.linearLayout_ActMenu01_Single);
        linearLayout_ActMenu01_Double = (LinearLayout) findViewById(R.id.linearLayout_ActMenu01_Double);

        imageView_ActMenu01_Single = (ImageView) findViewById(R.id.imageView_ActMenu01_Single);
        imageView_ActMenu01_Double = (ImageView) findViewById(R.id.imageView_ActMenu01_Double);

        imageView_ActMenu01_Power = (ImageView) findViewById(R.id.imageView_ActMenu01_Power);
        linearLayout_ActMenu01_Power = (LinearLayout) findViewById(R.id.linearLayout_ActMenu01_Power);
        linearLayout_ActMenu01_Lamp = (LinearLayout) findViewById(R.id.linearLayout_ActMenu01_Lamp);
        imageView_ActMenu01_Battery = (ImageView) findViewById(R.id.imageView_ActMenu01_Battery);
        imageButton_ActMenu01_PressureDown = (ImageButton) findViewById(R.id.imageButton_ActMenu01_PressureDown);
        imageButton_ActMenu01_PressureUp = (ImageButton) findViewById(R.id.imageButton_ActMenu01_PressureUp);
        imageButton_ActMenu01_CycleDown = (ImageButton) findViewById(R.id.imageButton_ActMenu01_CycleDown);
        imageButton_ActMenu01_CycleUp = (ImageButton) findViewById(R.id.imageButton_ActMenu01_CycleUp);

        textView_ActMenu01_PressureMin = (TextView) findViewById(R.id.textView_ActMenu01_PressureMin);
        textView_ActMenu01_PressureMax = (TextView) findViewById(R.id.textView_ActMenu01_PressureMax);
        textView_ActMenu01_CycleMin = (TextView) findViewById(R.id.textView_ActMenu01_CycleMin);
        textView_ActMenu01_CycleMax = (TextView) findViewById(R.id.textView_ActMenu01_CycleMax);

//        imageButton_ActMenu01_Power = (ImageButton) findViewById(R.id.imageButton_ActMenu01_Power);
//        imageButton_ActMenu01_Power.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                PHLib.showYesNoDialog("Are you sure you want to power down?", Menu01Activity.this, new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialogInterface, int i) {
//                        LOGe(TAG, "send power down!!!!!!!!!");
//                        PHCommon.sendData("G;");
//                    }
//                });
//            }
//        });

        imageView_ActMenu01_Power.setOnClickListener(onClickListener);
        linearLayout_ActMenu01_Lamp.setOnClickListener(onClickListener);
        imageButton_ActMenu01_PressureDown.setOnClickListener(onClickListener);
        imageButton_ActMenu01_PressureUp.setOnClickListener(onClickListener);
        imageButton_ActMenu01_CycleDown.setOnClickListener(onClickListener);
        imageButton_ActMenu01_CycleUp.setOnClickListener(onClickListener);

        linearLayout_ActMenu01_Expression.setOnClickListener(onClickListener);
        linearLayout_ActMenu01_Message.setOnClickListener(onClickListener);
        linearLayout_ActMenu01_Single.setOnClickListener(onClickListener);
        linearLayout_ActMenu01_Double.setOnClickListener(onClickListener);

        refreshOperationData();

        registerReceiver(dataReceiver, new IntentFilter(PHCommon.MSG_DATA_RECV));
        registerReceiver(vcmdReceiver, new IntentFilter(PHCommon.MSG_VCMD_RECV));


        startElapsedTimer();
    }

    private final BroadcastReceiver dataReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent a_intent) {
            Bundle bundle = a_intent.getExtras();
            String aData = bundle.getString("recv_data");
            if (aData != null) {
//            LOGe(TAG, "dataReceiver:" + aData);
                parseRecvData(PHCommon.gRecvData);
            }

            String aBattery = bundle.getString("recv_battery");
            if (aBattery != null) {
                parseRecvBattery(aBattery);
            }
        }
    };

    private final BroadcastReceiver vcmdReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent a_intent) {

            Bundle bundle = a_intent.getExtras();
            int aCmdCode = bundle.getInt("CmdCode");
            int aCmdType = bundle.getInt("CmdType");
            int aStepVal = bundle.getInt("StepVal");
            LOGe(TAG, "onReceive: aCmdCode:" + aCmdCode + " aCmdType:" + aCmdType + " aStepVal:" + aStepVal);

            if (aCmdType == CMD_TYPE_NORMAL) {
                switch (aCmdCode) {
                    case CMD_POWER:
                        PHCommon.powerDown(Menu01Activity.this);
                        break;
                    case CMD_LAMP:
                        PHCommon.toggleLamp();
                        break;
                    case CMD_MODE_CHANGE:
                        PHCommon.sendData(PHCommon.sCmdModeChange);
                        break;
//                    case CMD_MODE_EXPRESS:
//                    case CMD_EXPRESS:
//                        selectOperation(PHCommon.OP_MODE_EXP);
//                        break;
//                    case CMD_MODE_MASSAGE:
//                    case CMD_MASSAGE:
//                        selectOperation(PHCommon.OP_MODE_MSG);
//                        break;
                    case CMD_PRESSURE_UP:
                        doPressureUp();
                        break;
                    case CMD_PRESSURE_DOWN:
                        doPressureDown();
                        break;
                    case CMD_CYCLE_UP:
                        doCycleUp();
                        break;
                    case CMD_CYCLE_DOWN:
                        doCycleDown();
                        break;
                    default:
                        break;
                }

            } else if (aCmdType == CMD_TYPE_STEP) {
//                switch (aCmdCode) {
//                    case CMD_PRESSURE_N:
//                        doPressureN(aStepVal);
//                        break;
//                    case CMD_CYCLE_N:
//                        doCycleN(aStepVal);
//                        break;
//                    default:
//                        break;
//                }
            }
        }
    };

    private void parseRecvBattery(final String a_Battery) {
        PHCommon.gRecvBattery = a_Battery;

        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                if (a_Battery.equals("0")) {
                    imageView_ActMenu01_Battery.setImageResource(R.drawable.operation_b_0);

                } else if (a_Battery.equals("1")) {
                    imageView_ActMenu01_Battery.setImageResource(R.drawable.operation_b_1);

                } else if (a_Battery.equals("2")) {
                    imageView_ActMenu01_Battery.setImageResource(R.drawable.operation_b_2);

                } else if (a_Battery.equals("3")) {
                    imageView_ActMenu01_Battery.setImageResource(R.drawable.operation_b_3);

                } else if (a_Battery.equals("4")) {
                    imageView_ActMenu01_Battery.setImageResource(R.drawable.operation_b_4);

                } else if (a_Battery.equals("5")) {
                    imageView_ActMenu01_Battery.setImageResource(R.drawable.operation_b_5);
                }

            }
        });
    }

    private void parseRecvData(String a_Recv) {

//        LOGe(TAG, "parseRecvData:" + a_Recv);
        if (a_Recv.length() >= 14
                && a_Recv.substring(0, 1).equals("M")
                && a_Recv.substring(2, 3).equals("P")
                && a_Recv.substring(5, 6).equals("C")
                && a_Recv.substring(7, 8).equals("R")) {

            bRecvFirst = true;

            nOperationMode = PHLib.convertToInt(a_Recv.substring(1, 2));

            if (a_Recv.length() > 15 && a_Recv.substring(13, 14).equals("W")) {
                String aOpType = a_Recv.substring(14, 15);
                if (aOpType.equals("0")) {
                    nOperationType = OP_TYPE_SINGLE;
                } else {
                    nOperationType = OP_TYPE_DOUBLE;
                }
            }

            if (nOperationMode == PHCommon.OP_MODE_EXP) {
                nPressureExp = PHLib.convertToInt(a_Recv.substring(3, 5));
                nCycleExp = PHLib.convertToInt(a_Recv.substring(6, 7));

            } else if (nOperationMode == PHCommon.OP_MODE_MSG) {
                nPressureMsg = PHLib.convertToInt(a_Recv.substring(3, 5));
                nCycleMsg = PHLib.convertToInt(a_Recv.substring(6, 7));
            }

//            LOGe(TAG, "nPressureExp:" + nPressureExp + " nPressureMsg:" + nPressureMsg);

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    selectOperation(nOperationMode);
                    selectOperationType(nOperationType);
                }
            });
        }
    }

    private void selectOperationType(int a_Type) {
        selectOperationType(a_Type, true);
    }

    private void selectOperationType(int a_Type, boolean a_Disp) {

        if (a_Type == PHCommon.OP_TYPE_SINGLE) {
//            if (nOperationType != a_Type && !a_Disp) {
//                PHCommon.sendData("W;");
//            }
            imageView_ActMenu01_Single.setBackgroundResource(R.drawable.menu_ic_single_ov);
            imageView_ActMenu01_Double.setBackgroundResource(R.drawable.menu_ic_double);

            nOperationType = PHCommon.OP_TYPE_SINGLE;

        } else {
//            if (nOperationType != a_Type && !a_Disp) {
//                PHCommon.sendData("W;");
//            }
            imageView_ActMenu01_Single.setBackgroundResource(R.drawable.menu_ic_single);
            imageView_ActMenu01_Double.setBackgroundResource(R.drawable.menu_ic_double_ov);

            nOperationType = PHCommon.OP_TYPE_DOUBLE;
        }

        if (!a_Disp && !PHCommon.bProgramRunning) {
            PHCommon.sendData("W;");
        }

    }

    private void selectOperation(int a_Operation) {

//        LOGe(TAG, "selectOperation:" + a_Operation);

        if (a_Operation == PHCommon.OP_MODE_EXP) {
            if (nOperationMode == PHCommon.OP_MODE_MSG) {
                PHCommon.sendData(PHCommon.sCmdModeChange);
            }

            imageView_ActMenu01_Expression.setBackgroundResource(R.drawable.menu_ic_expr_ov);
            imageView_ActMenu01_Message.setBackgroundResource(R.drawable.menu_ic_mass);

            nOperationMode = PHCommon.OP_MODE_EXP;
            refreshOperationData();

        } else if (a_Operation == PHCommon.OP_MODE_MSG) {
            if (nOperationMode == PHCommon.OP_MODE_EXP) {
                PHCommon.sendData(PHCommon.sCmdModeChange);
            }

            imageView_ActMenu01_Expression.setBackgroundResource(R.drawable.menu_ic_expr);
            imageView_ActMenu01_Message.setBackgroundResource(R.drawable.menu_ic_mass_ov);

            nOperationMode = PHCommon.OP_MODE_MSG;
            refreshOperationData();
        }
    }

    private void startElapsedTimer() {

        stopElapsedTimer();

        taskElapsed = new TimerTask() {
            public void run() {
                if (mPeripheral != null
                        && mPeripheral.getConnectionState() == TIOConnection.STATE_CONNECTED
                        && PHCommon.bDeviceWakeUp) {

                    PHCommon.nElapsedTime++;
                    if (PHCommon.nElapsedTime > PHCommon.ELAPSED_TIME_MAX) {
                        PHCommon.nElapsedTime = PHCommon.ELAPSED_TIME_MAX;
                    }

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            refreshChart(mChartTimer, PHCommon.OP_TIMER);
                        }
                    });

//                } else {
//                    PHCommon.nElapsedTime = 0;
                }
            }
        };
        mTimerElapsed = new Timer();
        mTimerElapsed.schedule(taskElapsed, 1000, 1000);
    }

    private void stopElapsedTimer() {
        if (mTimerElapsed != null) {
            mTimerElapsed.cancel();
            mTimerElapsed.purge();
            mTimerElapsed = null;
        }
    }

    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {

            if (view.getId() == R.id.imageButton_ActMenu01_PressureDown) {
                doPressureDown();

            } else if (view.getId() == R.id.imageButton_ActMenu01_PressureUp) {
                doPressureUp();

            } else if (view.getId() == R.id.imageButton_ActMenu01_CycleDown) {
                doCycleDown();

            } else if (view.getId() == R.id.imageButton_ActMenu01_CycleUp) {
                doCycleUp();

            } else if (view.getId() == R.id.linearLayout_ActMenu01_Expression) {
                selectOperation(PHCommon.OP_MODE_EXP);

            } else if (view.getId() == R.id.linearLayout_ActMenu01_Message) {
                selectOperation(PHCommon.OP_MODE_MSG);

            } else if (view.getId() == R.id.linearLayout_ActMenu01_Single) {
                selectOperationType(OP_TYPE_SINGLE, false);

            } else if (view.getId() == R.id.linearLayout_ActMenu01_Double) {
                selectOperationType(OP_TYPE_DOUBLE, false);

            } else if (view.getId() == R.id.imageView_ActMenu01_Power) {
                PHCommon.powerDown(Menu01Activity.this);

            } else if (view.getId() == R.id.linearLayout_ActMenu01_Lamp) {
                PHCommon.toggleLamp();
            }
        }
    };

    @Override
    protected void onDestroy() {
        unregisterReceiver(dataReceiver);
        stopElapsedTimer();

        super.onDestroy();
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);

        if (hasFocus) {
            parseRecvData(PHCommon.gRecvData);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        PHCommon.nActivityNo = ACT_MENU01;

        nVoiceMode = VOICE_MODE_OPERATION;

        PHCommon.sendData(PHCommon.sCmdElapsedTime);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                PHCommon.sendData(PHCommon.sCmdInqueryStatus);
            }
        }, 100);

        LOGe(TAG, "PHCommon.gRecvData: " + PHCommon.gRecvData);

        linearLayout_ActMenu01_Power.setVisibility(View.VISIBLE);
        topMenu.selectMenu(1);
    }

    private void doPressureDown() {

        PHCommon.sendData(PHCommon.sCmdPressureDown);

        if (nOperationMode == PHCommon.OP_MODE_EXP) {
            nPressureExp--;
        } else if (nOperationMode == PHCommon.OP_MODE_MSG) {
            nPressureMsg--;
        }

        if(nPressureExp < PHCommon.EXP_PRESSURE_MIN) {
            nPressureExp = PHCommon.EXP_PRESSURE_MIN;
        }

        if(nPressureMsg < PHCommon.MSG_PRESSURE_MIN) {
            nPressureMsg = PHCommon.MSG_PRESSURE_MIN;
        }

        refreshChart(mChartPressure, PHCommon.OP_PRESSURE);
    }

    private void doPressureUp() {

        PHCommon.sendData(PHCommon.sCmdPressureUp);

        if (nOperationMode == PHCommon.OP_MODE_EXP) {
            nPressureExp++;
        } else if (nOperationMode == PHCommon.OP_MODE_MSG) {
            nPressureMsg++;
        }

        if(nPressureExp > PHCommon.EXP_PRESSURE_MAX) {
            nPressureExp = PHCommon.EXP_PRESSURE_MAX;
        }

        if(nPressureMsg > PHCommon.arrPressureLimit[nCycleMsg]) {
            nPressureMsg = PHCommon.arrPressureLimit[nCycleMsg];
        }

        refreshChart(mChartPressure,PHCommon.OP_PRESSURE);
    }

    private static Timer timerInitDataReceived;
    private static int nTimerInitDataReceivedMax = 30;

    private static void stopInitDataReceivedTimer() {
        nTimerInitDataReceivedMax = 30;

        if (timerInitDataReceived != null) {
            timerInitDataReceived.cancel();
            timerInitDataReceived.purge();
            timerInitDataReceived = null;
        }
    }

    private void doPressureN(final int a_PressureLevel) {
        stopInitDataReceivedTimer();

        TimerTask timerTaskInitDataReceived = new TimerTask() {
            @Override
            public void run() {

                if(bRecvFirst) {
                    stopInitDataReceivedTimer();
                    processPressureN(a_PressureLevel);
                }

                if (nTimerInitDataReceivedMax < 0) {
                    stopInitDataReceivedTimer();
                }

                nTimerInitDataReceivedMax--;
            }
        };

        timerInitDataReceived = new Timer();
        timerInitDataReceived.schedule(timerTaskInitDataReceived, 500, 500);
    }

    private void doCycleN(final int a_CycleLevel) {
        stopInitDataReceivedTimer();

        TimerTask timerTaskInitDataReceived = new TimerTask() {
            @Override
            public void run() {

                if(bRecvFirst) {
                    stopInitDataReceivedTimer();
                    processCycleN(a_CycleLevel);
                }

                if (nTimerInitDataReceivedMax < 0) {
                    stopInitDataReceivedTimer();
                }

                nTimerInitDataReceivedMax--;
            }
        };

        timerInitDataReceived = new Timer();
        timerInitDataReceived.schedule(timerTaskInitDataReceived, 500, 500);
    }

    private void processPressureN(int a_Level) {

        int aPressureLevel = 0;

        if (nOperationMode == PHCommon.OP_MODE_EXP) {
            aPressureLevel = nPressureExp;
        } else if (nOperationMode == PHCommon.OP_MODE_MSG) {
            aPressureLevel = nPressureMsg;
        }

        int aPressureToMove = aPressureLevel - a_Level;
        if (aPressureToMove == 0) {
            return ;
        } else if (aPressureToMove < 0) {
            for (int i = aPressureToMove; i < -1; i++) {
                PHCommon.sendData(PHCommon.sCmdPressureUp);
            }
        } else if (aPressureToMove > 0) {
            for (int i = 0; i <= aPressureToMove; i++) {
                PHCommon.sendData(PHCommon.sCmdPressureDown);
            }
        }

        if (nOperationMode == PHCommon.OP_MODE_EXP) {
            if (aPressureToMove < 0) {
                nPressureExp++;
            } else {
                nPressureExp--;
            }

        } else if (nOperationMode == PHCommon.OP_MODE_MSG) {
            if (aPressureToMove < 0) {
                nPressureMsg++;
            } else {
                nPressureMsg--;
            }
        }

        //Upper Limit
        if(nPressureExp > PHCommon.EXP_PRESSURE_MAX) {
            nPressureExp = PHCommon.EXP_PRESSURE_MAX;
        }

        if(nPressureMsg > PHCommon.arrPressureLimit[nCycleMsg]) {
            nPressureMsg = PHCommon.arrPressureLimit[nCycleMsg];
        }

        //Lower Limit
        if(nPressureExp < PHCommon.EXP_PRESSURE_MIN) {
            nPressureExp = PHCommon.EXP_PRESSURE_MIN;
        }

        if(nPressureMsg < PHCommon.MSG_PRESSURE_MIN) {
            nPressureMsg = PHCommon.MSG_PRESSURE_MIN;
        }

        refreshChart(mChartPressure,PHCommon.OP_PRESSURE);
    }

    private void processCycleN(int a_Level) {

        int aCycleLevel = 0;

        if (nOperationMode == PHCommon.OP_MODE_EXP) {
            aCycleLevel = nCycleExp;
        } else if (nOperationMode == PHCommon.OP_MODE_MSG) {
            aCycleLevel = nCycleMsg;
        }

        int aCycleToMove = aCycleLevel - a_Level;
        if (aCycleToMove == 0) {
            return ;
        } else if (aCycleToMove < 0) {
            for (int i = aCycleToMove; i < -1; i++) {
                PHCommon.sendData(PHCommon.sCmdCycleUp);
            }
        } else if (aCycleToMove > 0) {
            for (int i = 0; i <= aCycleToMove; i++) {
                PHCommon.sendData(PHCommon.sCmdCycleDown);
            }
        }

        if (nOperationMode == PHCommon.OP_MODE_EXP) {
            if (aCycleToMove < 0) {
                nCycleExp++;
            } else {
                nCycleExp--;
            }

        } else if (nOperationMode == PHCommon.OP_MODE_MSG) {
            if (aCycleToMove < 0) {
                nCycleMsg++;
            } else {
                nCycleMsg--;
            }
        }

        //Upper Limit
        if(nCycleExp > PHCommon.EXP_CYCLE_MAX) {
            nCycleExp = PHCommon.EXP_CYCLE_MAX;
        }

        if(nCycleMsg > PHCommon.MSG_CYCLE_MAX) {
            nCycleMsg = PHCommon.MSG_CYCLE_MAX;
        }

        //Lower Limit
        if(nCycleExp < PHCommon.EXP_CYCLE_MIN) {
            nCycleExp = PHCommon.EXP_CYCLE_MIN;
        }

        if(nCycleMsg < PHCommon.MSG_CYCLE_MIN) {
            nCycleMsg = PHCommon.MSG_CYCLE_MIN;
        }

        refreshChart(mChartCycle, PHCommon.OP_CYCLE);
    }

    private void doCycleDown() {

        PHCommon.sendData(PHCommon.sCmdCycleDown);

        if (nOperationMode == PHCommon.OP_MODE_EXP) {
            nCycleExp--;
        } else if (nOperationMode == PHCommon.OP_MODE_MSG) {
            nCycleMsg--;
        }

        if(nCycleExp < PHCommon.EXP_CYCLE_MIN) {
            nCycleExp = PHCommon.EXP_CYCLE_MIN;
        }

        if(nCycleMsg < PHCommon.MSG_CYCLE_MIN) {
            nCycleMsg = PHCommon.MSG_CYCLE_MIN;
        }

//        refreshChart(mChartCycle, PHCommon.OP_CYCLE);
        refreshOperationData();
    }

    private void doCycleUp() {

        PHCommon.sendData(PHCommon.sCmdCycleUp);

        if (nOperationMode == PHCommon.OP_MODE_EXP) {
            nCycleExp++;
        } else if (nOperationMode == PHCommon.OP_MODE_MSG) {
            nCycleMsg++;
        }

        if(nCycleExp > PHCommon.EXP_CYCLE_MAX) {
            nCycleExp = PHCommon.EXP_CYCLE_MAX;
        }

        if(nCycleMsg > PHCommon.MSG_CYCLE_MAX) {
            nCycleMsg = PHCommon.MSG_CYCLE_MAX;
        }

//        refreshChart(mChartCycle, PHCommon.OP_CYCLE);
        refreshOperationData();
    }

    private void reCalcPressureValue() {
        if (nCycleMsg >= 0 && nCycleMsg < PHCommon.arrPressureLimit.length) {
            int aPressure = PHCommon.arrPressureLimit[nCycleMsg];
            if (nPressureMsg > aPressure) {
                nPressureMsg = aPressure;
            }
        }
//        LOGe(TAG, "reCalcPressureValue: nCycleMsg:" + nCycleMsg + " nPressureMsg:" + nPressureMsg + " aPressure:" + PHCommon.arrPressureLimit[nCycleMsg]);
    }

    private void refreshChart(PieChart a_Chart, int a_Type) {

        int aChartValue = 0;
        float aValueData = 0f;
        int aValueMax = 0;

        if (nOperationMode == PHCommon.OP_MODE_MSG) {
            reCalcPressureValue();

            textView_ActMenu01_PressureMin.setText(String.format("%d", PHCommon.MSG_PRESSURE_MIN + 1));
            textView_ActMenu01_PressureMax.setText(String.format("%d", PHCommon.arrPressureLimit[nCycleMsg] + 1));

            textView_ActMenu01_CycleMin.setText(String.format("%d", PHCommon.MSG_CYCLE_MIN + 1));
            textView_ActMenu01_CycleMax.setText(String.format("%d", PHCommon.MSG_CYCLE_MAX + 1));

        } else if (nOperationMode == PHCommon.OP_MODE_EXP) {
            textView_ActMenu01_PressureMin.setText(String.format("%d", PHCommon.EXP_PRESSURE_MIN + 1));
            textView_ActMenu01_PressureMax.setText(String.format("%d", PHCommon.EXP_PRESSURE_MAX + 1));

            textView_ActMenu01_CycleMin.setText(String.format("%d", PHCommon.EXP_CYCLE_MIN + 1));
            textView_ActMenu01_CycleMax.setText(String.format("%d", PHCommon.EXP_CYCLE_MAX + 1));
        }

        if (a_Type == PHCommon.OP_PRESSURE) {
            if (nOperationMode == PHCommon.OP_MODE_EXP) {
                aChartValue = nPressureExp + 1;
                aValueData =  (float)aChartValue / (float)(PHCommon.EXP_PRESSURE_MAX + 1);
                aValueMax = PHCommon.EXP_PRESSURE_MAX + 1;

            } else if (nOperationMode == PHCommon.OP_MODE_MSG) {
                aChartValue = nPressureMsg + 1;
                aValueData = (float)aChartValue / (float)(PHCommon.arrPressureLimit[nCycleMsg] + 1);
                aValueMax = PHCommon.arrPressureLimit[nCycleMsg] + 1;
            }

        } else if (a_Type == PHCommon.OP_CYCLE) {
            if (nOperationMode == PHCommon.OP_MODE_EXP) {
                aChartValue = nCycleExp + 1;
                aValueData =  (float)aChartValue / (float)(PHCommon.EXP_CYCLE_MAX + 1);
                aValueMax = PHCommon.EXP_CYCLE_MAX + 1;

            } else if (nOperationMode == PHCommon.OP_MODE_MSG) {
                aChartValue = nCycleMsg + 1;
                aValueData = (float)aChartValue / (float)(PHCommon.MSG_CYCLE_MAX + 1);
                aValueMax = PHCommon.MSG_CYCLE_MAX + 1;
            }

        } else if (a_Type == PHCommon.OP_TIMER) {
            aChartValue = PHCommon.nElapsedTime;
//            aValueData = (float)aChartValue / (float)PHCommon.ELAPSED_TIME_MAX;
//            aValueMax = PHCommon.ELAPSED_TIME_MAX;
            aValueData = (float)aChartValue / (float)PHCommon.ELAPSED_TIME_MAX;
            aValueMax = PHCommon.ELAPSED_TIME_MAX / 60;
        }

        if (a_Type == PHCommon.OP_CYCLE) {
            if (nOperationMode == PHCommon.OP_MODE_EXP) {
                a_Chart.setCenterText(generateCenterSpannableText(String.format("%d", nCycleExp + 1)));
            } else if (nOperationMode == PHCommon.OP_MODE_MSG) {
                a_Chart.setCenterText(generateCenterSpannableText(String.format("%d", nCycleMsg + 1)));
            }

        } else if (a_Type == PHCommon.OP_PRESSURE) {
            a_Chart.setCenterText(generateCenterSpannableText(String.format("%d", aChartValue)));

        } else if (a_Type == PHCommon.OP_TIMER) {
            int aMin = aChartValue / 60;
            int aSec = aChartValue % 60;
            a_Chart.setCenterText(generateCenterSpannableText(String.format("%02d:%02d", aMin, aSec), nChartTextTimerSize));

        } else {
            a_Chart.setCenterText(generateCenterSpannableText(String.format("%d", aChartValue + 1), nChartTextTimerSize));
        }

        setChartData(aValueData, aValueMax, a_Chart);
    }

    private void refreshOperationData() {

        refreshChart(mChartPressure, PHCommon.OP_PRESSURE);
        refreshChart(mChartCycle, PHCommon.OP_CYCLE);
        refreshChart(mChartTimer, PHCommon.OP_TIMER);
    }

    private void moveOffScreen(PieChart a_Chart) {

//        Display display = getWindowManager().getDefaultDisplay();
//        int height = display.getHeight();  // deprecated
//
//        int offset = (int)(height * 0.65); /* percent to move */
//
//        RelativeLayout.LayoutParams rlParams = (RelativeLayout.LayoutParams)a_Chart.getLayoutParams();
//        rlParams.setMargins(0, 0, 0, -offset);
//        a_Chart.setLayoutParams(rlParams);
    }

    private SpannableString generateCenterSpannableText(String a_Text) {
        return generateCenterSpannableText(a_Text, nChartTextDefSize);
    }

    private SpannableString generateCenterSpannableText(String a_Text, float a_TextSize) {
        SpannableString s = new SpannableString(a_Text);
        s.setSpan(new RelativeSizeSpan(a_TextSize), 0, s.length(), 0);
        s.setSpan(new StyleSpan(Typeface.BOLD), 0, s.length(), 0);
        s.setSpan(new ForegroundColorSpan(0xffe98a65), 0, s.length(), 0);
//        s.setSpan(new RelativeSizeSpan(.8f), 14, s.length() - 15, 0);
//        s.setSpan(new StyleSpan(Typeface.ITALIC), s.length() - 14, s.length(), 0);
//        s.setSpan(new ForegroundColorSpan(ColorTemplate.getHoloBlue()), s.length() - 14, s.length(), 0);
        return s;
    }

    private void initChart(PieChart a_Chart) {
        initChart(a_Chart, true);
    }

    private void initChart(PieChart a_Chart, boolean a_Half) {

        a_Chart.setBackgroundColor(Color.WHITE);

//        moveOffScreen(a_Chart);

        a_Chart.setUsePercentValues(true);
        a_Chart.getDescription().setEnabled(false);

//        a_Chart.setCenterTextTypeface(mTfLight);

        a_Chart.setDrawHoleEnabled(true);
        a_Chart.setHoleColor(Color.WHITE);

        a_Chart.setTransparentCircleColor(Color.WHITE);
        a_Chart.setTransparentCircleAlpha(110);

        a_Chart.setHoleRadius(58f);
        a_Chart.setTransparentCircleRadius(61f);

        a_Chart.setDrawCenterText(true);

        a_Chart.setRotationEnabled(false);
        a_Chart.setHighlightPerTapEnabled(true);

        if (a_Half) {
            a_Chart.setCenterText(generateCenterSpannableText("0", nChartTextDefSize));
            a_Chart.setMaxAngle(180f); // HALF CHART
            a_Chart.setRotationAngle(180f);
            a_Chart.setCenterTextOffset(0, -10);
        } else {
            a_Chart.setCenterText(generateCenterSpannableText("0", nChartTextTimerSize));
        }

        setChartData(0f, 16, a_Chart);

        a_Chart.animateY(1400, Easing.EasingOption.EaseInOutQuad);

        Legend l = a_Chart.getLegend();
//        l.setVerticalAlignment(Legend.LegendVerticalAlignment.TOP);
//        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.CENTER);
//        l.setOrientation(Legend.LegendOrientation.HORIZONTAL);
//        l.setDrawInside(false);
//        l.setXEntrySpace(7f);
//        l.setYEntrySpace(0f);
//        l.setYOffset(0f);
        l.setEnabled(false);

        // entry label styling
        a_Chart.setEntryLabelColor(Color.WHITE);
//        a_Chart.setEntryLabelTypeface(mTfRegular);
        a_Chart.setEntryLabelTextSize(12f);

    }

    private void setChartData(float a_Value, int a_SliceCnt, PieChart a_Chart) {

        ArrayList<PieEntry> values = new ArrayList<PieEntry>();

        int[] arrColors;
//        if (a_SliceCnt < 0) {
        if (a_Chart.equals(mChartTimer)) {
//            arrColors = PHCommon.PIECHART_COLORS;
//            values.add(new PieEntry(a_Value, ""));
//            values.add(new PieEntry(1f - a_Value, ""));

            arrColors = new int[a_SliceCnt];
            float aSliceValue = 1f / (float) a_SliceCnt;
            int aValueIdx = (int)((float)a_SliceCnt * a_Value);

            for (int i = 0; i < a_SliceCnt; i++) {
                if (i < aValueIdx) {
                    arrColors[i] = 0xffffa500;
                    values.add(new PieEntry(aSliceValue, ""));
                } else {
                    arrColors[i] = 0xffb2aca1;
                    values.add(new PieEntry(aSliceValue, ""));
                }
            }

        } else {
            arrColors = new int[a_SliceCnt];
            float aSliceValue = 1f / (float) a_SliceCnt;
            int aValueIdx = (int)((float)a_SliceCnt * a_Value);

            for (int i = 0; i < a_SliceCnt; i++) {
                if (i < aValueIdx) {
                    arrColors[i] = 0xffffa500;
                    values.add(new PieEntry(aSliceValue, ""));
                } else {
                    arrColors[i] = 0xffb2aca1;
                    values.add(new PieEntry(aSliceValue, ""));
                }
            }
        }

        PieDataSet dataSet = new PieDataSet(values, "");
        dataSet.setSliceSpace(1f);
//        dataSet.setSelectionShift(5f);

        dataSet.setColors(arrColors);
//        dataSet.setColors(PHCommon.PIECHART_COLORS);
        //dataSet.setSelectionShift(0f);

        PieData data = new PieData(dataSet);
//        data.setValueFormatter(new PercentFormatter());
//        data.setValueTextSize(11f);
//        data.setValueTextColor(Color.WHITE);
//        data.setValueTypeface(mTfLight);
        data.setDrawValues(false);
        a_Chart.setData(data);

        a_Chart.invalidate();
    }

}