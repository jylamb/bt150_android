package com.bistos.bt150;

import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.bistos.bt150.com.PHCommon;

import java.util.ArrayList;

import static com.bistos.bt150.com.PHCommon.getVCmdResource;

/**
 * Created by jylamb on 01/04/2019.
 * Philosys
 * jylamb@philosys.com
 */
public class HelpActivity extends AppCompatActivity {

    private static final String TAG = "HelpActivity";

    private ListView listViewVCmd;
    private ArrayList<VCmdHelpData> arrData = null;
    private VCmdHelpListAdapter vCmdHelpListAdapter = null;

    private boolean bFirst = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help);


        Button button_ActMain_Close = (Button) findViewById(R.id.button_ActMain_Close);
        button_ActMain_Close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        arrData = new ArrayList<VCmdHelpData>();
        vCmdHelpListAdapter = new VCmdHelpListAdapter(this);
        ListView initVCmdDataList = (ListView) findViewById(R.id.listView_Help_VCmdHelp);
        initVCmdDataList.setAdapter(vCmdHelpListAdapter);

    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);

        if (bFirst && hasFocus) {
            bFirst = false;

            initVCmdDataList();
        }
    }

    private void initVCmdDataList() {
        arrData.clear();

        Resources res = getVCmdResource(this, PHCommon.curSpeechLanguage);

        addVCmdData(res.getString(R.string.CMD_MODE_CHANGE), getString(R.string.CMD_MODE_CHANGE_DESC));

        addVCmdData(res.getString(R.string.CMD_PRESSURE_UP), getString(R.string.CMD_PRESSURE_UP_DESC));
        addVCmdData(res.getString(R.string.CMD_PRESSURE_DOWN), getString(R.string.CMD_PRESSURE_DOWN_DESC));

        addVCmdData(res.getString(R.string.CMD_CYCLE_UP), getString(R.string.CMD_CYCLE_UP_DESC));
        addVCmdData(res.getString(R.string.CMD_CYCLE_DOWN), getString(R.string.CMD_CYCLE_DOWN_DESC));

        addVCmdData(res.getString(R.string.CMD_PLAY), getString(R.string.CMD_PLAY_DESC));
        addVCmdData(res.getString(R.string.CMD_STOP), getString(R.string.CMD_STOP_DESC));
        addVCmdData(res.getString(R.string.CMD_SKIP), getString(R.string.CMD_SKIP_DESC));
        addVCmdData(res.getString(R.string.CMD_PROGRAMID_N), getString(R.string.CMD_PROGRAMID_N_DESC));

        addVCmdData(res.getString(R.string.CMD_POWER), getString(R.string.CMD_POWER_DESC));
        addVCmdData(res.getString(R.string.CMD_LAMP), getString(R.string.CMD_LAMP_DESC));

        vCmdHelpListAdapter.notifyDataSetChanged();
    }

    private void addVCmdData(String a_VCmd, String a_VCmdDesc) {
        VCmdHelpData vCmdHelpData = new VCmdHelpData();
        vCmdHelpData.sVCmd = a_VCmd;
        vCmdHelpData.sVCmdDesc = a_VCmdDesc;
        arrData.add(vCmdHelpData);
    }


    private class VCmdHelpListAdapter extends BaseAdapter {
        private LayoutInflater mInflater;

        public VCmdHelpListAdapter (Context context) {
            mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            return arrData.size();
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            LinearLayout linearLayout_RowVCmdHelp_Base;
            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.row_vcmd_help_list, parent, false);
                linearLayout_RowVCmdHelp_Base = (LinearLayout)convertView.findViewById(R.id.linearLayout_RowVCmdHelp_Base);
            } else {
                linearLayout_RowVCmdHelp_Base = (LinearLayout)convertView.findViewById(R.id.linearLayout_RowVCmdHelp_Base);
                if (linearLayout_RowVCmdHelp_Base == null) {
                    convertView = mInflater.inflate(R.layout.row_vcmd_help_list, parent, false);
                    linearLayout_RowVCmdHelp_Base = (LinearLayout)convertView.findViewById(R.id.linearLayout_RowVCmdHelp_Base);
                }
            }

            TextView textView_RowVCmd_HelpNo = (TextView) convertView.findViewById(R.id.textView_RowVCmd_HelpNo);
            TextView textView_RowVCmd_VCmd = (TextView) convertView.findViewById(R.id.textView_RowVCmd_VCmd);

            VCmdHelpData vCmdHelpData = arrData.get(position);

            textView_RowVCmd_HelpNo.setText(vCmdHelpData.sVCmd);
            textView_RowVCmd_VCmd.setText(vCmdHelpData.sVCmdDesc);


            return convertView;
        }
    }


    class VCmdHelpData {
        String sVCmd = "";
        String sVCmdDesc = "";

        VCmdHelpData() {

        }

        void initVCmdHelpData() {
            sVCmd  = "";
            sVCmdDesc = "";
        }

    }

}