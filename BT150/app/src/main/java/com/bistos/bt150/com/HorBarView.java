package com.bistos.bt150.com;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

import java.util.jar.Attributes;

/**
 * Created by lamb on 2017. 11. 8..
 */

public class HorBarView extends View {

    private Paint paintStroke;
    private Paint paintDraw;
    private float fPer = 0;
//    private View  vParent;
//    private float fBarH = 40f;
    private String sText = "";
    private float fTextSize = 20;
    private int nBarColor = 0xff04b5bb;
    private Context mCtx = null;


    public HorBarView(Context context) {
        super(context);

        initHorBarView(context);
    }

    public HorBarView(Context context, AttributeSet attrs) {
        super(context, attrs);

        initHorBarView(context);
    }

    public void initHorBarView(Context context) {

        mCtx = context;

        paintStroke = new Paint();
        paintDraw = new Paint();

        fTextSize = PHLib.convertDpToPixel(12, context);
    }

//    public void setBarH(float a_H) {
//        fBarH = a_H;
//    }


    public void setText(String a_Text) {
        sText = a_Text;
    }

    public void setBarColor(int a_Color) {
        nBarColor = a_Color;
    }

    public void setPercent(float a_Per) {
        fPer = a_Per;
        invalidate();
    }

    public void onDraw(Canvas canvas) {

//        int nW = this.getWidth() - (int)PHLib.convertDpToPixel(40, mCtx);
        int nW = this.getWidth();
		int nH = this.getHeight();

        paintStroke.setAntiAlias(true);
        paintStroke.setColor(nBarColor);
//        paintStroke.setStrokeWidth(fBarH);

        paintDraw.setTextSize(fTextSize);
        paintDraw.setColor(nBarColor);

//        canvas.drawLine(0, 0, nW*fPer, nW, paintStroke);
        canvas.drawRect(0, 0, nW*fPer, nH, paintStroke);

//        canvas.drawText(sText, nW*fPer+10, nH, paintDraw);
    }
}
