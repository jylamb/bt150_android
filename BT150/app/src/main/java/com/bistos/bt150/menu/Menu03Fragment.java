package com.bistos.bt150.menu;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.TimePicker;

import com.bistos.bt150.R;
import com.bistos.bt150.com.DBHelper;
import com.bistos.bt150.com.HorBarView;
import com.bistos.bt150.com.MilkData;
import com.bistos.bt150.com.PHCommon;
import com.bistos.bt150.com.PHLib;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import cn.jeesoft.widget.pickerview.CharacterPickerView;
import cn.jeesoft.widget.pickerview.CharacterPickerWindow;
import cn.jeesoft.widget.pickerview.OnOptionChangedListener;

import static com.bistos.bt150.com.PHLib.LOG;
import static com.bistos.bt150.com.PHLib.LOGe;

/**
 * Created by lamb on 2017. 10. 20..
 */

public class Menu03Fragment extends Fragment {

    private static final String TAG = "StatisticFragment";
    private static final int nBarIdL = 9000001;
    private static final int nBarIdR = 9000002;

    private View vFragment = null;

    private static final int AXIS_TYPE_DAY = 100;
    private static final int AXIS_TYPE_WEEK = 200;
    private static final int AXIS_TYPE_MONTH = 300;

    private TextView textView_FrgMenu03_NoData;
    private ListView listView_Statistic_List;
    private StatisticListAdapter adapterStatisticList;

    private ImageButton imageButton_FrgMenu03_Pre;
    private ImageButton imageButton_FrgMenu03_Next;
    private TextView textView_FrgMenu03_Title;

    private TextView textView_PopStatisticAdd_Date = null;

    private ArrayList<MilkData> arrData = null;

    private Date dateCurSel = null;
    private int nMaxAmount = 0;

    private ImageButton imageButton_FrgMenu03_Add;

    private ArrayList <String> arrSeqMin = new ArrayList<String>();
    private ArrayList <String> arrSeqSec = new ArrayList<String>();

    private ArrayList <String> arrSearchYear = new ArrayList<String>();
    private ArrayList <String> arrSearchMon = new ArrayList<String>();
    private ArrayList <String> arrSearchDay = new ArrayList<String>();
    private ArrayList <String> arrSearchHour = new ArrayList<String>();
    private ArrayList <String> arrSearchMin = new ArrayList<String>();

    private ArrayList <String> arrMilkAmt = new ArrayList<String>();

    private int nIdxDurationMin = 0;
    private int nIdxDurationSec = 0;

    private Dialog dlgStatisticAdd;
//    private Dialog dlgStatisticDateTime;
    private long lMilkDateOrg = 0;

    private int nTabIdx = 0;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        vFragment = inflater.inflate(R.layout.fragment_menu03, container, false);

        nTabIdx = getArguments().getInt("position");

        dateCurSel = new Date(System.currentTimeMillis());

        if (nTabIdx == 1) {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(dateCurSel);
            calendar.set(Calendar.DAY_OF_MONTH, 1);
            dateCurSel = calendar.getTime();

//            Calendar calendar = Calendar.getInstance();
//            calendar.setTime(dateCurSel);
//            while (calendar.get(Calendar.DAY_OF_WEEK) > calendar.getFirstDayOfWeek()) {
//                // Substract 1 day until first day of week.
//                calendar.add(Calendar.DATE, -1);
//            }
//            dateCurSel = calendar.getTime();

        } else if(nTabIdx == 2) {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(dateCurSel);
            calendar.set(Calendar.DAY_OF_YEAR, 1);
            dateCurSel = calendar.getTime();
        }

        SimpleDateFormat aSdf = new SimpleDateFormat("yyyy-MM-dd");
        LOGe(TAG,  "nTabIdx:" + nTabIdx + " dateCurSel:" + aSdf.format(dateCurSel));

        imageButton_FrgMenu03_Pre = (ImageButton) vFragment.findViewById(R.id.imageButton_FrgMenu03_Pre);
        imageButton_FrgMenu03_Next = (ImageButton) vFragment.findViewById(R.id.imageButton_FrgMenu03_Next);
        textView_FrgMenu03_Title = (TextView) vFragment.findViewById(R.id.textView_FrgMenu03_Title);

        imageButton_FrgMenu03_Pre.setOnClickListener(onClickListener);
        imageButton_FrgMenu03_Next.setOnClickListener(onClickListener);

        arrData = new ArrayList<MilkData>();
        adapterStatisticList = new StatisticListAdapter(getActivity());

        listView_Statistic_List = (ListView) vFragment.findViewById(R.id.listView_Statistic_List);
        listView_Statistic_List.setAdapter(adapterStatisticList);

        textView_FrgMenu03_NoData = (TextView) vFragment.findViewById(R.id.textView_FrgMenu03_NoData);


        refreshDateTitle();

        imageButton_FrgMenu03_Add = (ImageButton) vFragment.findViewById(R.id.imageButton_FrgMenu03_Add);
        if (nTabIdx == 0) {
            imageButton_FrgMenu03_Add.setVisibility(View.VISIBLE);
        } else {
            imageButton_FrgMenu03_Add.setVisibility(View.GONE);
        }

        imageButton_FrgMenu03_Add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showStatisticEdit(-1);
            }
        });

        initData();

        getActivity().registerReceiver(refreshReceiver, new IntentFilter(PHCommon.MSG_VIEW_SEL));

        return vFragment;
    }

    private final BroadcastReceiver refreshReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent a_intent) {
            Bundle bundle = a_intent.getExtras();
            int aPosition = bundle.getInt("position");
            if (aPosition == nTabIdx) {
                doRefreshData();
            }
        }
    };

    @Override
    public void onDestroyView() {
        getActivity().unregisterReceiver(refreshReceiver);

        super.onDestroyView();
    }

    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (view.getId() == R.id.imageButton_FrgMenu03_Pre) {
                moveReferenceDate(-1);

            } else if (view.getId() == R.id.imageButton_FrgMenu03_Next) {
                moveReferenceDate(1);
            }
        }
    };

    private void moveReferenceDate(int a_Dir) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(dateCurSel);
        if (a_Dir < 0) {
            if(nTabIdx == 0) {
                calendar.add(Calendar.DATE, -1);
            } else if(nTabIdx == 1) {
//                calendar.add(Calendar.DATE, -7);
                calendar.add(Calendar.MONTH, -1);
            } else if(nTabIdx == 2) {
                calendar.add(Calendar.YEAR, -1);
            }
        } else {
            if(nTabIdx == 0) {
                calendar.add(Calendar.DATE, 1);
            } else if(nTabIdx == 1) {
//                calendar.add(Calendar.DATE, 7);
                calendar.add(Calendar.MONTH, 1);
            } else if(nTabIdx == 2) {
                calendar.add(Calendar.YEAR, 1);
            }
        }
        dateCurSel = calendar.getTime();

        refreshDateTitle();

        doRefreshData();
    }

    private void refreshDateTitle() {
        if (nTabIdx == 0) {
            SimpleDateFormat aSdf = new SimpleDateFormat("yyyy.MM.dd");
            textView_FrgMenu03_Title.setText(aSdf.format(dateCurSel));

        } else if (nTabIdx == 1) {
            SimpleDateFormat aSdf = new SimpleDateFormat("yyyy.MM");
            textView_FrgMenu03_Title.setText(aSdf.format(dateCurSel));

        } else if (nTabIdx == 2) {
            SimpleDateFormat aSdf = new SimpleDateFormat("yyyy");
            textView_FrgMenu03_Title.setText(aSdf.format(dateCurSel));
        }
    }

    private void doRefreshData() {
        if (readDBData() <= 0) {
//            initSampleTestData();
        }

        adapterStatisticList.notifyDataSetChanged();
    }

    private void initSampleTestData() {

        SharedPreferences pref = getActivity().getSharedPreferences(getString(R.string.project_name), Activity.MODE_PRIVATE);
        boolean aLoaded = pref.getBoolean("sample_statistic", false);
        if (aLoaded) {
            return;
        }

        SharedPreferences.Editor ePref = pref.edit();
        ePref.putBoolean("sample_statistic", true);
        ePref.commit();

        if (nTabIdx != 0) {
            return;
        }

        LOGe(TAG, "initSampleTestData-1");

//        arrData.add(new MilkData("201710200910", 10, 50, 30));
//        arrData.add(new MilkData("201710210911", 0, 20, 30));
//        arrData.add(new MilkData("201710220911", 30, 30, 30));
//        arrData.add(new MilkData("201710230911", 20, 10, 30));
//        arrData.add(new MilkData("201710240911", 14, 34, 30));
//        arrData.add(new MilkData("201710250911", 20, 30));
//        arrData.add(new MilkData("201710270911", 10, 30));
//        arrData.add(new MilkData("201710290911", 40, 60));
//        arrData.add(new MilkData("201710310911", 40, 30));

//        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmm");
//        DBHelper dbHelper = new DBHelper(getActivity());
//        dbHelper.setMilkData(PHLib.convertToTimeInMilliseconds(sdf, "201710210910"), 10, 10, 30);
//        dbHelper.setMilkData(PHLib.convertToTimeInMilliseconds(sdf, "201710220910"), 20, 20, 40);
//        dbHelper.setMilkData(PHLib.convertToTimeInMilliseconds(sdf, "201710230920"), 30, 40, 50);
//        dbHelper.setMilkData(PHLib.convertToTimeInMilliseconds(sdf, "201710240930"), 40, 50, 60);
//        dbHelper.setMilkData(PHLib.convertToTimeInMilliseconds(sdf, "201710250940"), 50, 60, 20);
//        dbHelper.setMilkData(PHLib.convertToTimeInMilliseconds(sdf, "201710260950"), 60, 30, 70);
//        dbHelper.setMilkData(PHLib.convertToTimeInMilliseconds(sdf, "201710270930"), 70, 40, 120);
//        dbHelper.setMilkData(PHLib.convertToTimeInMilliseconds(sdf, "201710280930"), 80, 50, 90);
//        dbHelper.setMilkData(PHLib.convertToTimeInMilliseconds(sdf, "201710290920"), 50, 60, 60);
//        dbHelper.setMilkData(PHLib.convertToTimeInMilliseconds(sdf, "201710300910"), 40, 10, 40);
//        dbHelper.setMilkData(PHLib.convertToTimeInMilliseconds(sdf, "201711010910"), 30, 20, 20);
//        dbHelper.setMilkData(PHLib.convertToTimeInMilliseconds(sdf, "201711030920"), 20, 30, 10);
//        arrData = dbHelper.getMilkData();

        Date aDate = new Date(System.currentTimeMillis());
        DBHelper dbHelper = new DBHelper(getActivity());

        for (int i = 0; i < 10; i++) {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(aDate);
            calendar.add(Calendar.MINUTE, 10);
            aDate = calendar.getTime();
            dbHelper.setMilkData(aDate.getTime(), i*10+10, i*10+10, i*10);
        }

        readDBData();
    }

    public void initData() {

        initMilkAmtData();

        initSeqTime();

        initSearchMonth();

        doRefreshData();
    }

    private Date getFirstDayOfWeek(long a_Date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date(a_Date));
        while (calendar.get(Calendar.DAY_OF_WEEK) > calendar.getFirstDayOfWeek()) {
            // Substract 1 day until first day of week.
            calendar.add(Calendar.DATE, -1);
        }
        return calendar.getTime();
    }

    private void addWeekMonthData(MilkData a_MilkData) {

        int aDateAdd = 0;
        SimpleDateFormat aSdf = null;

        if (nTabIdx == 1) {
            //Week
            aSdf = new SimpleDateFormat("yyyyMMdd");
            aDateAdd = PHLib.convertToInt(aSdf.format(getFirstDayOfWeek(a_MilkData.lDate)));
            LOGe(TAG, "addWeekMonthData-week: aDateAdd:" + aDateAdd);

        } else if (nTabIdx == 2) {
            //Month
            aSdf = new SimpleDateFormat("yyyyMM");
            aDateAdd = PHLib.convertToInt(aSdf.format(a_MilkData.lDate));
        }

        int aFound = -1;
        for (int i = 0; i < arrData.size(); i++) {
            MilkData milkData = arrData.get(i);
            int aDate = PHLib.convertToInt(aSdf.format(milkData.lDate));
            if (nTabIdx == 1) {
                aDate = PHLib.convertToInt(aSdf.format(getFirstDayOfWeek(milkData.lDate)));
                LOGe(TAG, "aDate:" + aDate);
            }
            if (aDate == aDateAdd) {
                aFound = i;
                break;
            }
        }

        if (aFound < 0) {
            long aDate = getFirstDayOfWeek(a_MilkData.lDate).getTime();
            arrData.add(new MilkData(aDate, a_MilkData.nAmtL, a_MilkData.nAmtR, a_MilkData.nTime));
            aFound = arrData.size()-1;

        } else {
            MilkData milkData = arrData.get(aFound);
            milkData.nAmtL += a_MilkData.nAmtL;
            milkData.nAmtR += a_MilkData.nAmtR;
            milkData.nTime += a_MilkData.nTime;
        }
    }

    private int readDBData() {

        arrData.clear();

        SimpleDateFormat aSdf = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat aSdfTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        String aFrom = "";
        String aTo = "";
        long aFromTime = 0;
        long aToTime = 0;

        try {
            DBHelper  dbHelper = new DBHelper(getActivity());

            if (nTabIdx == 0) {
                //Day
                aFrom = aSdf.format(dateCurSel) +  " 00:00:00";
                aTo = aSdf.format(dateCurSel) +  " 23:59:59";

                aFromTime = aSdfTime.parse(aFrom).getTime();
                aToTime = aSdfTime.parse(aTo).getTime();

                arrData = dbHelper.getMilkData(aFromTime, aToTime);


            } else {
                //Week & Month
                String aDateS = aSdf.format(dateCurSel) +  " 00:00:00";
                Date aDateTimeS = aSdfTime.parse(aDateS);
                aFromTime = aDateTimeS.getTime();

                Calendar calendar = Calendar.getInstance();
                calendar.setTime(aDateTimeS);
                if (nTabIdx == 1) {
//                    calendar.add(Calendar.DATE, 7);
                    calendar.add(Calendar.MONTH, 1);
                } else if (nTabIdx == 2) {
                    calendar.add(Calendar.YEAR, 1);
                }
                aToTime = calendar.getTime().getTime() - 1;

                ArrayList<MilkData> arrDays = dbHelper.getMilkData(aFromTime, aToTime);
                for (int i = 0; i < arrDays.size(); i++) {
                    MilkData milkData = arrDays.get(i);
                    addWeekMonthData(milkData);
                }
            }
            LOGe(TAG, "readDBData Cnt:" + arrData.size());

        } catch (ParseException e) {
            e.printStackTrace();
        }

        for (int i = 0; i < arrData.size(); i++) {
            if (nMaxAmount < arrData.get(i).nAmtL) {
                nMaxAmount = arrData.get(i).nAmtL;
            }
            if (nMaxAmount < arrData.get(i).nAmtR) {
                nMaxAmount = arrData.get(i).nAmtR;
            }
        }

        if(arrData.size() > 0) {
            listView_Statistic_List.setVisibility(View.VISIBLE);
            textView_FrgMenu03_NoData.setVisibility(View.GONE);

        } else {
            listView_Statistic_List.setVisibility(View.GONE);
            textView_FrgMenu03_NoData.setVisibility(View.VISIBLE);
        }

        return arrData.size();
    }

    private void initSearchMonth() {

        arrSearchYear.clear();
        arrSearchMon.clear();

        arrSearchHour.clear();
        arrSearchMin.clear();

        int aStart = 2017;
        for (int i = 0; i < 2; i++) {
            arrSearchYear.add(String.format("%d", aStart--));
        }

        for (int i = 0; i < 12; i++) {
            arrSearchMon.add(String.format("%02d", i+1));
        }

        for (int i = 0; i < 24; i++) {
            arrSearchHour.add(String.format("%02d", i));
        }
        for (int i = 0; i < 60; i++) {
            arrSearchMin.add(String.format("%02d", i));
        }

        initDayData(1);

//        LOG(TAG, "aStart:" + aStart + " aEnd:" + aEnd);
    }

    private void initDayData(int a_Month) {
        arrSearchDay.clear();

        int aMax = getMaxDayOfMonth(a_Month);
        for (int i = 0; i < aMax; i++) {
            arrSearchDay.add(String.format("%02d", i+1));
        }
    }

    private int getMaxDayOfMonth(int a_Month) {
        switch (a_Month) {
            case 2:
                return 28;
            case 4:
            case 6:
            case 9:
            case 11:
                return 30;
        }
        return 31;
    }

    private void initMilkAmtData() {
        arrMilkAmt.clear();

        for (int i = 0; i < 40; i++) {
            arrMilkAmt.add(String.format("%d", i*5));
        }
    }

    private void initSeqTime() {

        arrSeqMin.clear();
        arrSeqSec.clear();

        for (int i = 0; i < 100; i++) {
            arrSeqMin.add(String.format("%02d", i));
        }

        for (int i = 0; i < 12; i++) {
            arrSeqSec.add(String.format("%02d", i*5));
        }

//        LOG(TAG, "aStart:" + aStart + " aEnd:" + aEnd);
    }

    private class StatisticListAdapter extends BaseAdapter {
        private LayoutInflater mInflater;

        public StatisticListAdapter (Context context) {
            mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            return arrData.size();
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            LinearLayout linearLayout_RowStatistic_Base;
            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.row_statistic_list, parent, false);
                linearLayout_RowStatistic_Base = (LinearLayout)convertView.findViewById(R.id.linearLayout_RowStatistic_Base);
            } else {
                linearLayout_RowStatistic_Base = (LinearLayout)convertView.findViewById(R.id.linearLayout_RowStatistic_Base);
                if (linearLayout_RowStatistic_Base == null) {
                    convertView = mInflater.inflate(R.layout.row_statistic_list, parent, false);
                    linearLayout_RowStatistic_Base = (LinearLayout)convertView.findViewById(R.id.linearLayout_RowStatistic_Base);
                }
            }

            final MilkData statisticData = arrData.get(position);

            if (statisticData.nAmtL > 0 || statisticData.nAmtR > 0) {
                linearLayout_RowStatistic_Base.setBackgroundColor(getResources().getColor(R.color.colorWhite));
            } else {
                linearLayout_RowStatistic_Base.setBackgroundColor(getResources().getColor(R.color.colorSelectList));
            }

            linearLayout_RowStatistic_Base.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (nTabIdx == 0) {
                        showStatisticEdit(position);
                    }
                }
            });

            TextView textView_RowStatistic_Title = (TextView) convertView.findViewById(R.id.textView_RowStatistic_Title);

            TextView textView_RowStatistic_Time = (TextView) convertView.findViewById(R.id.textView_RowStatistic_Time);
            TextView textView_RowStatistic_Duration = (TextView) convertView.findViewById(R.id.textView_RowStatistic_Duration);

            if (nTabIdx == 0) {
                textView_RowStatistic_Title.setText(getString(R.string.Time));
                String aDateTime = new SimpleDateFormat("HH:mm").format(new Date(statisticData.lDate));
                textView_RowStatistic_Time.setText(aDateTime);

            } else if (nTabIdx == 1) {
                textView_RowStatistic_Title.setText(getString(R.string.Day));
                String aDateTimeS = new SimpleDateFormat("dd").format(new Date(statisticData.lDate));

                Calendar calendar = Calendar.getInstance();
                calendar.setTime(new Date(statisticData.lDate));
                calendar.add(Calendar.DATE, 6);
                String aDateTimeE = new SimpleDateFormat("dd").format(calendar.getTime());
                textView_RowStatistic_Time.setText(aDateTimeS + "~" + aDateTimeE);

            } else if (nTabIdx == 2) {
                textView_RowStatistic_Title.setText(getString(R.string.Month));
                String aDateTime = new SimpleDateFormat("MM").format(new Date(statisticData.lDate));
                textView_RowStatistic_Time.setText(aDateTime);
            }


            textView_RowStatistic_Duration.setText(String.format("%02d:%02d", statisticData.nTime/60, statisticData.nTime%60));

            String aUnit = PHCommon.getUnit(getActivity());

            TextView textView_RowStatistic_BarL = (TextView) convertView.findViewById(R.id.textView_RowStatistic_BarL);
            TextView textView_RowStatistic_BarR = (TextView) convertView.findViewById(R.id.textView_RowStatistic_BarR);

            if (aUnit.equals("ml")) {
                textView_RowStatistic_BarL.setText(String.format("%d", statisticData.nAmtL) + aUnit);
                textView_RowStatistic_BarR.setText(String.format("%d", statisticData.nAmtR) + aUnit);
            } else {
                textView_RowStatistic_BarL.setText(String.format("%.2f", statisticData.nAmtL*PHCommon.toOZ) + aUnit);
                textView_RowStatistic_BarR.setText(String.format("%.2f", statisticData.nAmtR*PHCommon.toOZ) + aUnit);
            }

            LinearLayout linearlayout_RowStatisticList_BarL = (LinearLayout)convertView.findViewById(R.id.linearlayout_RowStatisticList_BarL);
            HorBarView barViewL = (HorBarView)linearlayout_RowStatisticList_BarL.findViewById(nBarIdL);
            barViewL = (HorBarView) convertView.findViewById(R.id.horBarView_RowStatisticList_BarL);
            barViewL.setBarColor(0xff04b5bb);
//            barViewL.setText(String.format("%d", statisticData.nAmtL) + aUnit);
            barViewL.setPercent( (float)statisticData.nAmtL / (float)nMaxAmount );

            LinearLayout linearlayout_RowStatisticList_BarR = (LinearLayout)convertView.findViewById(R.id.linearlayout_RowStatisticList_BarR);
            HorBarView barViewR = (HorBarView)linearlayout_RowStatisticList_BarR.findViewById(nBarIdR);
            barViewR = (HorBarView) convertView.findViewById(R.id.horBarView_RowStatisticList_BarR);
            barViewR.setBarColor(0xff9fbb04);
//            barViewR.setText(String.format("%d", statisticData.nAmtR) + aUnit);
            barViewR.setPercent( (float)statisticData.nAmtR / (float)nMaxAmount );


            ImageView imageView_RowStatistic_Del = (ImageView) convertView.findViewById(R.id.imageView_RowStatistic_Del);
            if (nTabIdx == 0) {
                imageView_RowStatistic_Del.setVisibility(View.VISIBLE);
                imageView_RowStatistic_Del.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        PHLib.showYesNoDialog(getString(R.string.wantToDelete), getActivity(),new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                                DBHelper dbHelper = new DBHelper(getActivity());
                                dbHelper.deleteMilkData(statisticData.lDate);
                                readDBData();
                                adapterStatisticList.notifyDataSetChanged();
                            }
                        });
                    }
                });
            } else {
                imageView_RowStatistic_Del.setVisibility(View.GONE);
            }



            return convertView;
        }
    }

//    private void showStatisticDateTime() {
//        dlgStatisticDateTime = new Dialog(getActivity(), R.style.fullDialog);
//        LayoutInflater factory = LayoutInflater.from(getActivity());
//        final View vPopDlg = factory.inflate(R.layout.pop_datetime_select, null);
//        dlgStatisticDateTime.setContentView(vPopDlg);
//        dlgStatisticDateTime.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
//
//
//        ImageButton imageButton_PopDateTime_Close = (ImageButton) vPopDlg.findViewById(R.id.imageButton_PopDateTime_Close);
//        imageButton_PopDateTime_Close.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                dlgStatisticDateTime.cancel();
//            }
//        });
//        Button button_PopDateTime_Cancel = (Button) vPopDlg.findViewById(R.id.button_PopDateTime_Cancel);
//        button_PopDateTime_Cancel.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                dlgStatisticDateTime.cancel();
//            }
//        });
//        Button button_PopStatisticAdd_Save = (Button) vPopDlg.findViewById(R.id.button_PopStatisticAdd_Save);
//        button_PopStatisticAdd_Save.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                dlgStatisticDateTime.cancel();
//
////                String date_selected = String.valueOf(arrSeqMin.get(nIdxDurationMin) + " : " + arrSeqSec.get(nIdxDurationSec));
////                LOGe(TAG, "time_selected:" + date_selected);
////
////                long aDate = arrData.get(a_Idx).lDate;
////                int aLeft = PHLib.convertToInt(editText_PopStatisticAdd_LeftAmt.getText().toString());
////                int aRight = PHLib.convertToInt(editText_PopStatisticAdd_RightAmt.getText().toString());
////                int aMin = PHLib.convertToInt(arrSeqMin.get(nIdxDurationMin));
////                int aSec = PHLib.convertToInt(arrSeqMin.get(nIdxDurationSec));
////
////                saveStatisticItem(aDate, aLeft, aRight, aMin*60 + aSec);
//            }
//        });
//
//    }

    private void selectStatisticTime(final int a_Year, final int a_Month, final int a_Day, final int a_Hour, final int a_Min, final int a_Idx) {

        TimePickerDialog timePickerDialog = new TimePickerDialog(getActivity(), new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int hourOfDay, int minuteOfHour) {
                GregorianCalendar calendar = new GregorianCalendar();
                calendar.set(a_Year, a_Month, a_Day, hourOfDay, minuteOfHour);

                MilkData aMilkData = new MilkData();
                if (a_Idx >= 0 && a_Idx < arrData.size()) {
                    aMilkData = arrData.get(a_Idx);
                }
                MilkData statisticData = aMilkData;
                statisticData.lDate = calendar.getTimeInMillis();

                String aDateTime = new SimpleDateFormat("yyyy - MM - dd   HH : mm").format(new Date(statisticData.lDate));
                textView_PopStatisticAdd_Date.setText(aDateTime);
            }
        }, a_Hour, a_Min, true);


        timePickerDialog.show();

    }

    private void selectStatisticDate(long a_Date, final int a_Idx) {
        Date aItemDate = new Date(a_Date);
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTime(new Date(a_Date));
        final int aHour = calendar.get(Calendar.HOUR_OF_DAY);
        final int aMinute = calendar.get(Calendar.MINUTE);

        new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                selectStatisticTime(year, monthOfYear, dayOfMonth, aHour, aMinute, a_Idx);
            }
        }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH)).show();
    }

    private void showStatisticEdit(final int a_Idx) {

        dlgStatisticAdd = new Dialog(getActivity(), R.style.fullDialog);
        LayoutInflater factory = LayoutInflater.from(getActivity());
        final View vPopDlg = factory.inflate(R.layout.pop_statistic_add, null);
        dlgStatisticAdd.setContentView(vPopDlg);
        dlgStatisticAdd.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);


        MilkData aMilkData = new MilkData();
        if (a_Idx >= 0 && a_Idx < arrData.size()) {
            aMilkData = arrData.get(a_Idx);
        }
        final MilkData statisticData = aMilkData;

        lMilkDateOrg = statisticData.lDate;

        LinearLayout linearLayout_PopStatisticAdd_Date = (LinearLayout) vPopDlg.findViewById(R.id.linearLayout_PopStatisticAdd_Date);
        linearLayout_PopStatisticAdd_Date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectStatisticDate(statisticData.lDate, a_Idx);
            }
        });

        textView_PopStatisticAdd_Date = (TextView) vPopDlg.findViewById(R.id.textView_PopStatisticAdd_Date);

        final EditText editText_PopStatisticAdd_LeftAmt = (EditText) vPopDlg.findViewById(R.id.editText_PopStatisticAdd_LeftAmt);
        final EditText editText_PopStatisticAdd_RightAmt = (EditText) vPopDlg.findViewById(R.id.editText_PopStatisticAdd_RightAmt);


        final CharacterPickerWindow pickerAmtL = new CharacterPickerWindow(getActivity());
        pickerAmtL.getPickerView().setPicker(arrMilkAmt);
        pickerAmtL.getPickerView().setPickerTextColor(0xffe98a65, 0xffc5c5c5, getActivity().getResources().getColor(R.color.colorPrimary));
        pickerAmtL.setOnoptionsSelectListener(new OnOptionChangedListener() {
            @Override
            public void onOptionChanged(int option1, int option2, int option3) {
                editText_PopStatisticAdd_LeftAmt.setText(arrMilkAmt.get(option1));
            }
        });

        final CharacterPickerWindow pickerAmtR = new CharacterPickerWindow(getActivity());
        pickerAmtR.getPickerView().setPicker(arrMilkAmt);
        pickerAmtR.getPickerView().setPickerTextColor(0xffe98a65, 0xffc5c5c5, getActivity().getResources().getColor(R.color.colorPrimary));
        pickerAmtR.setOnoptionsSelectListener(new OnOptionChangedListener() {
            @Override
            public void onOptionChanged(int option1, int option2, int option3) {
                editText_PopStatisticAdd_RightAmt.setText(arrMilkAmt.get(option1));
            }
        });

        editText_PopStatisticAdd_LeftAmt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pickerAmtL.showAtLocation(view, Gravity.BOTTOM, 0, 0);
            }
        });

        editText_PopStatisticAdd_RightAmt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pickerAmtR.showAtLocation(view, Gravity.BOTTOM, 0, 0);
            }
        });

        TextView textView_PopStatisticAdd_LeftUnit = (TextView) vPopDlg.findViewById(R.id.textView_PopStatisticAdd_LeftUnit);
        TextView textView_PopStatisticAdd_RightUnit = (TextView) vPopDlg.findViewById(R.id.textView_PopStatisticAdd_RightUnit);

        CharacterPickerView pickerViewDurationMin = (CharacterPickerView) vPopDlg.findViewById(R.id.pickerViewDurationMin);
        pickerViewDurationMin.setPicker(arrSeqMin);
        pickerViewDurationMin.setPickerTextColor(0xffe98a65, 0xffc5c5c5, getActivity().getResources().getColor(R.color.colorPrimary));
        pickerViewDurationMin.setOnOptionChangedListener(new OnOptionChangedListener() {
            @Override
            public void onOptionChanged(int option1, int option2, int option3) {
                nIdxDurationMin = option1;
            }
        });
        CharacterPickerView pickerViewDurationSec = (CharacterPickerView) vPopDlg.findViewById(R.id.pickerViewDurationSec);
        pickerViewDurationSec.setPicker(arrSeqSec);
        pickerViewDurationSec.setPickerTextColor(0xffe98a65, 0xffc5c5c5, getActivity().getResources().getColor(R.color.colorPrimary));
        pickerViewDurationSec.setOnOptionChangedListener(new OnOptionChangedListener() {
            @Override
            public void onOptionChanged(int option1, int option2, int option3) {
                nIdxDurationSec = option1;
            }
        });


        ImageButton imageButton_PopStatisticAdd_Close = (ImageButton) vPopDlg.findViewById(R.id.imageButton_PopStatisticAdd_Close);
        imageButton_PopStatisticAdd_Close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dlgStatisticAdd.cancel();
            }
        });
        Button button_PopStatisticAdd_Cancel = (Button) vPopDlg.findViewById(R.id.button_PopStatisticAdd_Cancel);
        button_PopStatisticAdd_Cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dlgStatisticAdd.cancel();
            }
        });
        Button button_PopStatisticAdd_Save = (Button) vPopDlg.findViewById(R.id.button_PopStatisticAdd_Save);
        button_PopStatisticAdd_Save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dlgStatisticAdd.cancel();

                String date_selected = String.valueOf(arrSeqMin.get(nIdxDurationMin) + " : " + arrSeqSec.get(nIdxDurationSec));
                LOGe(TAG, "time_selected:" + date_selected);

                long aDate = System.currentTimeMillis();
                if (a_Idx >= 0 && a_Idx < arrData.size()) {
                    aDate = arrData.get(a_Idx).lDate;
                }

                int aMin = PHLib.convertToInt(arrSeqMin.get(nIdxDurationMin));
                int aSec = PHLib.convertToInt(arrSeqSec.get(nIdxDurationSec));

//                int aLeft = (int) PHLib.convertToDouble(editText_PopStatisticAdd_LeftAmt.getText().toString());
//                int aRight = (int) PHLib.convertToDouble(editText_PopStatisticAdd_RightAmt.getText().toString());
//                LOGe("aLeft:" + aLeft + " editText_PopStatisticAdd_LeftAmt:" + editText_PopStatisticAdd_LeftAmt.getText().toString());
//                saveStatisticItem(aDate, aLeft, aRight, aMin*60 + aSec);

                String aLeft = editText_PopStatisticAdd_LeftAmt.getText().toString();
                String aRight = editText_PopStatisticAdd_RightAmt.getText().toString();
                saveStatisticItem(aDate, aLeft, aRight, aMin*60 + aSec);

            }
        });

        String aUnit = PHCommon.getUnit(getActivity());
        textView_PopStatisticAdd_LeftUnit.setText(aUnit);
        textView_PopStatisticAdd_RightUnit.setText(aUnit);


        String aDateTime = new SimpleDateFormat("yyyy - MM - dd   HH : mm").format(new Date(statisticData.lDate));
        textView_PopStatisticAdd_Date.setText(aDateTime);

        if (aUnit.equals("ml")) {
            editText_PopStatisticAdd_LeftAmt.setText(String.format("%d", statisticData.nAmtL));
            editText_PopStatisticAdd_RightAmt.setText(String.format("%d", statisticData.nAmtR));
        } else {
            editText_PopStatisticAdd_LeftAmt.setText(String.format("%.2f", statisticData.nAmtL*PHCommon.toOZ));
            editText_PopStatisticAdd_RightAmt.setText(String.format("%.2f", statisticData.nAmtR*PHCommon.toOZ));
        }

        if (statisticData.nAmtL == 0) {
            pickerAmtL.getPickerView().setSelectOptions(10);
        } else {
            pickerAmtL.getPickerView().setSelectOptions(statisticData.nAmtL/5);
        }

        if (statisticData.nAmtR == 0) {
            pickerAmtR.getPickerView().setSelectOptions(10);
        } else {
            pickerAmtR.getPickerView().setSelectOptions(statisticData.nAmtR/5);
        }


        int aMin = (int) statisticData.nTime / 60;
        int aSec = (int) (statisticData.nTime % 60) / 5;

        pickerViewDurationMin.setSelectOptions(aMin);
        pickerViewDurationSec.setSelectOptions(aSec);


        dlgStatisticAdd.show();
    }

    private void saveStatisticItem(long a_Date, String a_Left, String a_Right, long a_Duration) {
        String aUnit = PHCommon.getUnit(getActivity());

        int aLeft = 0;
        int aRight = 0;

        if (aUnit.equals("ml")) {
            aLeft = (int) PHLib.convertToDouble(a_Left);
            aRight = (int) PHLib.convertToDouble(a_Right);
            saveStatisticItem(a_Date, aLeft, aRight, a_Duration);
        } else {

            aLeft = (int) Math.ceil(PHLib.convertToDouble(a_Left.replace(',', '.'))*PHCommon.toML);
            aRight = (int) Math.ceil(PHLib.convertToDouble(a_Right.replace(',','.'))*PHCommon.toML);
        }
        saveStatisticItem(a_Date, aLeft, aRight, a_Duration);
    }

    private void saveStatisticItem(long a_Date, int a_Left, int a_Right, long a_Duration) {

        DBHelper dbHelper = new DBHelper(getActivity());

        if (lMilkDateOrg != a_Date) {
            dbHelper.deleteMilkData(lMilkDateOrg);
        }

        dbHelper.setMilkData(a_Date, a_Left, a_Right, a_Duration);

//        String aUnit = PHCommon.getUnit(getActivity());
//        if (aUnit.equals("ml")) {
//            dbHelper.setMilkData(a_Date, a_Left, a_Right, a_Duration);
//        } else {
//            dbHelper.setMilkData(a_Date, (int) Math.ceil(a_Left*PHCommon.toML), (int) Math.ceil(a_Right*PHCommon.toML), a_Duration);
//        }

        readDBData();
        adapterStatisticList.notifyDataSetChanged();
    }

}